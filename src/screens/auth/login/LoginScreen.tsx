import React, { useState, useRef, useEffect, useCallback } from 'react';
import {
  View,
  StyleSheet,
  Keyboard,
  TouchableOpacity,
  BackHandler
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import { login } from '../../../store/auth/AuthThunk';
import { RootState, AppDispatch } from '../../../store';
import LoadingIndicator from '../../../components/LoadingIndicator';
import { LoginRequest } from '../../../models/Login';
import CustomButton from '../../../components/CustomButton';
import { COLORS } from '../../../utils/ColorUtils';
import CustomWrapperView from '../../../components/CustomWrapperView';
import BackgroundImageWrapper from '../../../components/BackgroundImageWrapper';
import CustomTextInput from '../../../components/CustomTextInput';
import SliderTab from '../../../components/SliderTab';
import { STRINGS } from '../../../utils/Strings';
import { ICONS } from '../../../utils/Icons';
import { ROUTE_LABELS } from '../../../utils/AppConstants';
import {
  Utils,
  dismissKeyboard,
  handlePlaceHolderUpdate
} from '../../../utils/Utils';
import CustomText from '../../../components/CustomText';
import { SizeUtils } from '../../../utils/SizeUtils';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../../types/RootStackParamList';
import { RouteProp, useFocusEffect } from '@react-navigation/native';
import CustomBackButton from '../../../components/CustomBackButton';
import { setError } from '../../../store/auth/AuthSlice';
import EncryptedStorageUtil from '../../../utils/EncryptedStorage/EncryptedStorage';
import useDialog from '../../../components/hooks/useDialog';

type LoginScreenProps = {
  navigation: StackNavigationProp<RootStackParamList, 'Login'>;
  route: RouteProp<RootStackParamList, 'Login'>;
};

const LoginScreen: React.FC<LoginScreenProps> = ({ navigation }) => {
  const dispatch = useDispatch<AppDispatch>();
  const { loading, error, user } = useSelector(
    (state: RootState) => state.auth
  );

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const passwordInputRef = useRef(null);
  const [selectedTab, setSelectedTab] = useState({ id: 3, name: 'Student' });
  const {
    dialogVisible,
    dialogTitle,
    dialogBody,
    dialogImage,
    displayConfirmButton,
    showDialog,
    hideDialog
  } = useDialog();
  const [rememberMe, setRememberMe] = useState(false);

  useEffect(() => {
    loadRememberedLogin();
  }, []);

  const loadRememberedLogin = async () => {
    try {
      const savedEmail = await EncryptedStorageUtil.getData('email');
      const savedPassword = await EncryptedStorageUtil.getData('password');
      // const savedRole = await EncryptedStorageUtil.getData('roleState');

      // console.log('saved role', JSON.parse(savedRole));
      // if (savedRole) setSelectedTab(JSON.parse(savedRole));

      if (savedEmail && savedPassword) {
        setEmail(savedEmail);
        setPassword(savedPassword);
        setRememberMe(true);
      }
    } catch (error) {
      console.error('Failed to load remembered login', error);
    }
  };

  const handleLogin = () => {
    dismissKeyboard();

    if (!email || !password) {
      dispatch(setError('Email and password are required'));
      return;
    }
    const loginData: LoginRequest = {
      email: email.trim(),
      password: password.trim()
    };
    dispatch(login(loginData));

    if (rememberMe) {
      EncryptedStorageUtil.saveData('email', email.trim());
      EncryptedStorageUtil.saveData('password', password.trim());
    } else {
      EncryptedStorageUtil.removeData('email');
      EncryptedStorageUtil.removeData('password');
    }
    // EncryptedStorageUtil.saveData('roleState', JSON.stringify(selectedTab));
  };

  useEffect(() => {
    if (error && !user) {
      showDialog('Error', error, ICONS.warningIcon, false);
    }
  }, [error, user]);

  const handleDialogDismiss = () => {
    if (dialogVisible) {
      hideDialog();
    }
    dispatch(setError(null));
  };

  return (
    <>
      <CustomBackButton showMoreIcon={false} />
      <CustomWrapperView
        dialogVisible={dialogVisible}
        dialogImage={dialogImage}
        dialogTitle={dialogTitle}
        dialogBody={dialogBody}
        onDialogDismiss={handleDialogDismiss}
        withScroll={false}
        enableAutoDismiss={false}
        displayConfirm={displayConfirmButton}
      >
        <BackgroundImageWrapper>
          <View style={styles.container}>
            <CustomText style={styles.title} weightType="bold">
              {'Login'}
            </CustomText>

            <CustomTextInput
              label={STRINGS.email}
              LeftIcon={ICONS.emailIcon}
              placeholder={handlePlaceHolderUpdate(selectedTab.name, 'email')}
              value={email}
              onChangeText={setEmail}
              moreStyles={{ marginTop: 20 }}
              onSubmitEditing={() => passwordInputRef.current?.focus()}
            />
            <CustomTextInput
              label={STRINGS.password}
              LeftIcon={ICONS.padlockIcon}
              placeholder={handlePlaceHolderUpdate(
                selectedTab.name,
                'password'
              )}
              value={password}
              onChangeText={setPassword}
              inputRef={passwordInputRef}
              secureTextEntry
              onSubmitEditing={handleLogin}
              moreStyles={{ marginVertical: 15 }}
            />
            <View style={styles.checkBoxForgotPasswordView}>
              <TouchableOpacity
                style={styles.rememberMeContainer}
                onPress={() => setRememberMe(!rememberMe)}
              >
                <TouchableOpacity
                  onPress={() => setRememberMe(!rememberMe)}
                  style={styles.checkbox}
                >
                  {rememberMe && <View style={styles.checkedBox} />}
                </TouchableOpacity>
                <CustomText style={styles.rememberMeText}>
                  Remember me
                </CustomText>
              </TouchableOpacity>
              <CustomText
                style={styles.forgotPasswordText}
                onPress={() =>
                  Utils.goToNewScreen(navigation, ROUTE_LABELS.RESET_PASSWORD)
                }
              >
                Forgot Password?
              </CustomText>
            </View>
            <CustomButton
              title="Log In"
              onPress={handleLogin}
              buttonStyle={styles.loginButton}
              textStyle={styles.loginText}
              disabled={loading}
            />
            <View
              style={{
                alignItems: 'center',
                marginTop: 15
              }}
            >
              <CustomText style={styles.signUpText}>
                Don't have an account?{' '}
                {
                  <CustomText
                    weightType="bold"
                    fontType="averta"
                    style={{
                      color: COLORS.sbsDarkenColor(COLORS.green, 30),
                      fontSize: SizeUtils.scaleFont(15)
                    }}
                    onPress={() =>
                      Utils.goToNewScreen(
                        navigation,
                        ROUTE_LABELS.REGISTER,
                        {},
                        false,
                        false
                      )
                    }
                  >
                    Sign Up
                  </CustomText>
                }
              </CustomText>
            </View>
          </View>
        </BackgroundImageWrapper>
      </CustomWrapperView>
      {loading && <LoadingIndicator title="Logging In" />}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    padding: 23
  },
  title: {
    fontSize: SizeUtils.scaleFont(20),
    marginBottom: 15,
    textAlign: 'left'
  },
  loginButton: {
    marginBottom: 10,
    backgroundColor: COLORS.sbsDarkenColor(COLORS.green, 30),
    marginTop: 20
  },
  loginText: {
    color: COLORS.white,
    fontSize: SizeUtils.scaleFont(14)
  },
  forgotPasswordText: {
    // marginVertical: 5,
    color: COLORS.sbsDarkenColor(COLORS.green, 30),
    fontSize: SizeUtils.scaleFont(13),
    textAlign: 'right'
  },
  signUpText: {
    marginVertical: 5,
    fontSize: SizeUtils.scaleFont(12),
    color: COLORS.gray,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center'
  },
  rememberMeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15
  },
  rememberMeText: {
    color: COLORS.sbsDarkenColor(COLORS.green, 30),
    fontSize: SizeUtils.scaleFont(13),
    marginLeft: 10
  },
  checkbox: {
    width: 15,
    height: 15,
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: COLORS.gray,
    justifyContent: 'center',
    alignItems: 'center'
  },
  checkedBox: {
    width: 8,
    height: 8,
    borderRadius: 10,
    backgroundColor: COLORS.sbsDarkenColor(COLORS.green, 30)
  },
  checkBoxForgotPasswordView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5
  }
});

export default LoginScreen;
