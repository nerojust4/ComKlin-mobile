import React, { useState, useEffect, useCallback } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { resetPasswordWithEmail } from '../../../store/auth/AuthThunk';
import { RootState, AppDispatch } from '../../../store';
import LoadingIndicator from '../../../components/LoadingIndicator';
import CustomButton from '../../../components/CustomButton';
import { COLORS } from '../../../utils/ColorUtils';

import CustomTextInput from '../../../components/CustomTextInput';
import { STRINGS } from '../../../utils/Strings';
import { ICONS } from '../../../utils/Icons';
import CustomWrapperView from '../../../components/CustomWrapperView';
import BackgroundImageWrapper from '../../../components/BackgroundImageWrapper';
import { ResetEmailRequest } from '../../../models/ResetEmailRequest';
import CustomText from '../../../components/CustomText';
import { SizeUtils } from '../../../utils/SizeUtils';
import CustomBackButton from '../../../components/CustomBackButton';
import { clearUser } from '../../../store/auth/AuthSlice';
import useDialog from '../../../components/hooks/useDialog';

const ResetPasswordScreen = ({ navigation }) => {
  const dispatch = useDispatch<AppDispatch>();
  const { loading, error, user } = useSelector(
    (state: RootState) => state.users
  );
  const [email, setEmail] = useState('');
  const {
    dialogVisible,
    dialogTitle,
    dialogBody,
    dialogImage,
    displayConfirmButton,
    showDialog,
    hideDialog
  } = useDialog();

  const handleResetEmail = async () => {
    if (!email) {
      showDialog('Error', 'Email is required', ICONS.warningIcon, false);
      return;
    }

    const resetEmailData: ResetEmailRequest = {
      email
    };

    try {
      await dispatch(resetPasswordWithEmail(resetEmailData)).unwrap();
    } catch (err) {
      hideDialog();
    }
  };

  const handleDialogDismiss = useCallback(() => {
    dispatch(clearUser());
    hideDialog();
  }, [dispatch]);

  const handleDialogOkay = useCallback(() => {
    hideDialog();
    dispatch(clearUser());
    navigation.goBack();
  }, [dispatch]);

  useEffect(() => {
    if (error) {
      showDialog('Error', error, ICONS.warningIcon, false);
    }
  }, [error]);

  useEffect(() => {
    if (!loading && !error && user) {
      showDialog(
        'Success',
        'Password reset was successful, now login with your new password',
        ICONS.successIcon,
        false
      );
    }
  }, [loading, error, user]);

  return (
    <>
      <CustomBackButton showMoreIcon={false} />
      <CustomWrapperView
        dialogVisible={dialogVisible}
        dialogImage={dialogImage}
        dialogTitle={dialogTitle}
        dialogBody={dialogBody}
        onDialogDismiss={handleDialogDismiss}
        onDialogOkay={handleDialogOkay}
        displayConfirm={displayConfirmButton}
      >
        <BackgroundImageWrapper>
          <View style={styles.container}>
            <CustomText style={styles.title} weightType="bold">
              Reset Password
            </CustomText>
            <CustomTextInput
              label={STRINGS.email}
              LeftIcon={ICONS.emailIcon}
              placeholder={STRINGS.enterEmail}
              value={email}
              onChangeText={setEmail}
              moreStyles={{ marginTop: 10 }}
              onSubmitEditing={handleResetEmail}
            />

            <CustomButton
              title="Reset"
              onPress={handleResetEmail}
              buttonStyle={[styles.button, styles.registerButton]}
              textStyle={styles.registerText}
              disabled={loading}
            />
            {loading && <LoadingIndicator />}
          </View>
        </BackgroundImageWrapper>
      </CustomWrapperView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 23
  },
  title: {
    fontSize: SizeUtils.scaleFont(20),
    marginBottom: 26,
    textAlign: 'left'
  },
  button: {
    width: '100%',
    padding: 15,
    borderRadius: 25,
    marginTop: 40,
    alignItems: 'center'
  },
  registerButton: {
    backgroundColor: COLORS.sbsMediumDarkRed
  },
  registerText: {
    color: COLORS.white,
    fontSize: SizeUtils.scaleFont(14)
  },
  error: {
    color: 'red',
    marginBottom: 12
  }
});

export default ResetPasswordScreen;
