import React, { useState, useEffect, useCallback, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState, AppDispatch } from '../../../store';
import LoadingIndicator from '../../../components/LoadingIndicator';
import CustomWrapperView from '../../../components/CustomWrapperView';
import BackgroundImageWrapper from '../../../components/BackgroundImageWrapper';
import { ICONS } from '../../../utils/Icons';

import { clearUser, setError } from '../../../store/user/UserSlice';
import { RootStackParamList } from '../../../types/RootStackParamList';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import CustomBackButton from '../../../components/CustomBackButton';
import { createUser } from '../../../store/user/UserThunk';
import UserRegistrationForm from '../../../components/UserRegistrationForm';
import useDialog from '../../../components/hooks/useDialog';
import { dismissKeyboard } from '../../../utils/Utils';
import { CreateUserRequest } from '../../../models/User';
type RegisterScreenProps = {
  navigation: StackNavigationProp<RootStackParamList, 'Register'>;
  route: RouteProp<RootStackParamList, 'Register'>;
};

const RegisterScreen: React.FC<RegisterScreenProps> = ({ navigation }) => {
  const dispatch = useDispatch<AppDispatch>();
  const { loading, error } = useSelector((state: RootState) => state.users);
  const [isRegistrationSuccessful, setIsRegistrationSuccessful] =
    useState(false);
  const {
    dialogVisible,
    dialogTitle,
    dialogBody,
    dialogImage,
    displayConfirmButton,
    showDialog,
    hideDialog
  } = useDialog();

  useEffect(() => {
    if (error) {
      showDialog('Error', error, ICONS.warningIcon, false);
    }
  }, [error]);

  const handleDialogDismiss = useCallback(() => {
    hideDialog();
    if (isRegistrationSuccessful) {
      navigation.goBack();
    }
    dispatch(clearUser());
  }, [dispatch]);

  const handleRegistrationSubmit = (userData: CreateUserRequest) => {
    dismissKeyboard();

    if (Object.values(userData).some((value) => value === '')) {
      dispatch(setError('All fields are required'));
      return;
    }
    console.log('Received User Data:', userData);
    dispatch(createUser(userData))
      .unwrap()
      .then(() => {
        showDialog(
          'Success',
          'User registration successful, login with new credentials',
          ICONS.successIcon,
          false
        );
        setIsRegistrationSuccessful(true);
      })
      .catch((error) => {
        dispatch(setError(error || 'Registration failed'));

        setIsRegistrationSuccessful(false);
      });
  };

  return (
    <>
      <CustomBackButton showMoreIcon={false} text="Register" />
      <CustomWrapperView
        dialogVisible={dialogVisible}
        dialogImage={dialogImage}
        dialogTitle={dialogTitle}
        dialogBody={dialogBody}
        onDialogDismiss={handleDialogDismiss}
        enableAutoDismiss={false}
        displayConfirm={displayConfirmButton}
      >
        <BackgroundImageWrapper>
          <UserRegistrationForm
            userType={'user'}
            onSubmit={handleRegistrationSubmit}
            loading={loading}
            style={{ paddingHorizontal: 20, paddingBottom: 25 }}
            buttonText={'Register'}
          />
        </BackgroundImageWrapper>
      </CustomWrapperView>
      {loading && <LoadingIndicator body="Registering User" />}
    </>
  );
};

export default RegisterScreen;
