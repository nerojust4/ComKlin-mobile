import React, {useState} from 'react';

// Initial state object for bottom sheets
const initialBottomSheetState = {
  main: false,
  country: false,
  state: false,
};

const RegisterBottomSheet = () => {
  // State to manage the visibility of bottom sheets
  const [bottomSheetVisible, setBottomSheetVisible] = useState(
    initialBottomSheetState,
  );

  // Dynamic function to handle opening of bottom sheets
  const handleOpenBottomSheet = sheetName => {
    setBottomSheetVisible(prevState => ({
      ...prevState,
      [sheetName]: true,
    }));
  };

  // Dynamic function to handle closing of bottom sheets
  const handleCloseBottomSheet = sheetName => {
    setBottomSheetVisible(prevState => ({
      ...prevState,
      [sheetName]: false,
    }));
  };

  return (
    <View>
      <Button
        title="Open Main Bottom Sheet"
        onPress={() => handleOpenBottomSheet('main')}
      />
      <Button
        title="Close Main Bottom Sheet"
        onPress={() => handleCloseBottomSheet('main')}
      />

      <Button
        title="Open Country Bottom Sheet"
        onPress={() => handleOpenBottomSheet('country')}
      />
      <Button
        title="Close Country Bottom Sheet"
        onPress={() => handleCloseBottomSheet('country')}
      />

      <Button
        title="Open State Bottom Sheet"
        onPress={() => handleOpenBottomSheet('state')}
      />
      <Button
        title="Close State Bottom Sheet"
        onPress={() => handleCloseBottomSheet('state')}
      />

      {bottomSheetVisible.main && (
        <BottomSheet>{/* Your main bottom sheet content */}</BottomSheet>
      )}

      {bottomSheetVisible.country && (
        <BottomSheet>{/* Your country bottom sheet content */}</BottomSheet>
      )}

      {bottomSheetVisible.state && (
        <BottomSheet>{/* Your state bottom sheet content */}</BottomSheet>
      )}
    </View>
  );
};

export default RegisterBottomSheet;
