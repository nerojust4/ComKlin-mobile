import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Button,
  Image,
  Platform,
  PermissionsAndroid,
  TouchableOpacity,
  Alert
} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import ImagePicker from 'react-native-image-picker';
import MapView, { Marker } from 'react-native-maps';
import CustomBackButton from '../../components/CustomBackButton';
import CustomWrapperView from '../../components/CustomWrapperView';
import CustomTextInput from '../../components/CustomTextInput';
import CustomButton from '../../components/CustomButton';
import { COLORS } from '../../utils/ColorUtils';
import { SizeUtils } from '../../utils/SizeUtils';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../store';
import CustomText from '../../components/CustomText';
import { IMAGES } from '../../utils/Images';
import moment from 'moment';
import LoadingIndicator from '../../components/LoadingIndicator';
import { createEvent } from '../../store/events/EventThunk';
import useDialog from '../../components/hooks/useDialog';
import { ICONS } from '../../utils/Icons';
import { setError, setEvent } from '../../store/events/EventSlice';

const CreateCleaningScreen = ({ navigation }) => {
  const [eventName, setEventName] = useState('');
  const [eventDescription, setEventDescription] = useState('');
  const [eventLocation, setEventLocation] = useState(null);
  const [eventDateTime, setEventDateTime] = useState(null);
  const [isDateTimePickerVisible, setIsDateTimePickerVisible] = useState(false);
  const [images, setImages] = useState([]);
  const [videos, setVideos] = useState([]);
  const dispatch = useDispatch();
  const { event, loading, error } = useSelector(
    (state: RootState) => state.events
  );
  const {
    dialogVisible,
    dialogTitle,
    dialogBody,
    dialogImage,
    displayConfirmButton,
    showDialog,
    hideDialog
  } = useDialog();
  // Function to request camera and gallery permissions (Android)
  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.CAMERA,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
      ]);
      if (
        granted['android.permission.CAMERA'] ===
          PermissionsAndroid.RESULTS.GRANTED &&
        granted['android.permission.WRITE_EXTERNAL_STORAGE'] ===
          PermissionsAndroid.RESULTS.GRANTED
      ) {
        console.log('Camera permissions granted');
      } else {
        console.log('Camera permissions denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  useEffect(() => {
    // Request camera permissions when the component mounts (Android only)
    if (Platform.OS === 'android') {
      requestCameraPermission();
    }
  }, []);
  useEffect(() => {
    if (error) {
      showDialog('Error', error, ICONS.warningIcon);
    }
  }, [error]);

  useEffect(() => {
    if (!loading && !error && event) {
      showDialog('Success', 'Activity Created Successfully', ICONS.successIcon);
    }
  }, [loading, error, event]);

  // Function to handle datetime picker visibility
  const showDateTimePicker = () => {
    setIsDateTimePickerVisible(true);
  };

  // Function to handle datetime selection
  const handleDateTimeConfirm = (date) => {
    console.log('Date is', date);
    setEventDateTime(date);
    setIsDateTimePickerVisible(false);
  };

  // Function to handle media upload from gallery
  const handleMediaUpload = (mediaType) => {
    const options = {
      mediaType: mediaType,
      storageOptions: {
        skipBackup: true,
        path: 'media'
      }
    };

    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response?.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        const uri = response.uri;
        if (mediaType === 'photo') {
          setImages([...images, uri]);
        } else if (mediaType === 'video') {
          setVideos([...videos, uri]);
        }
      }
    });
  };

  // Function to handle map marker press (location selection)
  const handleMapPress = (event) => {
    const { coordinate } = event.nativeEvent;
    setEventLocation(coordinate);
  };
  const handleDialogDismiss = () => {
    if (event) {
      navigation.goBack();
    }
    clearAllStuff();
  };

  const clearAllStuff = () => {
    hideDialog();
    dispatch(setError(null));
    dispatch(setEvent(null));
  };

  // Function to handle event submission
  const handleEventSubmit = () => {
    const eventPayload = {
      title: eventName,
      description: eventDescription,
      location: eventLocation,
      date: eventDateTime,
      images: images,
      videos: videos
    };
    dispatch(createEvent(eventPayload));

    // Reset form fields after submission (if needed)
    // setEventName('');
    // setEventDescription('');
    // setEventLocation(null);
    // setEventDateTime(null);
    // setImages([]);
    // setVideos([]);
  };

  return (
    <>
      <CustomBackButton text="Create Activity" />
      <CustomWrapperView
        dialogVisible={dialogVisible}
        dialogImage={dialogImage}
        dialogTitle={dialogTitle}
        dialogBody={dialogBody}
        onDialogDismiss={handleDialogDismiss}
      >
        <View style={styles.container}>
          <CustomTextInput
            label="Event Name"
            placeholder="Enter event name"
            value={eventName}
            onChangeText={setEventName}
            moreStyles={styles.input}
          />

          <CustomTextInput
            label="Event Description"
            placeholder="Enter description"
            value={eventDescription}
            onChangeText={setEventDescription}
            multiline
            moreStyles={styles.input}
          />

          {/* <CustomText style={styles.labelTitle}>
            Pick a location for the Event
          </CustomText> */}
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <CustomTextInput
              label="Event Location"
              placeholder="Enter location"
              value={eventLocation}
              onChangeText={setEventLocation}
              multiline
              moreStyles={[styles.input, { flex: 1 }]}
            />

            <TouchableOpacity>
              <Image
                source={IMAGES.locationIcon}
                style={{ width: 40, height: 40, tintColor: COLORS.green }}
              />
            </TouchableOpacity>
          </View>
          {/* <MapView
            style={styles.map}
            initialRegion={{
              latitude: 37.78825,
              longitude: -122.4324,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421
            }}
            onPress={handleMapPress}
          > */}
          {/* {eventLocation && <Marker coordinate={eventLocation} />}
          </MapView> */}

          <CustomText style={styles.labelTitle}>Event Start Date</CustomText>
          <TouchableOpacity
            onPress={showDateTimePicker}
            activeOpacity={0.6}
            style={styles.selectedStyle}
          >
            <View style={styles.row}>
              <CustomText style={{ fontSize: SizeUtils.scaleFont(14) }}>
                {eventDateTime
                  ? moment(new Date(eventDateTime)).format(
                      'MMMM Do YYYY, h:mm:ss a'
                    )
                  : 'Select a date and time'}
              </CustomText>
              <Image source={IMAGES.arrowDownIcon} style={styles.arrowIcon} />
            </View>
          </TouchableOpacity>

          <DateTimePickerModal
            isVisible={isDateTimePickerVisible}
            mode="datetime"
            onConfirm={handleDateTimeConfirm}
            onCancel={() => setIsDateTimePickerVisible(false)}
          />

          <CustomButton
            title="Upload Photo"
            onPress={() => handleMediaUpload('photo')}
            moreStyles={styles.uploadButton}
          />
          <CustomButton
            title="Upload Video"
            onPress={() => handleMediaUpload('video')}
            moreStyles={styles.uploadButton}
          />

          {images.length > 0 && (
            <View>
              {images.map((uri, index) => (
                <Image
                  key={index}
                  source={{ uri }}
                  style={styles.mediaPreview}
                />
              ))}
            </View>
          )}

          {videos.length > 0 && (
            <View>
              {videos.map((uri, index) => (
                <Text key={index} style={styles.videoText}>
                  {uri}
                </Text>
              ))}
            </View>
          )}

          <CustomButton
            title="Create"
            onPress={handleEventSubmit}
            buttonStyle={styles.submitButton}
            textStyle={styles.submitButtonText}
            disabled={loading}
          />
        </View>
      </CustomWrapperView>

      {loading && <LoadingIndicator body="Creating Activity" />}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 25
  },
  input: {
    marginBottom: 10
  },
  map: {
    height: 200,
    width: '100%',
    marginTop: 5
  },
  labelTitle: {
    color: COLORS.sbsDarkenColor(COLORS.lightGray, 20),
    fontSize: SizeUtils.scaleFont(13)
  },
  selectedStyle: {
    borderWidth: 0.5,
    height: SizeUtils.pxToDp(60),
    width: '100%',
    borderColor: COLORS.sbsDarkenColor(COLORS.lightGray, 20),
    borderRadius: 8,
    paddingHorizontal: 16,
    marginRight: 10,
    paddingVertical: 18,
    fontSize: SizeUtils.scaleFont(14),
    color: COLORS.gray,
    marginTop: 5
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  arrowIcon: {
    marginLeft: 10,
    tintColor: COLORS.sbsDarkenColor(COLORS.lightGray, 10),
    width: 15,
    height: 15
  },
  uploadButton: {
    marginBottom: 10,
    backgroundColor: COLORS.primaryColor,
    color: COLORS.white,
    height: 50
  },
  mediaPreview: {
    width: 100,
    height: 100,
    marginTop: 10,
    resizeMode: 'cover'
  },
  videoText: {
    marginTop: 10,
    color: COLORS.primaryColor
  },
  submitButton: {
    marginBottom: 10,
    backgroundColor: COLORS.sbsDarkenColor(COLORS.green, 30),
    marginTop: 30
  },
  submitButtonText: {
    color: COLORS.white,
    fontSize: SizeUtils.scaleFont(14)
  }
});

export default CreateCleaningScreen;
