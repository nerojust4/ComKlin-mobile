import React, { useEffect } from 'react';
import { View, Text, StyleSheet, ScrollView, Image } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../store';
import { IMAGES } from '../../../utils/Images';
import CustomWrapperView from '../../../components/CustomWrapperView';
import RoundComponent from '../../../components/RoundComponent';
import { COLORS } from '../../../utils/ColorUtils';
import SideBar from '../../../components/SideBar';
import CustomText from '../../../components/CustomText';
import { SizeUtils } from '../../../utils/SizeUtils';
import { fetchEvents } from '../../../store/events/EventThunk';
import BarChart from '../../../components/BarChart';
import XYAxisGraph from '../../../components/XYAxisGraph';
import AnimatedBarChart from '../../../components/BarChart';
import PieChart from '../../../components/PieChart';
import LineChart from '../../../components/LineChart';
import { Utils } from '../../../utils/Utils';
import { ROUTE_LABELS } from '../../../utils/AppConstants';

const HomeScreen = ({ navigation }) => {
  const { user } = useSelector((state: RootState) => state.auth);
  const { events } = useSelector((state: RootState) => state.events);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchEvents({ page: 1, pageSize: 3 }));
  }, [dispatch]);

  return (
    <CustomWrapperView withScroll={false}>
      <View style={styles.container}>
        <SideBar />
        <ScrollView style={styles.content}>
          <View style={styles.header}>
            <CustomText style={styles.greeting}>
              Hello{' '}
              <CustomText weightType="bold" style={styles.greeting}>
                {user?.firstName}!
              </CustomText>
            </CustomText>
          </View>

          <View style={styles.section}>
            <CustomText style={styles.sectionTitle} weightType="bold">
              Upcoming Activities
            </CustomText>
            <View style={styles.activityRow}>
              {events.slice(0, 3).map((item, index) => (
                <RoundComponent
                  key={index}
                  title={item.title}
                  onPress={() =>
                    Utils.goToNewScreen(navigation, ROUTE_LABELS.EVENT_DETAIL, {
                      data: item
                    })
                  }
                />
              ))}
            </View>
          </View>
          <View style={styles.section}>
            <CustomText style={styles.sectionTitle} weightType="bold">
              Happening Now!
            </CustomText>
            <View style={styles.activityRow}>
              <RoundComponent />
              <RoundComponent />
              <RoundComponent />
            </View>
          </View>
          <View style={styles.section}>
            <CustomText style={styles.sectionTitle} weightType="bold">
              Personal Impact
            </CustomText>
            <LineChart data={[10, 20, 15, 30, 25]} />
          </View>
          <View style={styles.section}>
            <CustomText style={styles.sectionTitle} weightType="bold">
              Community Impact
            </CustomText>
            <AnimatedBarChart data={[50, 70, 90, 60, 80]} />
          </View>
        </ScrollView>
      </View>
    </CustomWrapperView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row'
  },
  content: {
    flex: 1,
    padding: 10
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20
  },
  greeting: {
    fontSize: SizeUtils.scaleFont(14),
    marginRight: 10
    // fontFamily: 'CustomFont-Bold'
  },
  section: {
    marginBottom: 20
  },
  sectionTitle: {
    marginBottom: 10,
    fontSize: SizeUtils.scaleFont(13)
  },
  activityRow: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  chart: {
    height: 100,
    backgroundColor: '#ccc'
  }
});

export default HomeScreen;
