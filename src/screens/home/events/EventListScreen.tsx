import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import CustomBackButton from '../../../components/CustomBackButton';

// Sample data for events (replace with your actual data)
const sampleEvents = [
  {
    id: '1',
    name: 'Event 1',
    description: 'Description for Event 1',
    location: 'Location 1',
    dateTime: '2024-07-01 10:00 AM'
  },
  {
    id: '2',
    name: 'Event 2',
    description: 'Description for Event 2',
    location: 'Location 2',
    dateTime: '2024-07-02 02:00 PM'
  },
  {
    id: '3',
    name: 'Event 3',
    description: 'Description for Event 3',
    location: 'Location 3',
    dateTime: '2024-07-03 09:00 AM'
  }
  // Add more events as needed
];

const EventListScreen = ({ navigation }) => {
  // State to hold events data
  const [events, setEvents] = useState([]);

  // Mock data loading (can be replaced with actual data fetching)
  useEffect(() => {
    // Simulating data fetching delay
    setTimeout(() => {
      setEvents(sampleEvents);
    }, 1000); // Simulated delay of 1 second
  }, []);

  // Render item for FlatList
  const renderItem = ({ item }) => (
    <TouchableOpacity
      style={styles.eventItem}
      onPress={() => navigation.navigate('EventDetail', { eventId: item.id })}
    >
      <Text style={styles.eventName}>{item.name}</Text>
      <Text>{item.dateTime}</Text>
      <Text>{item.location}</Text>
    </TouchableOpacity>
  );

  return (
    <>
    <CustomBackButton text='Event Activities'/>
      <View style={styles.container}>
        <FlatList
          data={events}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
          style={{ width: '100%' }}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20
  },
  eventItem: {
    backgroundColor: '#f0f0f0',
    padding: 20,
    marginBottom: 10,
    borderRadius: 10,
    width: '100%'
  },
  eventName: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 5
  }
});

export default EventListScreen;
