import moment from 'moment';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import CustomBackButton from '../../../components/CustomBackButton';
import CustomWrapperView from '../../../components/CustomWrapperView';
import CustomText from '../../../components/CustomText';

const EventDetailScreen = ({ route }) => {
  const { data } = route.params; // Get eventId from navigation params

  // Fetch event details based on eventId (you can implement this part)

  // Mock event details (replace with actual data fetching)
  const event = {
    id: data.id,
    name: `Event ${data.title}`,
    description: `Description for Event ${data.description}`,
    dateTime: moment(new Date(data.date)).format('MMMM Do YYYY, h:mm:ss a'),
    location: data.location // Replace with actual dateTime
  };

  return (
    <>
      <CustomBackButton text="Event Detail" />
      <CustomWrapperView withScroll={false}>
        <View style={styles.container}>
          <CustomText style={styles.eventName}>{event.name}</CustomText>
          <CustomText>{event.dateTime}</CustomText>
          <CustomText>{event.location}</CustomText>
          <CustomText style={styles.description}>
            {event.description}
          </CustomText>
        </View>
      </CustomWrapperView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20
  },
  eventName: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10
  },
  description: {
    marginTop: 10
  }
});

export default EventDetailScreen;
