//import liraries
import React, { Component, useCallback, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity
} from 'react-native';
import CustomWrapperView from '../../components/CustomWrapperView';
import CustomBackButton from '../../components/CustomBackButton';
import useDialog from '../../components/hooks/useDialog';
import { RootState } from '../../store';
import { useDispatch, useSelector } from 'react-redux';
import { ICONS } from '../../utils/Icons';
import { setError, setEvents } from '../../store/events/EventSlice';
import LoadingIndicator from '../../components/LoadingIndicator';
import { fetchEvents } from '../../store/events/EventThunk';
import moment from 'moment';
import CustomText from '../../components/CustomText';
import { SizeUtils } from '../../utils/SizeUtils';

// create a component
const FeedScreen = ({ navigation }) => {
  const { events, loading, error } = useSelector(
    (state: RootState) => state.events
  );
  const dispatch = useDispatch();
  const {
    dialogVisible,
    dialogTitle,
    dialogBody,
    dialogImage,
    displayConfirmButton,
    showDialog,
    hideDialog
  } = useDialog();

  useEffect(() => {
    dispatch(fetchEvents({ page: 1, pageSize: 100 }));
  }, []);

  useEffect(() => {
    if (error) {
      showDialog('Error', error, ICONS.warningIcon);
    }
  }, [error]);

  const handleDialogDismiss = () => {
    hideDialog();

    if (!error) {
      navigation.goBack();
    }
    clearAllStuff();
  };
  const handleDialogOkay = useCallback(() => {
    hideDialog();
  }, [dispatch]);

  const clearAllStuff = () => {
    hideDialog();
    dispatch(setError(null));
    dispatch(setEvents(null));
  };

  const handleJoinEvent = (eventId) => {
    // Handle the join event logic here
    console.log('Joining event with id:', eventId);
    showDialog(
      'Event',
      'Do you want to join this event?',
      ICONS.warningIcon,
      true
    );
  };

  const renderItem = ({ item }) => (
    <View style={styles.eventItem}>
      <CustomText style={styles.eventTitle}>{item.title}</CustomText>
      <CustomText style={styles.eventDescription}>
        {item.description}
      </CustomText>
      <CustomText style={styles.eventDate}>
        {moment(new Date(item.date)).format('MMMM Do YYYY, h:mm:ss a')}
      </CustomText>
      <CustomText style={styles.eventLocation}>{item.location}</CustomText>
      <TouchableOpacity
        style={styles.joinButton}
        onPress={() => handleJoinEvent(item.id)}
      >
        <CustomText style={styles.joinButtonText}>Join</CustomText>
      </TouchableOpacity>
    </View>
  );

  return (
    <>
      <CustomBackButton text="All Events" />
      <CustomWrapperView
        dialogVisible={dialogVisible}
        dialogImage={dialogImage}
        dialogTitle={dialogTitle}
        dialogBody={dialogBody}
        onDialogDismiss={handleDialogDismiss}
        onDialogOkay={handleDialogOkay}
        displayConfirm={displayConfirmButton}
        withScroll={false}
      >
        <View style={styles.container}>
          <FlatList
            data={events || []}
            renderItem={renderItem}
            keyExtractor={(item) => item.id.toString()}
          />
        </View>
      </CustomWrapperView>

      {loading && <LoadingIndicator body="Loading All Events" />}
    </>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    backgroundColor: '#fff'
  },
  eventItem: {
    padding: 16,
    marginBottom: 16,
    backgroundColor: '#f8f8f8',
    borderRadius: 8,
    shadowColor: '#000',
    shadowOpacity: 0.1,
    shadowRadius: 4,
    elevation: 2
  },
  eventTitle: {
    fontSize: SizeUtils.scaleFont(14),
    fontWeight: 'bold',
    marginBottom: 8
  },
  eventDescription: {
    fontSize: SizeUtils.scaleFont(14),
    marginBottom: 8
  },
  eventDate: {
    fontSize: SizeUtils.scaleFont(14),
    color: '#666',
    marginBottom: 8
  },
  eventLocation: {
    fontSize: SizeUtils.scaleFont(14),
    color: '#666'
  },
  joinButton: {
    marginTop: 8,
    padding: 10,
    backgroundColor: 'green',
    borderRadius: 5,
    alignItems: 'center'
  },
  joinButtonText: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: SizeUtils.scaleFont(14)
  }
});

//make this component available to the app
export default FeedScreen;
