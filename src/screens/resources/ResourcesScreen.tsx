import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity,
  Linking
} from 'react-native';
import CustomBackButton from '../../components/CustomBackButton';
import CustomWrapperView from '../../components/CustomWrapperView';
import { SizeUtils } from '../../utils/SizeUtils';
import { IMAGES } from '../../utils/Images';

const ResourcesScreen = () => {
  // Sample data for water and air pollution resources
  const pollutionResources = [
    {
      id: 3,
      title: 'Causes of Water Pollution',
      description:
        'Understand the main causes of water pollution and their impacts.',
      link: 'https://www.nationalgeographic.com/environment/article/causes-of-water-pollution',
      image: IMAGES.waterIcon
    },
    {
      id: 4,
      title: 'Effects of Water Pollution on Marine Life',
      description:
        'Learn how water pollution affects marine ecosystems and species.',
      link: 'https://www.worldwildlife.org/threats/water-pollution',
      image: IMAGES.waterIcon
    },
    {
      id: 5,
      title: 'Health Effects of Air Pollution',
      description: 'Explore the health impacts of air pollution on humans.',
      link: 'https://www.health.ny.gov/environmental/indoors/air/pollution.htm',
      image: IMAGES.airIcon
    },
    {
      id: 6,
      title: 'Air Pollution and Climate Change',
      description:
        'Discover the link between air pollution and global climate change.',
      link: 'https://www.epa.gov/ghgemissions/sources-greenhouse-gas-emissions',
      image: IMAGES.airIcon
    },
    {
      id: 7,
      title: 'Preventing Water Pollution',
      description:
        'Strategies and methods to prevent water pollution in communities.',
      link: 'https://www.environment.gov.au/water/water-quality/urban-water/water-pollution',
      image: IMAGES.waterIcon
    },
    {
      id: 8,
      title: 'Air Pollution Control Technologies',
      description:
        'Overview of technologies used to control and reduce air pollution.',
      link: 'https://www.epa.gov/air-research/air-pollution-control-technologies',
      image: IMAGES.airIcon
    },
    {
      id: 9,
      title: 'Water Pollution Solutions',
      description:
        'Innovative solutions and practices to combat water pollution globally.',
      link: 'https://www.unwater.org/water-facts/water-pollution/',
      image: IMAGES.waterIcon
    }
    // Add more resources as needed
  ];

  const renderResource = ({ item }) => (
    <TouchableOpacity
      style={styles.resourceItem}
      onPress={() => Linking.openURL(item.link)}
    >
      <Image source={item.image} style={styles.resourceImage} />
      <View style={styles.resourceDetails}>
        <Text style={styles.resourceTitle}>{item.title}</Text>
        <Text style={styles.resourceDescription}>{item.description}</Text>
      </View>
    </TouchableOpacity>
  );

  return (
    <>
      <CustomBackButton text="Pollution Resources" />
      <CustomWrapperView withScroll={false}>
        <View style={styles.container}>
          <FlatList
            data={pollutionResources}
            renderItem={renderResource}
            keyExtractor={(item) => item.id.toString()}
            contentContainerStyle={{ paddingBottom: 20 }}
          />
        </View>
      </CustomWrapperView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: 20
  },
  resourceItem: {
    flexDirection: 'row',
    marginBottom: 20,
    backgroundColor: '#fff',
    borderRadius: 8,
    shadowColor: '#000',
    shadowOpacity: 0.1,
    shadowRadius: 4,
    shadowOffset: { width: 0, height: 2 },
    elevation: 2
  },
  resourceImage: {
    width: 120,
    height: 120,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    resizeMode: 'cover'
  },
  resourceDetails: {
    flex: 1,
    padding: 10
  },
  resourceTitle: {
    fontSize: SizeUtils.scaleFont(16),
    fontWeight: 'bold',
    marginBottom: 5
  },
  resourceDescription: {
    fontSize: SizeUtils.scaleFont(14),
    color: '#666'
  }
});

export default ResourcesScreen;
