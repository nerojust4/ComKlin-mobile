import React, { useState, useRef, useEffect, useCallback } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  FlatList,
  BackHandler
} from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from '../../types/RootStackParamList';
import { COLORS } from '../../utils/ColorUtils';
import CustomButton from '../../components/CustomButton';
import { SizeUtils } from '../../utils/SizeUtils';
import CustomCarousel from '../../components/CustomCarousel';
import CustomWrapperView from '../../components/CustomWrapperView';
import { Utils } from '../../utils/Utils';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../store';
import { setError } from '../../store/auth/AuthSlice';
import { ROUTE_LABELS } from '../../utils/AppConstants';
import useDialog from '../../components/hooks/useDialog';

const { width: screenWidth, height: screenHeight } = Dimensions.get('window');

interface CarouselItemData {
  uri: string;
  title: string;
  subtitle: string;
}

type OnboardingScreenProps = StackScreenProps<RootStackParamList, 'Onboarding'>;

const OnboardingScreen: React.FC<OnboardingScreenProps> = ({ navigation }) => {
  const [activeSlide, setActiveSlide] = useState(0);
  const flatListRef = useRef<FlatList<CarouselItemData>>(null);
  const dispatch = useDispatch();
  // const {
  //   loading,
  //   error,
  //   config: data
  // } = useSelector((state: RootState) => state.config);

  const {
    dialogVisible,
    dialogTitle,
    dialogBody,
    dialogImage,
    displayConfirmButton,
    showDialog,
    hideDialog
  } = useDialog();

  useEffect(() => {
    // dispatch(fetchAppConfig({ page: 1, pageSize: 50 }));
    // dispatch(fetchRoles({}));
  }, []);

  useEffect(() => {
    const interval = setInterval(() => {
      const nextIndex = (activeSlide + 1) % ([])?.length;
      setActiveSlide(nextIndex);
      flatListRef.current?.scrollToIndex({ index: nextIndex, animated: true });
    }, 3000);

    return () => clearInterval(interval);
  }, [activeSlide]);

  //we do not want to display anythin on the UI
  // useEffect(() => {
  //   if (authError) {
  //     showDialog('Error', authError, ICONS.warningIcon, false);
  //   }
  // }, [authError]);

  const handleDialogDismiss = () => {
    hideDialog();
    dispatch(setError(null)); // Clear the error state
  };

  return (
    <CustomWrapperView
      dialogVisible={dialogVisible}
      dialogImage={dialogImage}
      dialogTitle={dialogTitle}
      dialogBody={dialogBody}
      onDialogDismiss={handleDialogDismiss}
      displayConfirm={displayConfirmButton}
    >
      <View style={styles.container}>
        <View style={styles.carouselContainer}>
          <CustomCarousel 
          // data={data}
          // loading={loading}
          //  error={error}
            />
        </View>

        <CustomButton
          title="Log In"
          onPress={() =>
            Utils.goToNewScreen(
              navigation,
              ROUTE_LABELS.LOGIN,
              {},
              false,
              false
            )
          }
          buttonStyle={[styles.button, styles.loginButton]}
          textStyle={styles.loginText}
        />
        <CustomButton
          title="Sign Up"
          onPress={() => Utils.goToNewScreen(navigation, ROUTE_LABELS.REGISTER)}
          buttonStyle={[styles.button, styles.signupButton]}
          textStyle={styles.signupText}
        />
      </View>
    </CustomWrapperView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:COLORS.white
  },
  carouselContainer: {
    alignItems: 'center',
    height: screenHeight * 0.7
  },
  flatList: {
    width: screenWidth
  },
  button: {
    width: '80%',
    padding: 15,
    borderRadius: 25,
    marginVertical: 10,
    alignItems: 'center'
  },
  loginButton: {
    borderColor: COLORS.sbsDarkenColor(COLORS.green, 30),
    borderWidth: 0.5,
    marginTop: 30,
    color: COLORS.white
  },
  signupButton: {
    backgroundColor: COLORS.sbsDarkenColor(COLORS.green, 30)
  },
  loginText: {
    color: COLORS.sbsDarkenColor(COLORS.green, 30),
    fontSize: SizeUtils.scaleFont(14)
  },
  signupText: {
    color: COLORS.white,
    fontSize: SizeUtils.scaleFont(14)
  }
});

export default OnboardingScreen;
