import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Button,
  Image,
  Platform,
  PermissionsAndroid,
  TouchableOpacity,
  Alert
} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import ImagePicker from 'react-native-image-picker';
import CustomBackButton from '../../components/CustomBackButton';
import CustomWrapperView from '../../components/CustomWrapperView';
import CustomTextInput from '../../components/CustomTextInput';
import CustomButton from '../../components/CustomButton';
import { COLORS } from '../../utils/ColorUtils';
import { SizeUtils } from '../../utils/SizeUtils';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../store';
import CustomText from '../../components/CustomText';
import { IMAGES } from '../../utils/Images';
import moment from 'moment';
import LoadingIndicator from '../../components/LoadingIndicator';
import { joinEvent } from '../../store/events/EventThunk';
import useDialog from '../../components/hooks/useDialog';
import { ICONS } from '../../utils/Icons';
import { setError, setEvent } from '../../store/events/EventSlice';
import { dismissKeyboard } from '../../utils/Utils';

const JoinEventScreen = ({ navigation }) => {
  const [eventName, setEventName] = useState('');
  const [eventDescription, setEventDescription] = useState('');
  const [eventDateTime, setEventDateTime] = useState(null);
  const [isDateTimePickerVisible, setIsDateTimePickerVisible] = useState(false);
  const [images, setImages] = useState([]);
  const [videos, setVideos] = useState([]);
  const dispatch = useDispatch();
  const { event, loading, error } = useSelector(
    (state: RootState) => state.events
  );
  const {
    dialogVisible,
    dialogTitle,
    dialogBody,
    dialogImage,
    displayConfirmButton,
    showDialog,
    hideDialog
  } = useDialog();

  useEffect(() => {
    if (error) {
      showDialog('Error', error, ICONS.warningIcon);
    }
  }, [error]);

  useEffect(() => {
    if (!loading && !error && event) {
      showDialog('Success', 'Successfully Joined Event', ICONS.successIcon);
    }
  }, [loading, error, event]);

  const showDateTimePicker = () => {
    setIsDateTimePickerVisible(true);
  };

  const handleDateTimeConfirm = (date) => {
    console.log('Date is', date);
    setEventDateTime(date);
    setIsDateTimePickerVisible(false);
  };

  const handleDialogDismiss = () => {
    if (event) {
      navigation.goBack();
    }
    clearAllStuff();
  };

  const clearAllStuff = () => {
    hideDialog();
    dispatch(setError(null));
    dispatch(setEvent(null));
  };

  const handleJoinEvent = () => {
    dismissKeyboard();
    // Validate basic class details

    const eventPayload = {
      title: eventName,
      description: eventDescription,
      date: eventDateTime
    };
    
    if (Object.values(eventPayload).some((value) => !value)) {
      dispatch(setError('All fields are required'));
      return;
    }

    dispatch(joinEvent(eventPayload));
  };

  return (
    <>
      <CustomBackButton text="Join Event" />
      <CustomWrapperView
        dialogVisible={dialogVisible}
        dialogImage={dialogImage}
        dialogTitle={dialogTitle}
        dialogBody={dialogBody}
        onDialogDismiss={handleDialogDismiss}
      >
        <View style={styles.container}>
          <CustomTextInput
            label="Event Name"
            placeholder="Enter event name"
            value={eventName}
            onChangeText={setEventName}
            moreStyles={styles.input}
          />

          <CustomTextInput
            label="Event Description"
            placeholder="Enter description"
            value={eventDescription}
            onChangeText={setEventDescription}
            multiline
            moreStyles={styles.input}
          />

          <CustomText style={styles.labelTitle}>Event Start Date</CustomText>
          <TouchableOpacity
            onPress={showDateTimePicker}
            activeOpacity={0.6}
            style={styles.selectedStyle}
          >
            <View style={styles.row}>
              <CustomText style={{ fontSize: SizeUtils.scaleFont(14) }}>
                {eventDateTime
                  ? moment(new Date(eventDateTime)).format(
                      'MMMM Do YYYY, h:mm:ss a'
                    )
                  : 'Select a date and time'}
              </CustomText>
              <Image source={IMAGES.arrowDownIcon} style={styles.arrowIcon} />
            </View>
          </TouchableOpacity>

          <DateTimePickerModal
            isVisible={isDateTimePickerVisible}
            mode="datetime"
            onConfirm={handleDateTimeConfirm}
            onCancel={() => setIsDateTimePickerVisible(false)}
          />

          <CustomButton
            title="Join"
            onPress={handleJoinEvent}
            buttonStyle={styles.submitButton}
            textStyle={styles.submitButtonText}
            disabled={loading}
          />
        </View>
      </CustomWrapperView>

      {loading && <LoadingIndicator body="Joining Event" />}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 25
  },
  input: {
    marginBottom: 10
  },
  labelTitle: {
    color: COLORS.sbsDarkenColor(COLORS.lightGray, 20),
    fontSize: SizeUtils.scaleFont(13)
  },
  selectedStyle: {
    borderWidth: 0.5,
    height: SizeUtils.pxToDp(60),
    width: '100%',
    borderColor: COLORS.sbsDarkenColor(COLORS.lightGray, 20),
    borderRadius: 8,
    paddingHorizontal: 16,
    marginRight: 10,
    paddingVertical: 18,
    fontSize: SizeUtils.scaleFont(14),
    color: COLORS.gray,
    marginTop: 5
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  arrowIcon: {
    marginLeft: 10,
    tintColor: COLORS.sbsDarkenColor(COLORS.lightGray, 10),
    width: 15,
    height: 15
  },
  uploadButton: {
    marginBottom: 10,
    backgroundColor: COLORS.primaryColor,
    color: COLORS.white,
    height: 50
  },
  mediaPreview: {
    width: 100,
    height: 100,
    marginTop: 10,
    resizeMode: 'cover'
  },
  videoText: {
    marginTop: 10,
    color: COLORS.primaryColor
  },
  submitButton: {
    marginBottom: 10,
    backgroundColor: COLORS.sbsDarkenColor(COLORS.green, 30),
    marginTop: 30
  },
  submitButtonText: {
    color: COLORS.white,
    fontSize: SizeUtils.scaleFont(14)
  }
});

export default JoinEventScreen;
