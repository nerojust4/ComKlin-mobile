import React from 'react';
import { View, Text, StyleSheet, Image, FlatList } from 'react-native';
import { useSelector } from 'react-redux';
import { RootState } from '../../store';
import CustomBackButton from '../../components/CustomBackButton';
import CustomWrapperView from '../../components/CustomWrapperView';
import LoadingIndicator from '../../components/LoadingIndicator';
import CustomText from '../../components/CustomText';
import { SizeUtils } from '../../utils/SizeUtils';
import { IMAGES } from '../../utils/Images';
import { COLORS } from '../../utils/ColorUtils';
import moment from 'moment';

const ProfileScreen = () => {
  const user = useSelector((state: RootState) => state.auth.user);
  const events = useSelector((state: RootState) => state.events.events); // Adjust according to your state structure

  // Function to format event date for grouping
  const formatDateForGrouping = (date) => {
    const eventDate = new Date(date);
    const today = new Date();
    if (
      eventDate.getDate() === today.getDate() &&
      eventDate.getMonth() === today.getMonth() &&
      eventDate.getFullYear() === today.getFullYear()
    ) {
      return 'Today';
    } else {
      return eventDate.toLocaleDateString(); // Adjust formatting as needed
    }
  };

  // Group events by date
  const groupedEvents = events.reduce((groups, event) => {
    const date = formatDateForGrouping(event.date);
    if (!groups[date]) {
      groups[date] = [];
    }
    groups[date].push(event);
    return groups;
  }, {});

  const renderEvent = ({ item }) => (
    <View style={styles.eventItem}>
      <CustomText style={styles.eventTitle}>{item.title}</CustomText>
      <CustomText style={styles.eventDate}>
        {moment(new Date(item.date)).format('MMMM Do YYYY, h:mm:ss a')}
      </CustomText>
      <CustomText style={styles.eventDescription}>
        {item.description}
      </CustomText>
    </View>
  );

  return (
    <>
      <CustomBackButton text="User Profile" />
      <CustomWrapperView withScroll={false}>
        <View style={styles.profileContainer}>
          <Image
            source={{
              uri:
                user.profileImage ||
                'https://newprofilepic.photo-cdn.net//assets/images/article/profile.jpg?90af0c8'
            }}
            style={styles.profileImage}
          />
          <View style={styles.detailsContainer}>
            <CustomText style={styles.label}>
              Name:{' '}
              <CustomText style={styles.value}>
                {user?.firstName} {user?.lastName}
              </CustomText>
            </CustomText>
            <CustomText style={styles.label}>
              Username:{' '}
              <CustomText style={styles.value}>{user?.username}</CustomText>
            </CustomText>
            <CustomText style={styles.label}>
              Email: <CustomText style={styles.value}>{user?.email}</CustomText>
            </CustomText>

            <View style={{ flexDirection: 'row' }}>
              <CustomText style={styles.label}>
                Badge:{' '}
                <CustomText style={styles.value}>
                  {user?.badge?.name}
                </CustomText>
              </CustomText>
              <Image source={IMAGES.badgeIcon} style={styles.badgeImage} />
            </View>
            {/* Add more user details as needed */}
          </View>
        </View>

        <View style={styles.activitiesContainer}>
          <CustomText style={styles.activitiesTitle}>My Activities</CustomText>
          {Object.keys(groupedEvents).length > 0 ? (
            <FlatList
              data={Object.keys(groupedEvents)}
              renderItem={({ item }) => (
                <>
                  <CustomText style={styles.eventDate}>{item}</CustomText>
                  <FlatList
                    data={groupedEvents[item]}
                    renderItem={renderEvent}
                    keyExtractor={(event) => event.id.toString()}
                  />
                </>
              )}
              keyExtractor={(item) => item}
            />
          ) : (
            <CustomText style={styles.noActivityText}>
              No activity found
            </CustomText>
          )}
        </View>
      </CustomWrapperView>
    </>
  );
};

const styles = StyleSheet.create({
  profileContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 16,
    backgroundColor: '#fff'
  },
  profileImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginRight: 16
  },
  badgeImage: {
    width: 25,
    height: 25,
    borderRadius: 50,
    tintColor: COLORS.green
  },
  detailsContainer: {
    flex: 1
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 8
  },
  label: {
    fontSize: SizeUtils.scaleFont(14),
    marginBottom: 4
  },
  value: {
    fontWeight: 'normal',
    fontSize: SizeUtils.scaleFont(14)
  },
  activitiesContainer: {
    flex: 1,
    padding: 16,
    backgroundColor: '#f9f9f9'
  },
  activitiesTitle: {
    fontSize: 22,
    fontWeight: 'bold',
    marginBottom: 16
  },
  eventItem: {
    padding: 16,
    backgroundColor: '#fff',
    borderRadius: 8,
    marginBottom: 8,
    shadowColor: '#000',
    shadowOpacity: 0.1,
    shadowRadius: 4,
    shadowOffset: { width: 0, height: 2 },
    elevation: 2
  },
  eventTitle: {
    fontSize: SizeUtils.scaleFont(14),
    fontWeight: 'bold'
  },
  eventDate: {
    fontSize: SizeUtils.scaleFont(14),
    color: '#888',
    marginVertical: 4
  },
  eventDescription: {
    fontSize: SizeUtils.scaleFont(14)
  },
  noActivityText: {
    fontSize: SizeUtils.scaleFont(14),
    color: '#888',
    textAlign: 'center',
    marginTop: 16
  }
});

export default ProfileScreen;
