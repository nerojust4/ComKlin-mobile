// src/services/api.ts
import axios from 'axios';
import {navigationRef, reset} from '../navigation/NavigationService';
import {clearUser, setError} from '../store/auth/AuthSlice';
import {ROUTE_LABELS} from '../utils/AppConstants';
import {DeviceInfoWrapper} from '../utils/DeviceUtils';
import DeviceInfo from 'react-native-device-info';
import {logout} from '../store/auth/AuthThunk';
import store, { RootState } from '../store';

const api = axios.create({
  baseURL: 'https://largely-summary-elk.ngrok-free.app/api/v1',
});

// Request interceptor to encrypt data
api.interceptors.request.use(
  async config => {
    const state: RootState = store.getState();
    const token = state?.auth.token;
    // console.log('Gotten token', token);
    if (token) {
      config.headers['Authorization'] = `Bearer ${token}`;
    }

    // Add custom headers
    // config.headers['client'] = DeviceInfoWrapper.getPhoneType();
    (config.headers['deviceId'] = DeviceInfo.getDeviceId()),
      (config.headers['appVersion'] = DeviceInfo.getVersion());
    config.headers['phoneType'] = DeviceInfo.getDeviceType();
    config.headers['model'] = DeviceInfoWrapper.getModel();
    (config.headers['systemName'] = DeviceInfoWrapper.getSystemName()),
      (config.headers['systemVersion'] = DeviceInfoWrapper.getSystemVersion());
    config.headers['bundleId'] = DeviceInfoWrapper.getBundleId();

    return config;
  },
  error => {
    return Promise.reject(error);
  },
);

// Response interceptor to decrypt data and handle token expiration
api.interceptors.response.use(
  response => {
    if (response.data) {
      // response.data = decryptData(response.data);
    }
    return response;
  },
  error => {
    console.log('error api level', error.response.status);
    if (error.response) {
      if (
        error.response.status === 401
        // || error.response.status === 403
      ) {
        // Dispatch an action to clear the token
        store.dispatch(logout());
        store.dispatch(setError('Unauthorized to access this resource'));
        // Redirect to login screen
        reset(ROUTE_LABELS.LOGIN);
      } else if (
        error.response.status === 500 ||
        error.response.status === 502
      ) {
        // Handle 500 errors
        console.log('Server error occurred');
        // Dispatch an action to set an appropriate error message
        // store.dispatch(
        //   setError('Server error occurred, please try again later'),
        // );
        // Optionally, display an error message to the user
        return Promise.reject(error); // Optionally, you can reject the promise to propagate the error further
      }
    }
    return Promise.reject(error);
  },
);

export default api;
