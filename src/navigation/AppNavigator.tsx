// src/navigation/AppNavigator.tsx
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import {RootState} from '../store';
import AuthNavigator from './AuthNavigator';
import HomeNavigator from './HomeNavigator';
import {navigationRef} from './NavigationService';
import SessionTimeoutHandler from '../../src/components/SessionTimeoutHandler';

const AppNavigator = () => {
  const token = useSelector((state: RootState) => state.auth.token);
  // console.log("app nav data",token)
  return (
    <NavigationContainer ref={navigationRef}>
      <SessionTimeoutHandler>
        {token ? <HomeNavigator /> : <AuthNavigator />}
      </SessionTimeoutHandler>
    </NavigationContainer>
  );
};

export default AppNavigator;
