// src/navigation/AuthNavigator.tsx
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LoginScreen from '../screens/auth/login/LoginScreen';
import RegisterScreen from '../screens/auth/register/RegisterScreen';
import OnboardingScreen from '../screens/onboarding/OnboardingScreen';
import ResetPasswordScreen from '../screens/auth/reset/ResetPassword';
import {COLORS} from '../utils/ColorUtils'; // Import your color utils
import {ROUTE_LABELS} from '../utils/AppConstants';

const Stack = createStackNavigator();

const AuthNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName={ROUTE_LABELS.ONBOARDING}
      screenOptions={{
        headerStyle: {
          backgroundColor: COLORS.sbsRed, // Customize header background color
        },
        headerTintColor: COLORS.white, // Customize header text color
        headerTitleStyle: {
          fontWeight: 'bold', // Customize header text style
        },
        headerBackTitleVisible: false, // Optionally hide back button text
        headerShown: false,
      }}>
      <Stack.Screen
        name={ROUTE_LABELS.ONBOARDING}
        component={OnboardingScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={ROUTE_LABELS.LOGIN}
        component={LoginScreen}
        // options={{title: 'Log In'}}
      />
      <Stack.Screen
        name={ROUTE_LABELS.REGISTER}
        component={RegisterScreen}
        // options={{title: 'Sign Up'}} // Customize header title for this screen
      />
      <Stack.Screen
        name={ROUTE_LABELS.RESET_PASSWORD}
        component={ResetPasswordScreen}
        // options={{title: 'Reset Password'}} // Customize header title for this screen
      />
    </Stack.Navigator>
  );
};

export default AuthNavigator;
