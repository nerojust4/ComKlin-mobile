// src/navigator/AppNavigator.js

import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerContent from '../components/DrawerContent';
import DashboardScreen from '../screens/home/dashboard/DashboardScreen';
import { ROUTE_LABELS } from '../utils/AppConstants';
import RegisterScreen from '../screens/auth/register/RegisterScreen';
import { useSelector } from 'react-redux';
import { RootState } from '../store';
import CreateCleaningScreen from '../screens/cleaning_activity/CreateCleaningScreen';
import EventDetailScreen from '../screens/home/events/EventDetailScreen';
import EventListScreen from '../screens/home/events/EventListScreen';
import ProfileScreen from '../screens/profile/ProfileScreen';
import SettingsScreen from '../screens/settings/SettingsScreen';
import MessagesScreen from '../screens/messages/MessagesScreen';
import JoinScreen from '../screens/join/JoinScreen';
import FeedScreen from '../screens/feed/FeedScreen';
import NotificationScreen from '../screens/notification/NotificationScreen';
import PollutionResourcesScreen from '../screens/resources/ResourcesScreen';
import ResourcesScreen from '../screens/resources/ResourcesScreen';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const MainStack = () => {
  const { user } = useSelector((state: RootState) => state.auth);
  const allowedFeatures = user?.features || [];

  return (
    <Stack.Navigator>
      <Stack.Screen
        name={ROUTE_LABELS.HOME}
        component={DashboardScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={ROUTE_LABELS.CREATE_CLEANING}
        component={CreateCleaningScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={ROUTE_LABELS.EVENT_LIST}
        component={EventListScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={ROUTE_LABELS.PROFILE}
        component={ProfileScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={ROUTE_LABELS.SETTINGS}
        component={SettingsScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={ROUTE_LABELS.EVENT_DETAIL}
        component={EventDetailScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={ROUTE_LABELS.MESSAGE}
        component={MessagesScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={ROUTE_LABELS.JOIN}
        component={JoinScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={ROUTE_LABELS.FEED}
        component={FeedScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={ROUTE_LABELS.NOTIFICATION}
        component={NotificationScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={ROUTE_LABELS.RESOURCES}
        component={ResourcesScreen}
        options={{ headerShown: false }}
      />

      {/* {allowedFeatures.includes(ROUTE_LABELS.CREATE_USER) && ( */}
        <Stack.Screen
          name={ROUTE_LABELS.CREATE_USER}
          component={RegisterScreen}
          options={{ headerShown: false }}
        />
      {/* )} */}
    </Stack.Navigator>
  );
};

const HomeNavigator = () => {
  return (
    <Drawer.Navigator
      drawerWidth={250}
      drawerType="slide"
      drawerPosition="left"
      drawerOverlay={true}
      drawerStatusBarAnimation="slide"
      drawerContent={(props) => <DrawerContent {...props} />}
    >
      <Drawer.Screen
        name={ROUTE_LABELS.MAIN}
        component={MainStack}
        options={{
          headerShown: false
        }}
      />
    </Drawer.Navigator>
  );
};

const AppNavigator = () => {
  return <MainStack />;
};

export default AppNavigator;
