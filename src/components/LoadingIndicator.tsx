// src/components/LoadingIndicator.tsx
import React from 'react';
import {View, ActivityIndicator, StyleSheet} from 'react-native';
import {COLORS} from '../utils/ColorUtils';
import CustomText from './CustomText';

interface LoaderProps {
  title?: string;
  body?: string;
}

const LoadingIndicator: React.FC<LoaderProps> = ({
  title = 'Loading',
  body = 'Please wait...',
}) => {
  return (
    <View style={styles.container}>
      <ActivityIndicator size={60} color={COLORS.green} />
      <CustomText weightType="bold" fontType="monserrat" style={styles.titleText}>
        {title}
      </CustomText>
      <CustomText fontType='montserrat' weightType='regular' style={styles.bodyText}>{body}</CustomText>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.80)',
  },
  titleText: {
    marginVertical: 20,
  },
  bodyText: {
    // marginTop: 10,
  },
});

export default LoadingIndicator;
