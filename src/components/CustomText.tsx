import React from 'react';
import {Text, StyleSheet, TextStyle, StyleProp, TextProps} from 'react-native';
import {COLORS} from '../utils/ColorUtils';
import {SizeUtils} from '../utils/SizeUtils';

interface CustomTextProps extends TextProps {
  weightType?: 'bold' | 'medium' | 'regular'; // These will be used to set the font families later based on weight
  isHyperlink?: boolean; // if text is of hyperlink type
  leftIcon?: any;
  fontType?: 'montserrat' | 'roboto' | 'productSans' | 'averta'; // Font type
}

const CustomText: React.FC<CustomTextProps> = ({
  children,
  style,
  onPress,
  leftIcon,
  weightType = 'medium',
  isHyperlink,
  fontType = 'montserrat', // Default font type
  ...rest
}) => {
  // Font maps for different font types
  const fontMaps = {
    montserrat: {
      bold: 'Montserrat-Bold',
      medium: 'Montserrat-Medium',
      regular: 'Montserrat-Regular',
    },
    roboto: {
      bold: 'roboto-bold',
      medium: 'roboto_medium',
      regular: 'roboto_regular',
    },
    productSans: {
      bold: 'ProductSans-Bold',
      regular: 'ProductSans-Regular',
    },
    averta: {
      bold: 'Averta-Bold',
      regular: 'Averta-Regular',
    },
  };

  // Get the font map based on the font type
  const selectedFontMap = fontMaps[fontType] || fontMaps['montserrat'];

  const defaultFontSize = SizeUtils.scaleFont(16);

  return (
    <>
      {leftIcon}
      <Text
        style={[
          styles.text,
          {
            fontFamily: selectedFontMap[weightType],
            fontSize: defaultFontSize,
          },
          isHyperlink && {...styles.hyperlink, fontFamily: selectedFontMap.bold},
          style,
        ]}
        onPress={onPress}
        {...rest}>
        {children}
      </Text>
    </>
  );
};

const styles = StyleSheet.create({
  text: {
    color: COLORS.darkAsh,
  },
  hyperlink: {
    color: COLORS.fcmbPurple,
    textDecorationLine: 'underline',
  },
});

export default CustomText;
