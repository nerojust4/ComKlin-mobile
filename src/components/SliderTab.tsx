import React, {useState, useRef, useCallback} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Animated,
  StyleSheet,
  Image,
  StyleProp,
  ViewStyle,
  ViewProps,
  Platform,
  Pressable,
  Easing,
} from 'react-native';
import {COLORS, ColorUtils} from '../utils/ColorUtils';
import CustomText from './CustomText';
import LinearGradient from 'react-native-linear-gradient';
import {SizeUtils} from '../utils/SizeUtils';

interface TabOption {
  id: number;
  name: string;
}

interface SliderTabProps {
  tabs: TabOption[];
  currentTab: TabOption;
  onSelect: (tab: TabOption) => void;
  tabStyle?: StyleProp<ViewStyle>;
  containerStyle?: StyleProp<ViewStyle>;
}

const SliderTab: React.FC<SliderTabProps> = ({
  tabs,
  currentTab,
  onSelect,
  tabStyle,
  containerStyle,
}) => {
  const translateMainView = useRef(new Animated.Value(0)).current;

  const onTranslate = () => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(translateMainView, {
          toValue: 5,
          duration: 100,
          useNativeDriver: true,
          easing: Easing.ease,
        }),
        Animated.timing(translateMainView, {
          toValue: 0,
          duration: 100,
          useNativeDriver: true,
          easing: Easing.ease,
        }),
      ]),
      {iterations: 2},
    ).start();
  };

  const handleActiveSelect = (tab: TabOption) => {
    onTranslate();
    onSelect(tab);
  };

  const isActive = (tab: TabOption) => tab.id === currentTab.id;

  const ActiveTab = useCallback(
    (tab: TabOption) => {
      return (
        <TouchableOpacity
          style={styles.shadowStyle}
          onPress={() => onSelect(tab)}
          key={tab.id}>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={[
              COLORS.sbsMediumDarkRed,
              COLORS.sbsRed,
              COLORS.sbsMediumDarkRed,
            ]}
            style={[styles.active, tabStyle]}>
            <CustomText weightType="medium" style={styles.activeText}>
              {tab.name}
            </CustomText>
          </LinearGradient>
        </TouchableOpacity>
      );
    },
    [tabs],
  );

  const InActiveTab = useCallback(
    (tab: TabOption) => {
      return (
        <TouchableOpacity
          onPress={() => handleActiveSelect(tab)}
          key={tab.id}
          style={styles.inactive}>
          <CustomText weightType="regular" fontType='productSans' style={styles.inactiveText}>
            {tab.name}
          </CustomText>
        </TouchableOpacity>
      );
    },
    [tabs],
  );

  return (
    <Animated.View
      style={[
        styles.container,
        containerStyle,
        {
          transform: [
            {
              translateX: translateMainView,
            },
          ],
        },
      ]}>
      {tabs?.map(tab => (isActive(tab) ? ActiveTab(tab) : InActiveTab(tab)))}
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: COLORS.lightShadePink,
    borderRadius: 30,
    alignItems: 'center',
    flexDirection: 'row',
    padding: 5,
  },
  active: {
    zIndex: 2,
    height: 39,
    flex: 1,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inactive: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 39,
  },
  activeText: {
    color: COLORS.white,
    fontSize: SizeUtils.scaleFont(12),
    fontWeight: '500',
  },
  inactiveText: {
    color: COLORS.sbsDarkRed,
    fontSize: SizeUtils.scaleFont(12),
  },
  shadowStyle: {
    elevation: 8,
    shadowColor: COLORS.sbsDarkRed,
    shadowOpacity: 0.3,
    shadowOffset: Platform.select({ios: {height: 2, width: 5}}),
    shadowRadius: 10,
    flex: 1,
  },
});

export default SliderTab;
