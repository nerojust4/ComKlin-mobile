import React from 'react';
import {View, StyleSheet} from 'react-native';
import Svg, {Rect, Line, Text as SvgText} from 'react-native-svg';

const BarChart = ({data}) => {
  // Example data array
  // You can replace this with your own data array
  // For example, [10, 20, 15, 30, 25]
  const chartData = data || [50, 70, 90, 60, 80];

  const barWidth = 20;
  const chartHeight = 150;
  const barSpacing = 30;
  const labelOffset = 20;

  // Calculate the chart width based on data length and bar width/spacing
  const chartWidth = chartData.length * (barWidth + barSpacing);

  // Calculate the maximum value to scale the bars
  const maxValue = Math.max(...chartData);

  // Calculate the y-axis scale factor to fit the bars within the chart height
  const scaleY = chartHeight / maxValue;

  // Create an array for x-axis labels (assuming 1-based indexing for simplicity)
  const labels = Array.from(
    {length: chartData.length},
    (_, i) => `Label ${i + 1}`,
  );

  return (
    <View style={styles.container}>
      <Svg width={chartWidth} height={chartHeight + labelOffset}>
        {/* Draw x-axis */}
        <Line
          x1="0"
          y1={chartHeight}
          x2={chartWidth}
          y2={chartHeight}
          stroke="black"
          strokeWidth="1"
        />

        {/* Draw y-axis */}
        <Line
          x1="0"
          y1="0"
          x2="0"
          y2={chartHeight}
          stroke="black"
          strokeWidth="1"
        />

        {/* Draw bars */}
        {chartData.map((value, index) => {
          const barHeight = value * scaleY;
          const x = index * (barWidth + barSpacing) + barSpacing / 2;
          const y = chartHeight - barHeight;
          return (
            <Rect
              key={index}
              x={x}
              y={y}
              width={barWidth}
              height={barHeight}
              fill="green"
            />
          );
        })}

        {/* Draw x-axis labels */}
        {labels.map((label, index) => (
          <SvgText
            key={index}
            x={index * (barWidth + barSpacing) + barWidth / 2 + barSpacing / 2}
            y={chartHeight + labelOffset - 5}
            fontSize="12"
            textAnchor="middle">
            {label}
          </SvgText>
        ))}

        {/* Draw y-axis labels (assuming vertical bars) */}
        {chartData.map((value, index) => (
          <SvgText
            key={index}
            x="-10"
            y={chartHeight - value * scaleY}
            fontSize="12"
            textAnchor="end"
            alignmentBaseline="middle">
            {value}
          </SvgText>
        ))}
      </Svg>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20,
  },
});

export default BarChart;
