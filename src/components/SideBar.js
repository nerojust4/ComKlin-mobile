import React from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {IMAGES} from '../utils/Images';
import {COLORS} from '../utils/ColorUtils';
import CustomText from './CustomText';
import {SizeUtils} from '../utils/SizeUtils';
import {ROUTE_LABELS} from '../utils/AppConstants';

const Sidebar = () => {
  const SIDEBAR_ITEMS = [
    {
      icon: IMAGES.notificationIcon,
      label: 'Notification',
      route: ROUTE_LABELS.HOME,
    },
    {
      icon: IMAGES.profileIcon,
      label: 'Profile',
      route: ROUTE_LABELS.PROFILE,
    },
    {
      icon: IMAGES.feedIcon,
      label: 'Event Feed',
      route: ROUTE_LABELS.FEED,
    },
    {
      route: 'CreateActivity',
      icon: IMAGES.addIcon,
      label: 'Create Activity',
      route: ROUTE_LABELS.CREATE_CLEANING,
    },
    {
      route: 'JoinActivity',
      icon: IMAGES.joinIcon,
      label: 'Join',
      route: ROUTE_LABELS.FEED,
    },
    {
      route: 'Messages',
      icon: IMAGES.messageIcon,
      label: 'Messages',
      route: ROUTE_LABELS.MESSAGE,
    },
    {
      route: 'Resources',
      icon: IMAGES.resourceIcon,
      label: 'Resources',
      route: ROUTE_LABELS.RESOURCES,
    },
    {
      route: 'Settings',
      icon: IMAGES.settingsIcon,
      label: 'Settings',
      route: ROUTE_LABELS.SETTINGS,
    },
    {
      route: 'Logout',
      icon: IMAGES.logoutIcon,
      label: 'Logout',
      route: ROUTE_LABELS.CREATE_CLEANING,
    },
  ];

  const navigation = useNavigation();

  return (
    <View style={styles.sidebar}>
      {SIDEBAR_ITEMS.map((item, index) => (
        <TouchableOpacity
          key={index}
          onPress={() => navigation.navigate(item.route)}
          style={{alignItems: 'center', paddingHorizontal: 5}}>
          <Image source={item.icon} style={styles.icon} />
          <CustomText style={styles.iconText}>{item.label}</CustomText>
        </TouchableOpacity>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  sidebar: {
    width: 'auto',
    backgroundColor: '#f4f4f4',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingVertical: 20,
  },
  icon: {
    width: 25,
    height: 25,
    // marginBottom: 5,
    tintColor: COLORS.sbsDarkenColor(COLORS.green, 30),
    alignContent: 'center',
    alignItems: 'center',
  },
  iconText: {
    textAlign: 'center',
    fontSize: SizeUtils.scaleFont(11),
    paddingTop: 7,
  },
});

export default Sidebar;
