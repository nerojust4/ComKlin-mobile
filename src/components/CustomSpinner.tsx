// src/components/Spinner.tsx
import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Modal, Image} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import { COLORS } from '../utils/ColorUtils';
import { SizeUtils } from '../utils/SizeUtils';
import CustomText from './CustomText';

interface SpinnerProps {
  label: string;
  selectedValue: string;
  onValueChange: (itemValue: string, itemIndex: number) => void;
  options: {label: string; value: string}[];
  leftIcon?: React.ReactNode;
  moreStyles?: object;
}

const Spinner: React.FC<SpinnerProps> = ({
  label,
  selectedValue,
  onValueChange,
  options,
  leftIcon,
  moreStyles,
}) => {
  const [modalVisible, setModalVisible] = useState(false);

  return (
    <View style={[styles.container, moreStyles]}>
      <CustomText style={styles.label}>{label}</CustomText>
      <TouchableOpacity
        style={styles.inputContainer}
        onPress={() => setModalVisible(true)}>
        {leftIcon && <View style={styles.iconContainer}>{leftIcon}</View>}
        <CustomText style={styles.selectedItem}>{selectedValue}</CustomText>
      </TouchableOpacity>
      <Modal
        animationType='fade'
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => setModalVisible(false)}>
        <View style={styles.modalContainer}>
          <View style={styles.modalInnerContainer}>
            <Picker
              selectedValue={selectedValue}
              style={styles.picker}
              onValueChange={(itemValue, itemIndex) => {
                onValueChange(itemValue, itemIndex);
                setModalVisible(false);
              }}>
              {options.map((option, index) => (
                <Picker.Item key={index} label={option.label} value={option.label} />
              ))}
            </Picker>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginVertical: 10,
  },
  label: {
    fontSize: SizeUtils.scaleFont(13),
    marginBottom: 5,
    color: COLORS.lightGray5,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 0.3,
    height: SizeUtils.pxToDp(60),
    borderColor: COLORS.sbsDarkenColor(COLORS.lightGray, 20),
    borderRadius: 8,
    paddingHorizontal: 16,
    backgroundColor: COLORS.fadePinkBackground,
  },
  iconContainer: {
    marginRight: 12,
  },
  selectedItem: {
    flex: 1,
    fontSize: SizeUtils.scaleFont(14),
    color: COLORS.black,
    fontFamily: 'Montserrat-Regular',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalInnerContainer: {
    backgroundColor: 'white',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  picker: {
    height: 200, // Adjust as needed
  },
});

export default Spinner;
