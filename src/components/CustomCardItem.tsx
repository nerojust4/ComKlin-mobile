import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const ManageSchool = () => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Icon name="menu" size={30} color="#000" />
        <Text style={styles.headerText}>Manage School</Text>
        <Icon name="add" size={30} color="#000" />
        <Icon name="notifications-none" size={30} color="#000" />
      </View>
      <View style={styles.content}>
        <TouchableOpacity style={styles.button}>
          <View style={styles.buttonIcon}>
            <Icon name="people-alt" size={25} color="#fff" />
          </View>
          <Text style={styles.buttonText}>Rooms</Text>
          <Text style={styles.buttonSubtext}>1 room</Text>
          <Icon name="chevron-right" size={25} color="#000" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.button}>
          <View style={styles.buttonIcon}>
            <Icon name="schedule" size={25} color="#fff" />
          </View>
          <Text style={styles.buttonText}>Reminders</Text>
          <View style={styles.buttonBadge}>
            <Text style={styles.buttonBadgeText}>1</Text>
          </View>
          <Icon name="chevron-right" size={25} color="#000" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.button}>
          <View style={styles.buttonIcon}>
            <Icon name="home" size={25} color="#fff" />
          </View>
          <Text style={styles.buttonText}>Room check</Text>
          <Icon name="chevron-right" size={25} color="#000" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.button}>
          <View style={styles.buttonIcon}>
            <Icon name="people" size={25} color="#fff" />
          </View>
          <Text style={styles.buttonText}>Staff</Text>
          <Icon name="chevron-right" size={25} color="#000" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.button}>
          <View style={styles.buttonIcon}>
            <Icon name="door-open" size={25} color="#fff" />
          </View>
          <Text style={styles.buttonText}>Check - In/Out</Text>
          <Icon name="chevron-right" size={25} color="#000" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.button}>
          <View style={styles.buttonIcon}>
            <Icon name="send" size={25} color="#fff" />
          </View>
          <Text style={styles.buttonText}>Invite Parents</Text>
          <Icon name="chevron-right" size={25} color="#000" />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonDemo}
          onPress={() => console.log('Demo Room Pressed')}>
          <Text style={styles.buttonDemoText}>Try a Demo Room</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 16,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  headerText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  content: {
    padding: 16,
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 16,
    borderRadius: 8,
    marginBottom: 12,
    backgroundColor: '#F4F4F4',
  },
  buttonIcon: {
    backgroundColor: '#6200EE',
    padding: 12,
    borderRadius: 8,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: 'bold',
    marginLeft: 16,
  },
  buttonSubtext: {
    fontSize: 14,
    color: '#6200EE',
    marginLeft: 16,
  },
  buttonBadge: {
    backgroundColor: '#FF0000',
    padding: 4,
    borderRadius: 50,
    marginLeft: 16,
  },
  buttonBadgeText: {
    fontSize: 12,
    color: '#fff',
    textAlign: 'center',
  },
  buttonDemo: {
    backgroundColor: '#6200EE',
    padding: 12,
    borderRadius: 8,
    marginTop: 16,
  },
  buttonDemoText: {
    fontSize: 16,
    color: '#fff',
    textAlign: 'center',
  },
});

export default ManageSchool;
