import React, { useState } from 'react';
import {
  Keyboard,
  Modal,
  Pressable,
  StyleSheet,
  View,
  ViewStyle
} from 'react-native';
import DatePicker from 'react-native-date-picker';
import { COLORS } from '../utils/ColorUtils';
import { ICONS } from '../utils/Icons';
import { SizeUtils } from '../utils/SizeUtils';
import { STRINGS } from '../utils/Strings';
import CustomButton from './CustomLoaderButton';
import CustomText from './CustomText';
import { dismissKeyboard } from '../utils/Utils';

interface CustomDatePickerProps {
  selectedDate?: (dob: Date) => void;
  onDateChange?: (newDate: Date) => void;
  style?: ViewStyle;
  labelTitle: string;
  mode: 'date' | 'time' | 'datetime';
  minimumDate?: Date; // Add the minimumDate prop
}

const CustomDatePicker: React.FC<CustomDatePickerProps> = ({
  selectedDate,
  onDateChange,
  style,
  labelTitle = STRINGS.dobLabel,
  mode = 'date',
  minimumDate = new Date(1980, 0, 1) // Default value for minimumDate
}) => {
  const [date, setDate] = useState(new Date());
  const [placeHolder, setPlaceHolder] = useState('DD-MM-YYYY');
  const [modalVisible, setModalVisible] = useState(false);
  const [textColor, setTextColor] = useState(COLORS.lightGray); // Default text color
  const [dateSelected, setDateSelected] = useState(false); // Track if a date is selected

  const handleDateChange = (newDate: Date | undefined): void => {
    if (newDate !== undefined) {
      setDate(newDate);
      setPlaceHolder(newDate.toLocaleDateString('en-GB')); // Format date to DD-MM-YYYY
      setTextColor(COLORS.gray); // Change text color to black when date is chosen
      // setDateSelected(true); // Set date selected to true
    }
  };

  const handleModalVisibility = (): void => {
    dismissKeyboard();
    setModalVisible(true);
  };

  const handleDoneButton = () => {
    setModalVisible(false);
    setDateSelected(true)
    if (dateSelected && onDateChange) {
      onDateChange(date); // Call onDateChange only if a valid date is selected
    }
  };

  return (
    <View style={style}>
      <CustomText
        style={[
          styles.title,
          { color: COLORS.sbsDarkenColor(COLORS.lightGray, 20) }
        ]}
      >
        {labelTitle}
      </CustomText>
      <Pressable
        onPress={handleModalVisibility}
        style={[
          styles.datePicker,
          dateSelected ? styles.datePickerActive : styles.datePickerInactive // Use dateSelected to determine border color
        ]}
      >
        {ICONS.calenderIcon}
        <CustomText
          style={[
            styles.date,
            { color: textColor } // Dynamic text color based on whether date is chosen
          ]}
        >
          {placeHolder}
        </CustomText>
      </Pressable>
      <Modal
        visible={modalVisible}
        animationType="fade"
        transparent={true}
        onRequestClose={() => setModalVisible(false)}
      >
        <View style={styles.dateMainView}>
          <View style={styles.dateView}>
            <CustomText
              style={{
                textAlign: 'center',
                color: COLORS.sbsMediumDarkRed,
                marginTop: 25
              }}
            >
              {labelTitle}
            </CustomText>
            <DatePicker
              date={date}
              onDateChange={(newDate) => {
                handleDateChange(newDate);
              }}
              mode={mode}
              buttonColor={'red'}
              minimumDate={minimumDate} // Use the minimumDate prop
              maximumDate={new Date()} // Set maximumDate to current date
              dividerColor={COLORS.sbsDarkRed}
              style={{
                marginTop: SizeUtils.responsiveHeight(1)
              }}
              textColor={COLORS.black} // Ensure text color remains black
            />
            <CustomButton
              title="Done"
              onPress={handleDoneButton}
              moreButtonStyles={styles.button}
              textStyle={styles.text}
            />
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default CustomDatePicker;

const styles = StyleSheet.create({
  datePicker: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    borderWidth: 0.4,
    borderColor: COLORS.sbsDarkenColor(COLORS.lightGray, 20),
    height: SizeUtils.pxToDp(60),
    borderRadius: 8,
    paddingHorizontal: 16,
    gap: 10,
    marginTop: 5
  },
  datePickerActive: {
    // borderColor: COLORS.lightGray, // Border color when date picker is active
  },
  datePickerInactive: {
    // borderColor: COLORS.lightGray, // Border color when date picker is inactive
  },
  title: {
    fontSize: SizeUtils.scaleFont(12)
  },
  date: {
    fontSize: SizeUtils.scaleFont(14)
  },
  dateMainView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  dateView: {
    backgroundColor: COLORS.lightGray1,
    height: SizeUtils.responsiveHeight(40),
    borderRadius: 10,
    shadowColor: COLORS.lightGray
  },
  button: {
    marginTop: 10,
    height: '60%',
    width: '60%',
    alignSelf: 'center'
  },
  text: {
    color: COLORS.white,
    // fontWeight: '500',
    fontSize: SizeUtils.scaleFont(13)
  }
});
