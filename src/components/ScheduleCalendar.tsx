import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity
} from 'react-native';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import moment from 'moment';
import CustomText from './CustomText';
import { SizeUtils } from '../utils/SizeUtils';
import { COLORS } from '../utils/ColorUtils';
import { IMAGES } from '../utils/Images';
import { isStudent } from '../utils/Utils';

LocaleConfig.locales['en'] = {
  monthNames: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ],
  monthNamesShort: [
    'Jan.',
    'Feb.',
    'Mar.',
    'Apr.',
    'May.',
    'Jun.',
    'Jul.',
    'Aug.',
    'Sept.',
    'Oct.',
    'Nov.',
    'Dec.'
  ],
  dayNames: [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
  ],
  dayNamesShort: ['Sun.', 'Mon.', 'Tue.', 'Wed.', 'Thu.', 'Fri.', 'Sat.']
};
LocaleConfig.defaultLocale = 'en';

const StudentScheduleCalendar = ({ scheduleData, user, onDeleteStudent }) => {
  const [selectedDate, setSelectedDate] = useState(
    moment().format('YYYY-MM-DD')
  );
  const [markedDates, setMarkedDates] = useState({});

  const isStudentUser = isStudent(user);

  useEffect(() => {
    markScheduleDates();
  }, [scheduleData]);

  const markScheduleDates = () => {
    const marked = {};
    scheduleData?.enrollments.forEach((item) => {
      const date = item?.startDate.split('T')[0];
      marked[date] = { marked: true, dotColor: COLORS.sbsRed };
    });
    setMarkedDates(marked);
  };

  const renderScheduleDetails = ({ item }) => {
    return (
      <View style={styles.scheduleItem}>
        <CustomText style={styles.scheduleTitle} weightType="bold">
          {scheduleData?.class?.name}
        </CustomText>
        <CustomText style={styles.subText}>
          <Image source={IMAGES.calendarIcon} style={styles.arrowIcon} />
          {'  '}
          {moment(item?.startDate).format('hh:mm a')}
          {' - '}
          {moment(item?.endDate).format('hh:mm a')}
        </CustomText>

        <CustomText style={[styles.subText, { marginVertical: 3 }]}>
          Course:{' '}
          <CustomText
            fontType="averta"
            weightType="bold"
            style={styles.subText}
          >
            {scheduleData?.class?.course?.courseName}
          </CustomText>
        </CustomText>
        <CustomText style={[styles.subText, { marginVertical: 3 }]}>
          Lecturer: {scheduleData?.class?.lecturer?.user?.firstName}{' '}
          {scheduleData?.class?.lecturer?.user?.lastName}
        </CustomText>
        <CustomText style={styles.subText}>
          Room:{' '}
          <CustomText
            style={styles.subText}
            fontType="averta"
            weightType="bold"
          >
            {scheduleData?.class?.classRoom?.name} (
            {scheduleData?.class?.classRoom?.roomNumber})
          </CustomText>
        </CustomText>
      </View>
    );
  };
  const handleDelete = (studentId) => {
    onDeleteStudent(studentId);
  };

  const renderStudentDetails = ({ item }) => {
    return (
      <View style={styles.studentDetails}>
        <View style={styles.studentInfoContainer}>
          <CustomText style={styles.studentName}>
            {item?.user?.firstName} {item?.user?.lastName}
          </CustomText>
          <CustomText style={styles.studentInfo}>
            Student ID: {item?.profile?.id}
          </CustomText>
        </View>
        <TouchableOpacity onPress={() => handleDelete(item?.profile?.id)}>
          <Image source={IMAGES.deleteIcon} style={styles.deleteIcon} />
        </TouchableOpacity>
      </View>
    );
  };

  const filteredSchedule = scheduleData?.enrollments?.filter(
    (item) => item?.startDate?.split('T')[0] === selectedDate
  );

  return (
    <View style={styles.container}>
      <Calendar
        onDayPress={(day) => setSelectedDate(day.dateString)}
        markedDates={{
          ...markedDates,
          [selectedDate]: {
            selected: true,
            marked: filteredSchedule?.length > 0,
            selectedColor: COLORS.sbsMediumDarkRed
          }
        }}
        theme={{
          selectedDayBackgroundColor: COLORS.sbsMediumDarkRed,
          todayTextColor: COLORS.sbsMediumDarkRed,
          arrowColor: COLORS.sbsRed
        }}
      />
      <View style={styles.detailsContainer}>
        {filteredSchedule?.length > 0 ? (
          <>
            <FlatList
              data={[filteredSchedule[0]]}
              renderItem={renderScheduleDetails}
              keyExtractor={(item) => item.id.toString()}
            />
            {!isStudentUser && (
              <>
                <CustomText
                  weightType="bold"
                  style={[styles.subText, { marginLeft: 15, marginTop: 5 }]}
                >
                  Students
                </CustomText>
                <FlatList
                  data={scheduleData?.students || []}
                  renderItem={renderStudentDetails}
                  keyExtractor={(item) => item.user.id.toString()}
                  ListHeaderComponent={<View style={styles.flatListSpacer} />} // Add this to control spacing
                />
              </>
            )}
          </>
        ) : (
          <CustomText style={styles.noScheduleText}>
            No schedule for this day.
          </CustomText>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: '#fff'
  },
  detailsContainer: {
    // flex: 1,
    marginTop: 15
  },
  scheduleItem: {
    paddingHorizontal: 15,
    paddingBottom: 7,
    paddingTop: 5,
    borderBottomWidth: 0.6,
    borderBottomColor: COLORS.sbsLightenColor(COLORS.gray, 15)
  },
  studentDetails: {
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    flexDirection: 'row',
    alignItems: 'center'
  },
  scheduleTitle: {
    fontSize: SizeUtils.scaleFont(15),
    paddingBottom: 5,
    color: COLORS.sbsLightenColor(COLORS.sbsMediumDarkRed, 20)
  },
  studentName: {
    fontSize: SizeUtils.scaleFont(14),
    marginBottom: 5
  },
  studentInfo: {
    fontSize: SizeUtils.scaleFont(12),
    color: COLORS.sbsDarkenColor(COLORS.lightGray, 10)
  },
  noScheduleText: {
    textAlign: 'center',
    marginTop: 20,
    fontSize: SizeUtils.scaleFont(16),
    color: '#999'
  },
  subText: {
    fontSize: SizeUtils.scaleFont(13)
  },
  labelTitle: {
    fontSize: SizeUtils.scaleFont(13),
    color: COLORS.sbsDarkenColor(COLORS.lightGray, 10)
  },
  arrowIcon: {
    marginLeft: 10,
    tintColor: COLORS.sbsDarkenColor(COLORS.lightGray, 10),
    width: 15,
    height: 15
  },
  flatListSpacer: {
    backgroundColor: 'transparent', // Set a background color if necessary
    marginTop: 5
  },
  deleteIcon: {
    width: 20,
    height: 20,
    marginLeft: 16
  },
  studentInfoContainer: {
    flex: 1
  }
});

export default StudentScheduleCalendar;
