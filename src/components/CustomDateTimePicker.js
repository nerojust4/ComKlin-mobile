import React, {useState} from 'react';
import {Platform, View, Button, StyleSheet} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';

const CustomDateTimePicker = ({initialDate, onDateChange}) => {
  const [selectedDate, setSelectedDate] = useState(initialDate);
  const [showPicker, setShowPicker] = useState(false);

  const handleDateChange = (event, date) => {
    setShowPicker(Platform.OS === 'ios');
    if (date) {
      setSelectedDate(date);
      if (typeof onDateChange === 'function') {
        onDateChange(date);
      }
    }
  };

  const showDateTimePicker = () => {
    setShowPicker(true);
  };

  const hideDateTimePicker = () => {
    setShowPicker(false);
  };

  return (
    <View style={styles.container}>
      <Button title="Select Date" onPress={showDateTimePicker} />
      {showPicker && (
        <DateTimePicker
          value={selectedDate}
          mode="datetime"
        //   display={Platform.OS === 'ios' ? 'spinner' : 'default'}
          onChange={handleDateChange}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    paddingTop: 20,
  },
});

export default CustomDateTimePicker;
