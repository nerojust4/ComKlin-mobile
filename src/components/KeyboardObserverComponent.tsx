import React, { useState, useEffect } from 'react';
import {
  Animated,
  Dimensions,
  Keyboard,
  Platform,
  StyleSheet,
  TextInput,
  UIManager,
  View,
  ViewStyle,
} from 'react-native';

interface IKeyboardProps {
  children?: React.ReactNode;
  propStyle?: ViewStyle;
}

const KeyboardObserverComponent: React.FC<IKeyboardProps> = ({
  children,
  propStyle,
}) => {
  const { State: TextInputState } = TextInput;
  const [shift, setShift] = useState(new Animated.Value(0));

  useEffect(() => {
    const showSubscription = Keyboard.addListener(
      'keyboardDidShow',
      handleKeyboardDidShow
    );
    const hideSubscription = Keyboard.addListener(
      'keyboardDidHide',
      handleKeyboardDidHide
    );

    return () => {
      showSubscription.remove();
      hideSubscription.remove();
    };
  }, []);

  const handleKeyboardDidShow = (event: { endCoordinates: { height: number } }) => {
    const windowHeight = Dimensions.get('window').height;
    const keyboardHeight = event.endCoordinates.height;

    const currentlyFocusedField = TextInputState.currentlyFocusedInput();
    if (!currentlyFocusedField) return;

    currentlyFocusedField.measure((x, y, width, height, pageX, pageY) => {
      const fieldHeight = height;
      const fieldTop = pageY;
      const gap = windowHeight - keyboardHeight - (fieldTop + fieldHeight);

      if (gap < 0) {
        Animated.timing(shift, {
          toValue: Platform.OS === 'ios' ? gap - 20 : gap + 60,
          duration: 300,
          useNativeDriver: true,
        }).start();
      }
    });
  };

  const handleKeyboardDidHide = () => {
    Animated.timing(shift, {
      toValue: 0,
      duration: 120,
      useNativeDriver: true,
    }).start();
  };

  return (
    <Animated.View
      style={[styles.container, propStyle, { transform: [{ translateY: shift }] }]}
    >
      {children}
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default KeyboardObserverComponent;
