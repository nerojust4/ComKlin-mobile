import React from 'react';
import {View, StyleSheet} from 'react-native';
import Svg, {Line, G, Text as SvgText, Polyline} from 'react-native-svg';

const LineChart = ({data}) => {
  // Example data array
  // You can replace this with your own data array
  // For example, [10, 20, 15, 30, 25]
  const chartData = data || [10, 20, 15, 30, 25];

  const chartWidth = 250;
  const chartHeight = 150;
  const paddingLeft = 20;
  const paddingBottom = 20;
  const paddingRight = 10;
  const paddingTop = 10;
  const maxYValue = Math.max(...chartData);
  const minYValue = Math.min(...chartData);
  const yValueRange = maxYValue - minYValue;

  // Calculate scales and dimensions
  const scaleX =
    (chartWidth - paddingLeft - paddingRight) / (chartData.length - 1);
  const scaleY = (chartHeight - paddingTop - paddingBottom) / yValueRange;

  // Create points for the line
  const points = chartData
    .map((value, index) => {
      const x = index * scaleX + paddingLeft;
      const y = chartHeight - ((value - minYValue) * scaleY + paddingBottom);
      return `${x},${y}`;
    })
    .join(' ');

  // Create x-axis labels (assuming 1-based indexing for simplicity)
  const labels = Array.from(
    {length: chartData.length},
    (_, i) => `Label ${i + 1}`,
  );

  return (
    <View style={styles.container}>
      <Svg width={chartWidth} height={chartHeight}>
        {/* Draw y-axis */}
        <Line
          x1={paddingLeft}
          y1={paddingTop}
          x2={paddingLeft}
          y2={chartHeight - paddingBottom}
          stroke="black"
          strokeWidth="1"
        />

        {/* Draw x-axis */}
        <Line
          x1={paddingLeft}
          y1={chartHeight - paddingBottom}
          x2={chartWidth - paddingRight}
          y2={chartHeight - paddingBottom}
          stroke="black"
          strokeWidth="1"
        />

        {/* Draw line */}
        <Polyline points={points} stroke="green" strokeWidth="2" fill="none" />

        {/* Draw x-axis labels */}
        {labels.map((label, index) => (
          <SvgText
            key={index}
            x={index * scaleX + paddingLeft}
            y={chartHeight - paddingBottom + 15}
            fontSize="12"
            textAnchor="middle">
            {label}
          </SvgText>
        ))}

        {/* Draw y-axis labels */}
        <SvgText
          x={paddingLeft - 5}
          y={paddingTop + 10}
          fontSize="12"
          textAnchor="end"
          alignmentBaseline="middle">
          {maxYValue}
        </SvgText>
        <SvgText
          x={paddingLeft - 5}
          y={chartHeight - paddingBottom}
          fontSize="12"
          textAnchor="end"
          alignmentBaseline="middle">
          {minYValue}
        </SvgText>
      </Svg>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
});

export default LineChart;
