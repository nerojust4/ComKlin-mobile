import {useState, useCallback} from 'react';
import { triggerVibration } from '../../utils/Utils';

const useDialog = () => {
  const [dialogVisible, setDialogVisible] = useState(false);
  const [dialogTitle, setDialogTitle] = useState('');
  const [dialogBody, setDialogBody] = useState('');
  const [dialogImage, setDialogImage] = useState(null);
  const [displayConfirmButton, setDisplayConfirmButton] = useState(false);

  const showDialog = useCallback((title, body, image, displayConfirm) => {
    setDialogTitle(title);
    setDialogBody(body);
    setDialogImage(image);
    setDisplayConfirmButton(displayConfirm);
    setDialogVisible(true);
    // triggerVibration();
  }, []);

  const hideDialog = useCallback(() => {
    setDialogVisible(false);
  }, []);

  return {
    dialogVisible,
    dialogTitle,
    dialogBody,
    dialogImage,
    displayConfirmButton,
    showDialog,
    hideDialog,
  };
};

export default useDialog;
