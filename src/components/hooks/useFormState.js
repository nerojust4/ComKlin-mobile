import {useState} from 'react';

const useFormState = () => {
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    username: '',
    phoneNumber: '',
    email: '',
    password: '',
    dob: null,
    city: '',
    address: '',
    course: ''
  });

  const handleInputChange = (field, value) => {
    setFormData(prevData => ({
      ...prevData,
      [field]: value,
    }));
  };

  const resetForm = () => {
    setFormData({
      firstName: '',
      lastName: '',
      username: '',
      phoneNumber: '',
      email: '',
      password: '',
      dob: null,
      city: '',
      address: '',
      course: '',
    });
  };

  return {formData, handleInputChange, resetForm};
};

export default useFormState;
