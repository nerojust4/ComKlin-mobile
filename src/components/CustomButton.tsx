import React from 'react';
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  ViewStyle,
  TextStyle
} from 'react-native';
import { SizeUtils } from '../utils/SizeUtils';
import CustomText from './CustomText';
import { COLORS } from '../utils/ColorUtils';

interface CustomButtonProps {
  onPress: () => void;
  title: string;
  buttonStyle?: ViewStyle;
  textStyle?: TextStyle;
  disabled?: boolean;
}

const CustomButton: React.FC<CustomButtonProps> = ({
  onPress,
  title,
  buttonStyle,
  textStyle,
  disabled
}) => {
  return (
    <TouchableOpacity
      onPressOut={onPress}
      style={[styles.button, buttonStyle, disabled && styles.disabledButton]}
      disabled={disabled}
      activeOpacity={0.6}
    >
      <CustomText
        style={[styles.text, textStyle, disabled && styles.disabledText]}
      >
        {title}
      </CustomText>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  button: {
    padding: 18,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: COLORS.sbsDarkenColor(COLORS.green, 30)
  },
  text: {
    fontSize: SizeUtils.scaleFont(16),
    color: 'white'
  },
  disabledButton: {
    backgroundColor: COLORS.lightGray2
  },
  disabledText: {
    color: '#999'
  }
});

export default CustomButton;
