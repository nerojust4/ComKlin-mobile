import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Modal} from 'react-native';
import {Calendar} from 'react-native-calendars';
import DateTimePicker from '@react-native-community/datetimepicker';
import CustomButton from './CustomButton'; // Assuming you have a CustomButton component
import {COLORS} from '../utils/ColorUtils';

const CustomCalendarTimePicker = ({onDateSelect, visible, onClose}) => {
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [showTimePicker, setShowTimePicker] = useState(false);
  const [minDate, setMinDate] = useState(new Date()); // Minimum date is today
  useEffect(() => {
    if (!visible) {
      // Reset selected date when modal closes
      setSelectedDate(new Date());
    }
  }, [visible]);

  const handleDateSelect = day => {
    setSelectedDate(new Date(day.timestamp));
    setShowTimePicker(true);
  };

  const handleTimeChange = (event, date) => {
    setShowTimePicker(false);
    if (date) {
      const localDateTime = new Date(selectedDate);
      localDateTime.setHours(date.getHours());
      localDateTime.setMinutes(date.getMinutes());
      localDateTime.setSeconds(0);
      localDateTime.setMilliseconds(0);

      // Format the date as a local time string without timezone
      const formattedDate = `${localDateTime.getFullYear()}-${String(
        localDateTime.getMonth() + 1,
      ).padStart(2, '0')}-${String(localDateTime.getDate()).padStart(
        2,
        '0',
      )} ${String(localDateTime.getHours()).padStart(2, '0')}:${String(
        localDateTime.getMinutes(),
      ).padStart(2, '0')}:${String(localDateTime.getSeconds()).padStart(
        2,
        '0',
      )}`;
      console.log(formattedDate);
      onDateSelect(formattedDate);
    }
  };

  const handleClose = () => {
    setShowTimePicker(false);
    onClose();
  };

  return (
    <Modal
      visible={visible}
      transparent={true}
      animationType="slide"
      onRequestClose={handleClose}>
      <View style={styles.modalContainer}>
        <View style={styles.contentContainer}>
          <Calendar
            current={selectedDate.toISOString().split('T')[0]}
            onDayPress={handleDateSelect}
            style={styles.calendar}
            minDate={minDate.toISOString().split('T')[0]}
            // maxDate={maxDate.toISOString().split('T')[0]}
            theme={{
              // backgroundColor: COLORS.sbsRed, // Calendar background color
              // calendarBackground: COLORS.lightGray1, // Calendar container background color
              textSectionTitleColor: '#333333', // Title text color
              selectedDayBackgroundColor: COLORS.sbsRed, // Selected day background color
              selectedDayTextColor: '#ffffff', // Selected day text color
              // textDayFontWeight:'condensedBold',
              todayTextColor: COLORS.sbsRed, // Today text color
              dayTextColor: '#333333', // Default day text color
              textDisabledColor: '#d9e1e8', // Disabled day text color
              dotColor: COLORS.sbsRed, // Marker dot color
              selectedDotColor: '#ffffff', // Selected marker dot color
              arrowColor: COLORS.sbsRed, // Navigation arrows color
              monthTextColor: COLORS.sbsMediumDarkRed, // Month text color
              indicatorColor: COLORS.sbsRed, // Indicator color (vertical line on selected date)
              textDayFontFamily: 'monospace', // Day text font family
              textMonthFontFamily: 'monospace', // Month text font family
              textDayHeaderFontFamily: 'monospace', // Day header text font family
              textDayFontSize: 16, // Day text font size
              textMonthFontSize: 16, // Month text font size
              textDayHeaderFontSize: 16, // Day header text font size
            }}
          />
          {showTimePicker && (
            <DateTimePicker
              value={selectedDate}
              mode="time"
              display="default"
              onChange={handleTimeChange}
            />
          )}
        </View>
        <CustomButton title="Close" onPress={handleClose} />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)', // Semi-transparent background
  },
  contentContainer: {
    backgroundColor: 'white',
    width: '80%',
    borderRadius: 10,
    padding: 20,
    elevation: 5, // Android elevation for shadow
  },
  calendar: {
    marginBottom: 10,
  },
});

export default CustomCalendarTimePicker;
