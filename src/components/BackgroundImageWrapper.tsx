import {
  Dimensions,
  ImageBackground,
  ImageBackgroundProps,
  StyleSheet,
  ViewProps,
} from 'react-native';
import {COLORS} from '../utils/ColorUtils';
import {IMAGES} from '../utils/Images';

interface IBackgroundImageWrapper extends ImageBackgroundProps {}

const BackgroundImageWrapper = ({
  source = IMAGES.backgroundImage,
  style,
  imageStyle,
  children,
  ...rest
}: IBackgroundImageWrapper) => {
  return (
    <ImageBackground
      source={source}
      style={[styles.container, style]}
      imageStyle={[styles.imageStyle, imageStyle]}
      {...rest}>
      {children}
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  imageStyle: {
    resizeMode: 'cover',
    height: Dimensions.get('window').height / 2,
    position: 'absolute',
    top: '50%',
  },
  container: {
    // flex: 1,
    //   backgroundColor: COLORS.fadePinkBackground,
    // backgroundColor: COLORS.lightGray1,
    // position: 'relative',
    // justifyContent: 'space-between',
    //   paddingHorizontal: 20
    height: '100%',
  },
});

export default BackgroundImageWrapper;
