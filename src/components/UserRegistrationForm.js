import React, {useState, useRef, useEffect, useMemo, useCallback} from 'react';
import {
  View,
  StyleSheet,
  Keyboard,
  TouchableOpacity,
  Image,
} from 'react-native';
import CustomTextInput from '../components/CustomTextInput';
import CustomButton from '../components/CustomButton';
import CustomDatePicker from '../components/CustomDatePicker';
import {COLORS} from '../utils/ColorUtils';
import {SizeUtils} from '../utils/SizeUtils';
import CustomText from '../components/CustomText';
import CustomBottomSheet from '../components/CustomBottomSheet';
import {ICONS} from '../utils/Icons';
import {STRINGS} from '../utils/Strings';
import SliderTab from './SliderTab';
import {useDispatch, useSelector} from 'react-redux';
import {IMAGES} from '../utils/Images';
import useFormState from '../components/hooks/useFormState';
import {AccountTabOptions} from '../utils/AppConstants';
import {countryOptions, stateOptions} from '../utils/Countries';

const UserRegistrationForm = ({
  userType,
  onSubmit,
  loading,
  buttonText,
  style = {},
}) => {
  const {formData, handleInputChange, resetForm} = useFormState();
  const dispatch = useDispatch();
  const [bottomSheetVisible, setBottomSheetVisible] = useState({
    gender: false,
    country: false,
    state: false,
    role: false,
  });
  const [selectedOptions, setSelectedOptions] = useState({
    gender: {id: 0, name: 'Select gender'},
    country: {id: 0, name: 'Select country'},
    state: {id: 0, name: 'Select state'},
    role: {id: 0, name: 'Select role'},
  });
  const [selectedTab, setSelectedTab] = useState({id: 3, name: 'Student'});
  // const {roles} = useSelector(state => state.roles);

  // useEffect(() => {
  //   if (!roles) {
  //     dispatch(fetchRoles());
  //   }
  // }, [roles]);

  const inputRefsData = () => ({
    firstName: useRef(null),
    lastName: useRef(null),
    phoneNumber: useRef(null),
    email: useRef(null),
    password: useRef(null),
    username: useRef(null),
    city: useRef(null),
    address: useRef(null),
    qualification: useRef(null),
    yearsOfExperience: useRef(null),
    department: useRef(null),
    course: useRef(null),
    courseRef: useRef(null),
    yearOfStudyRef: useRef(null),
  });
  const inputRefs = inputRefsData();

  const genderOptions = [
    {id: 1, name: 'Male'},
    {id: 2, name: 'Female'},
  ];

  const handleCloseBottomSheet = useCallback(type => {
    setBottomSheetVisible(prevState => ({
      ...prevState,
      [type]: false,
    }));
  }, []);

  const handleOpenBottomSheet = useCallback(type => {
    if (Keyboard.isVisible()) {
      Keyboard.dismiss();
    }
    setBottomSheetVisible(prevState => ({
      ...prevState,
      [type]: true,
    }));
  }, []);

  const handleSelectItem = useCallback((listId, item) => {
    setSelectedOptions(prevState => ({
      ...prevState,
      [listId]: item,
    }));
  }, []);

  const handleRegister = useCallback(() => {
    let userDetails = {
      gender: selectedOptions.gender.name,
      country: selectedOptions.country.name,
      state: selectedOptions.state.name,
      firstName: formData.firstName,
      lastName: formData.lastName,
      phoneNumber: formData.phoneNumber,
      email: formData.email,
      username: formData.username,
      password: formData.password,
      city: formData.city,
      address: formData.address,
      dob: formData.dob,
    };

    onSubmit(userDetails);
  }, [formData, selectedOptions, userType, selectedTab?.id, onSubmit]);

  return (
    <View style={[styles.container, style]}>
      <CustomTextInput
        label={STRINGS.firstName}
        LeftIcon={ICONS.profileIcon}
        placeholder="First Name"
        value={formData.firstName}
        onSubmitEditing={() => inputRefs.lastName.current.focus()}
        onChangeText={value => handleInputChange('firstName', value)}
      />
      <CustomTextInput
        label={STRINGS.lastName}
        LeftIcon={ICONS.profileIcon}
        placeholder="Last Name"
        inputRef={inputRefs.lastName}
        onSubmitEditing={() => inputRefs.phoneNumber.current.focus()}
        value={formData.lastName}
        onChangeText={value => handleInputChange('lastName', value)}
        moreStyles={{marginTop: 10}}
      />
      <CustomTextInput
        label={STRINGS.phoneNumber}
        LeftIcon={ICONS.phoneIcon}
        placeholder="Phone Number"
        inputRef={inputRefs.phoneNumber}
        value={formData.phoneNumber}
        onSubmitEditing={() => inputRefs.email.current.focus()}
        onChangeText={value => handleInputChange('phoneNumber', value)}
        moreStyles={{marginTop: 10}}
        maxLength={14}
      />
      <CustomTextInput
        label={STRINGS.email}
        LeftIcon={ICONS.emailIcon}
        placeholder="Email"
        value={formData.email}
        inputRef={inputRefs.email}
        onSubmitEditing={() => inputRefs.username.current.focus()}
        onChangeText={value => handleInputChange('email', value)}
        autoCapitalize="none"
        moreStyles={{marginTop: 10}}
      />
      <CustomTextInput
        label={STRINGS.username}
        LeftIcon={ICONS.profileIcon}
        placeholder="Username"
        inputRef={inputRefs.username}
        value={formData.username}
        onChangeText={value => handleInputChange('username', value)}
        onSubmitEditing={() => inputRefs.password.current.focus()}
        moreStyles={{marginTop: 10}}
      />
      <CustomTextInput
        label={STRINGS.password}
        LeftIcon={ICONS.padlockIcon}
        placeholder="Password"
        value={formData.password}
        inputRef={inputRefs.password}
        onChangeText={value => handleInputChange('password', value)}
        secureTextEntry
        moreStyles={{marginTop: 10}}
        onSubmitEditing={() => inputRefs.city.current.focus()}
      />
      <CustomDatePicker
        selectedDate={formData.dob}
        minimumDate={new Date(1980, 0, 1)}
        onDateChange={(date: Date) => {
          // Handle selected date here
          console.log('Selected date:', date);
          handleInputChange('dob', date); // Assuming handleInputChange is defined elsewhere
        }}
        style={{flex: 1, alignContent: 'center', marginTop: 10}}
      />
      <View style={{marginTop: 10}}>
        <CustomText style={styles.labelTitle}>Gender</CustomText>

        <TouchableOpacity
          onPress={() => handleOpenBottomSheet('gender')}
          activeOpacity={0.6}
          style={styles.selectedStyle}>
          <View style={styles.row}>
            <CustomText style={{fontSize: SizeUtils.scaleFont(14)}}>
              {selectedOptions.gender.name}
            </CustomText>
            <Image source={IMAGES.arrowDownIcon} style={styles.arrowIcon} />
          </View>
        </TouchableOpacity>
        <CustomBottomSheet
          items={genderOptions}
          visible={bottomSheetVisible.gender}
          titleText="Select a gender"
          onClose={() => handleCloseBottomSheet('gender')}
          onSelectItem={item => handleSelectItem('gender', item)}
        />
      </View>

      <View style={{marginTop: 10}}>
        <CustomText style={styles.labelTitle}>Country</CustomText>

        <TouchableOpacity
          onPress={() => handleOpenBottomSheet('country')}
          activeOpacity={0.6}
          style={styles.selectedStyle}>
          <View style={styles.row}>
            <CustomText style={{fontSize: SizeUtils.scaleFont(14)}}>
              {selectedOptions.country.name}
            </CustomText>
            <Image source={IMAGES.arrowDownIcon} style={styles.arrowIcon} />
          </View>
        </TouchableOpacity>
        <CustomBottomSheet
          items={countryOptions}
          visible={bottomSheetVisible.country}
          titleText="Select a country"
          onClose={() => handleCloseBottomSheet('country')}
          onSelectItem={item => handleSelectItem('country', item)}
        />
      </View>
      <View style={{marginTop: 10}}>
        <CustomText style={styles.labelTitle}>State</CustomText>

        <TouchableOpacity
          onPress={() => handleOpenBottomSheet('state')}
          activeOpacity={0.6}
          style={styles.selectedStyle}>
          <View style={styles.row}>
            <CustomText style={{fontSize: SizeUtils.scaleFont(14)}}>
              {selectedOptions.state.name}
            </CustomText>
            <Image source={IMAGES.arrowDownIcon} style={styles.arrowIcon} />
          </View>
        </TouchableOpacity>
        <CustomBottomSheet
          items={stateOptions}
          visible={bottomSheetVisible.state}
          titleText="Select a state"
          onClose={() => handleCloseBottomSheet('state')}
          onSelectItem={item => handleSelectItem('state', item)}
        />
      </View>

      <CustomTextInput
        label={STRINGS.city}
        LeftIcon={ICONS.cityIcon}
        placeholder="Enter City"
        value={formData.city}
        moreStyles={{marginTop: 8}}
        inputRef={inputRefs.city}
        onChangeText={value => handleInputChange('city', value)}
        onSubmitEditing={() => inputRefs.address.current.focus()}
      />
      <CustomTextInput
        label={STRINGS.address}
        LeftIcon={ICONS.addressIcon}
        placeholder="Address"
        inputRef={inputRefs.address}
        value={formData.address}
        onChangeText={value => handleInputChange('address', value)}
        moreStyles={{marginTop: 10}}
      />
 
      <CustomButton
        title={buttonText}
        onPress={handleRegister}
        buttonStyle={styles.registerButton}
        textStyle={styles.registerText}
        disabled={loading}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    paddingBottom: 25,
  },
  title: {
    fontSize: SizeUtils.scaleFont(20),
    marginBottom: 10,
    textAlign: 'left',
    top: -7,
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  registerButton: {
    marginTop: 30,
    marginBottom: 0,
    backgroundColor: COLORS.sbsDarkenColor(COLORS.green, 30),
  },
  registerText: {
    color: COLORS.white,
    fontSize: SizeUtils.scaleFont(14),
  },

  selectedStyle: {
    borderWidth: 0.5,
    height: SizeUtils.pxToDp(60),
    width: '100%',
    borderColor: COLORS.sbsDarkenColor(COLORS.lightGray, 20),
    borderRadius: 8,
    paddingHorizontal: 16,
    marginRight: 10,
    paddingVertical: 18,
    fontSize: SizeUtils.scaleFont(14),
    color: COLORS.gray,
    marginTop: 5,
  },
  labelTitle: {
    color: COLORS.sbsDarkenColor(COLORS.lightGray, 20),
    fontSize: SizeUtils.scaleFont(13),
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  arrowIcon: {
    marginLeft: 10,
    tintColor: COLORS.sbsDarkenColor(COLORS.lightGray, 10),
    width: 15,
    height: 15,
  },
});

export default UserRegistrationForm;
