// src/components/SessionTimeoutHandler.tsx
import React, { useEffect, useState, ReactNode } from 'react';
import { AppState, AppStateStatus, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { clearUser, setError } from '../store/auth/AuthSlice';
import { reset } from '../navigation/NavigationService';
import { RootState } from '../store';
import { ROUTE_LABELS } from '../utils/AppConstants';
import { logout } from '../store/auth/AuthThunk';

const SESSION_TIMEOUT_DURATION = 15 * 60 * 1000; // 30 minutes

let timeoutId: NodeJS.Timeout | null = null;
let appStateSubscription: any;

interface SessionTimeoutHandlerProps {
  children: ReactNode;
}

const SessionTimeoutHandler: React.FC<SessionTimeoutHandlerProps> = ({
  children
}) => {
  const dispatch = useDispatch();
  const token = useSelector((state: RootState) => state.auth.token);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const handleAppStateChange = (nextAppState: AppStateStatus) => {
      if (!token) return; // Do nothing if user is not logged in

      if (nextAppState === 'background') {
        dispatch(logout({}));
        dispatch(setError('Session has expired due to in-activity'));
        reset(ROUTE_LABELS.LOGIN);
      }
    };

    appStateSubscription = AppState.addEventListener(
      'change',
      handleAppStateChange
    );

    resetTimeout(); // Initial call to set the timeout
    setIsLoading(false);

    return () => {
      if (appStateSubscription) {
        appStateSubscription.remove();
      }
      if (timeoutId) {
        clearTimeout(timeoutId);
      }
    };
  }, [dispatch, token]);

  const resetTimeout = () => {
    if (!token) return; // Do nothing if user is not logged in

    if (timeoutId) {
      clearTimeout(timeoutId);
    }
    timeoutId = setTimeout(() => {
      if (!token) return; // Do nothing if user is not logged in

      dispatch(logout({}));
      dispatch(setError('Session has expired due to inactivity'));
      reset(ROUTE_LABELS.LOGIN);
    }, SESSION_TIMEOUT_DURATION);
  };

  const handleUserActivity = () => {
    if (!token) return; // Do nothing if user is not logged in
    if (timeoutId) {
      clearTimeout(timeoutId);
    }
    resetTimeout();
  };

  if (isLoading) {
    return null; // Render nothing until initialization is complete
  }

  return (
    <View style={{ flex: 1 }} onTouchStart={handleUserActivity}>
      {children}
    </View>
  );
};

export default SessionTimeoutHandler;
