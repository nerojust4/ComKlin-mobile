import React from 'react';
import {View, FlatList, TouchableOpacity, StyleSheet} from 'react-native';
import {BottomSheet} from 'react-native-paper';
import CustomText from './CustomText'; // Ensure this path is correct
import {SizeUtils} from '../utils/SizeUtils'; // Ensure this path is correct
import {COLORS} from '../utils/ColorUtils'; // Ensure this path is correct

const CustomBottomSheetComponent = ({
  items,
  visible,
  onClose,
  onSelectItem,
  titleText,
}) => {
  return (
    <BottomSheet
      visible={visible}
      onDismiss={onClose}
      contentContainerStyle={styles.sheetContainer}>
      <View style={styles.header}>
        <CustomText style={styles.title}>{titleText}</CustomText>
      </View>
      <FlatList
        data={items}
        keyExtractor={item => item.id.toString()}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() => onSelectItem(item)}
            style={styles.item}>
            <CustomText style={styles.itemText}>{item.name}</CustomText>
          </TouchableOpacity>
        )}
        ListEmptyComponent={() => {
          return (
            <CustomText style={styles.emptyText}>No data found</CustomText>
          );
        }}
      />
    </BottomSheet>
  );
};

const styles = StyleSheet.create({
  sheetContainer: {
    backgroundColor: COLORS.white,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    padding: 16,
  },
  header: {
    marginBottom: 16,
  },
  title: {
    fontSize: SizeUtils.scaleFont(18),
    fontWeight: 'bold',
    color: COLORS.black,
  },
  item: {
    paddingVertical: 12,
    borderBottomWidth: 0.5,
    borderBottomColor: COLORS.lightGray,
  },
  itemText: {
    fontSize: SizeUtils.scaleFont(16),
    color: COLORS.black,
  },
  emptyText: {
    textAlign: 'center',
    marginTop: 35,
    fontSize: SizeUtils.scaleFont(14),
  },
});

export default CustomBottomSheetComponent;
