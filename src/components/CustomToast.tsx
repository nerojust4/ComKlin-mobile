import React, {useEffect, useRef, useState} from 'react';
import {Animated, StyleSheet} from 'react-native';
import {COLORS} from '../utils/ColorUtils';
import {SizeUtils} from '../utils/SizeUtils';
import CustomText from './CustomText';

interface ToastProps {
  isConnected?: boolean;
}

const CustomToast: React.FC<ToastProps> = ({isConnected = true}) => {
  const [isVisible, setIsVisible] = useState(true);
  const message = isConnected ? 'Back online' : 'No Internet connection';
  const translateYAnim = useRef(new Animated.Value(-100)).current; // Initial value for translateY: -100

  useEffect(() => {
    setIsVisible(true); // Reset visibility whenever isConnected changes

    // Animate the toast entrance
    Animated.spring(translateYAnim, {
      toValue: 0, // Final value for translateY: 0 (normal position)
      speed: 8, // Adjust the speed of the animation as needed
      bounciness: 8, // Adjust the bounciness of the animation as needed
      useNativeDriver: true,
    }).start();

    // Set timeout only when connected
    if (isConnected) {
      const timer = setTimeout(() => {
        setIsVisible(false);
      }, 5000);

      // Cleanup function to clear timeout when disconnected
      return () => {
        clearTimeout(timer);
      };
    }
  }, [translateYAnim, isConnected]);

  return isVisible ? (
    <Animated.View
      style={[
        styles.container,
        {
          backgroundColor: isConnected ? COLORS.green : COLORS.darkAsh,
          transform: [{translateY: translateYAnim}], // Bind translateY to animated value
        },
      ]}>
      <CustomText
        weightType="regular"
        style={[
          styles.message,
          {
            width: isConnected
              ? SizeUtils.responsiveWidth(30)
              : SizeUtils.responsiveWidth(40),
          },
        ]}>
        {message}
      </CustomText>
    </Animated.View>
  ) : null;
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 8,
    paddingHorizontal: 16,
    backgroundColor: 'transparent',
    alignItems: 'center',
    marginBottom: 10,
  },
  message: {
    fontSize: SizeUtils.scaleFont(16),
    color: COLORS.white,
    textAlign: 'center',
  },
});

export default CustomToast;
