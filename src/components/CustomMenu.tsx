import React, { useState, useRef } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import {Menu as MaterialMenu, MenuItem, MenuDivider } from 'react-native-material-menu';

interface CustomMenuProps {
  items: string[];
  placeholder: string;
  onSelect: (item: string) => void;
}

const CustomMenu: React.FC<CustomMenuProps> = ({ items, placeholder, onSelect }) => {
  const [menuVisible, setMenuVisible] = useState(false);
  const menuRef = useRef<MaterialMenu | null>(null);

  const showMenu = () => {
    setMenuVisible(true);
    menuRef.current?.show();
  };

  const hideMenu = () => {
    setMenuVisible(false);
    menuRef.current?.hide();
  };

  const handleSelect = (item: string) => {
    onSelect(item);
    hideMenu();
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={showMenu} style={styles.button}>
        <Text style={styles.buttonText}>{placeholder}</Text>
      </TouchableOpacity>
      <MaterialMenu
        ref={(ref) => (menuRef.current = ref)}
        button={<View />}
        visible={menuVisible}
        onRequestClose={hideMenu}>
        {items.map((item, index) => (
          <React.Fragment key={index}>
            <MenuItem onPress={() => handleSelect(item)}>{item}</MenuItem>
            {index < items.length - 1 && <MenuDivider />}
          </React.Fragment>
        ))}
      </MaterialMenu>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: 'lightgray',
    borderRadius: 10,
    margin: 10,
  },
  button: {
    padding: 10,
    backgroundColor: '#FFF',
    borderRadius: 5,
  },
  buttonText: {
    color: '#000',
    fontSize: 16,
  },
});

export default CustomMenu;
