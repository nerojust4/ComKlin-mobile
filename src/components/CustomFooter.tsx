import React from 'react';
import { StyleSheet, View } from 'react-native';
import CustomButton from './CustomButton';
import CustomText from './CustomText';
import { COLORS } from '../utils/ColorUtils';
import { SizeUtils } from '../utils/SizeUtils';

const RenderFooterView = ({ fetchMore, page, loadingMore, totalPages }) => {
  return (
    <View style={styles.paginationContainer}>
      <CustomButton
        onPress={() => fetchMore(page - 1)}
        disabled={page === 1 || loadingMore}
        title="Previous"
        textStyle={{ color: COLORS.black, fontSize: SizeUtils.scaleFont(12) }}
        buttonStyle={[
          styles.button,
          styles.paginationButton,
          page === 1 || loadingMore
            ? styles.inactiveButton
            : styles.activeButton
        ]}
      />
      <CustomText weightType="medium" style={styles.pageNumberText}>
        Page {page} of {totalPages}
      </CustomText>
      <CustomButton
        onPress={() => fetchMore(page + 1)}
        disabled={loadingMore || totalPages === page}
        title="Next"
        textStyle={{ color: COLORS.black, fontSize: SizeUtils.scaleFont(12) }}
        buttonStyle={[
          styles.button,
          styles.paginationButton,
          loadingMore || totalPages === page
            ? styles.inactiveButton
            : styles.activeButton
        ]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    borderRadius: 25,
    alignItems: 'center',
    height: 35,
    padding: 0
  },
  paginationButton: {
    borderWidth: 0.5,
    borderRadius: 20
  },
  activeButton: {
    borderColor: COLORS.sbsDarkerRed,
    backgroundColor: COLORS.white,
    flex: 0.4
  },
  inactiveButton: {
    borderColor: COLORS.lightGray,
    backgroundColor: COLORS.transparent,
    flex: 0.4
  },
  paginationContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 20,
    marginBottom: 2,
    flex: 0.08
  },
  pageNumberText: {
    fontSize: SizeUtils.scaleFont(12),
    marginHorizontal: 10
  }
});

export default RenderFooterView;
