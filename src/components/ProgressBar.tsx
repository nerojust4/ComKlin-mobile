import React from "react";
import { Bar } from "react-native-progress"
import { Dimensions } from "react-native"
import {BarPropTypes} from "react-native-progress"
import { COLORS } from "../utils/ColorUtils";

export interface IProgressBar extends BarPropTypes {

}

const ProgressBar = ({
    progress,
    width = Dimensions.get("window").width - 32,
    color = COLORS.successGreen,
    unfilledColor = COLORS.borderGray,
    height = 8,
    ...rest
}: IProgressBar) => {
    return (
        <Bar 
            progress={progress}
            width={width}
            animated
            color={color}
            unfilledColor={unfilledColor}
            height={height}
            borderWidth={0}
            {...rest}
        />
    )
}

export default ProgressBar