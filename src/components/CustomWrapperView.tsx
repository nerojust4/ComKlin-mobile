import React from 'react';
import {
  View,
  StyleSheet,
  StatusBar,
  useColorScheme,
  StyleProp,
  ViewStyle,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  SafeAreaView
} from 'react-native';
import CustomDialog from './CustomDialog';
import { COLORS } from '../utils/ColorUtils';

interface CustomWrapperViewProps {
  children: React.ReactNode;
  style?: StyleProp<ViewStyle>;
  withPaddingTop?: boolean;
  withScroll?: boolean;
  enableAutoDismiss?: boolean;
  dialogVisible: boolean;
  dialogImage?: any;
  dialogTitle: string;
  dialogBody: string;
  onDialogDismiss?: () => void;
  onDialogOkay?: () => void;
  displayConfirm?: boolean;
}

const CustomWrapperView: React.FC<CustomWrapperViewProps> = React.memo(
  ({
    children,
    style,
    withPaddingTop = false,
    withScroll = true,
    enableAutoDismiss,
    dialogVisible,
    dialogImage,
    dialogTitle,
    dialogBody,
    onDialogDismiss,
    onDialogOkay,
    displayConfirm = false
  }) => {
    return (
      <>
        <StatusBar
          barStyle={'dark-content'}
          backgroundColor={COLORS.background}
        />
        <SafeAreaView style={[styles.container, style]}>
          <KeyboardAvoidingView
            behavior={Platform.select({ ios: 'padding', android: 'padding' })}
            style={styles.flex}
          >
            {withScroll ? (
              <ScrollView
                style={styles.flex}
                showsVerticalScrollIndicator={false}
                keyboardShouldPersistTaps="handled"
                contentContainerStyle={styles.scrollViewContent}
              >
                {children}
              </ScrollView>
            ) : (
              <View style={styles.flex}>{children}</View>
            )}
          </KeyboardAvoidingView>
          <CustomDialog
            enableAutoDismiss={enableAutoDismiss}
            visible={dialogVisible}
            image={dialogImage}
            title={dialogTitle}
            body={dialogBody}
            onNegativeClick={onDialogDismiss}
            onPositiveClick={onDialogOkay}
            displayConfirm={displayConfirm}
          />
        </SafeAreaView>
      </>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.background
  },
  flex: {
    flex: 1
  },
  scrollViewContent: {
    flexGrow: 0.3,
    paddingTop: Platform.select({ ios: 20, android: 0 })
  }
});

export default CustomWrapperView;
