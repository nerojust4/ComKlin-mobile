/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  ActivityIndicator,
  StyleProp,
  StyleSheet,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {COLORS, ColorUtils} from '../utils/ColorUtils';
import {SizeUtils} from '../utils/SizeUtils';
import CustomText from './CustomText';

interface AppBtnProps {
  onPress: () => void;
  title: string;
  isLoading?: boolean;
  isDisabled?: boolean;
  type?: 'outline' | undefined;
  color?: string;
  icon?: React.ReactNode;
  iconName?: React.ReactNode;
  moreButtonStyles?: StyleProp<ViewStyle>;
  textStyle?: StyleProp<ViewStyle>;
  borderColor?: string;
  textColor?: string;
  useFlex?: boolean;
}

const CustomButton: React.FC<AppBtnProps> = ({
  onPress,
  title,
  textStyle,
  isLoading,
  isDisabled,
  type,
  icon,
  iconName,
  moreButtonStyles,
  borderColor = COLORS.sbsRed,
  textColor = COLORS.sbsRed,
  useFlex = true,
}) => {
  return (
    <>
      {type === 'outline' ? (
        <TouchableOpacity
          activeOpacity={0.4}
          onPress={onPress}
          accessible={true}
          accessibilityLabel="AppButton"
          style={[
            {
              ...styles.btnOutline,
              borderColor: borderColor,
              opacity: isDisabled ? 0.5 : 1,
              backgroundColor: 'transparent',
            },
            moreButtonStyles,
          ]}
          disabled={isDisabled}>
          {isLoading ? (
            <ActivityIndicator size="large" color={COLORS.fcmbPurple} />
          ) : (
            <>
              <View style={{flexDirection: 'row'}}>
                {icon && iconName}
                <CustomText
                  weightType="medium"
                  style={[styles.title, {color: textColor}]}>
                  {title}
                </CustomText>
              </View>
            </>
          )}
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          activeOpacity={0.4}
          style={useFlex && {flex: 1}}
          disabled={isDisabled}
          onPress={onPress}>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={[COLORS.sbsRed, COLORS.sbsMediumDarkRed, COLORS.sbsDarkRed]}
            style={[
              styles.linearGradient,
              {opacity: isDisabled ? 0.3 : 1},
              moreButtonStyles,
            ]}>
            {isLoading ? (
              <ActivityIndicator size="large" color={COLORS.white} />
            ) : (
              <CustomText
                // weightType="bold"
                style={[styles.gradientText, textStyle]}>
                {title}
              </CustomText>
            )}
          </LinearGradient>
        </TouchableOpacity>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  appButtonContainer: {
    borderRadius: 50,
    paddingHorizontal: 20,
    height: SizeUtils.responsiveHeight(20),
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnOutline: {
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    height: SizeUtils.responsiveHeight(8),
  },
  gradientText: {
    textAlign: 'center',
    fontSize: SizeUtils.scaleFont(16),
    color: COLORS.white,
    fontWeight: '600',
  },
  title: {
    fontSize: SizeUtils.scaleFont(16),
  },
  linearGradient: {
    height: SizeUtils.responsiveHeight(8),
    borderRadius: 50,
    paddingHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default CustomButton;
