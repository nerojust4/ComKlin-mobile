import React, { useEffect, useState } from 'react';
import { Modal, View, StyleSheet } from 'react-native';
import CustomButton from './CustomButton';
import { COLORS } from '../utils/ColorUtils';
import { SizeUtils } from '../utils/SizeUtils';
import CustomText from './CustomText';

interface CustomDialogProps {
  enableAutoDismiss?: boolean;
  visible?: boolean;
  title: string;
  body: string;
  image: any; // Ensure the type matches your image source
  onNegativeClick?: any | null;
  onPositiveClick?: () => void;
  displayConfirm: boolean;
}

const CustomDialog: React.FC<CustomDialogProps> = ({
  visible = false,
  enableAutoDismiss = true,
  title,
  body,
  image,
  displayConfirm = false,
  onNegativeClick = () => {},
  onPositiveClick
}) => {
  const [countdown, setCountdown] = useState(20);
  const [showCountdown, setShowCountdown] = useState(false);

  useEffect(() => {
    let dismissTimer: ReturnType<typeof setTimeout>;
    let countdownInterval: ReturnType<typeof setInterval>;
    let countdownStartTimer: ReturnType<typeof setTimeout>;

    if (visible && enableAutoDismiss) {
      // Start a timer to show the countdown after 5 seconds
      countdownStartTimer = setTimeout(() => {
        setShowCountdown(true);

        // Set an interval to update the countdown timer every second
        countdownInterval = setInterval(() => {
          setCountdown((prevCountdown) => prevCountdown - 1);
        }, 1000);

        // Set a timer to automatically dismiss the dialog after the countdown ends
        dismissTimer = setTimeout(() => {
          onNegativeClick(); // Dismiss the dialog by calling the negative click handler
        }, countdown * 1000);
      }, 5000);
    }

    // Clear the timers when the dialog is hidden or unmounted
    return () => {
      clearTimeout(dismissTimer);
      clearTimeout(countdownStartTimer);
      clearInterval(countdownInterval);
      setCountdown(countdown); // Reset the countdown when the dialog is hidden
      setShowCountdown(false); // Reset the countdown visibility when the dialog is hidden
    };
  }, [visible, enableAutoDismiss, onNegativeClick]);

  return (
    <Modal
      transparent={true}
      visible={visible}
      animationType="none"
      statusBarTranslucent
    >
      <View style={styles.container}>
        <View style={styles.dialog}>
          {image}
          <CustomText style={styles.title} weightType="bold">
            {title}
          </CustomText>
          <CustomText style={styles.body}>{body}</CustomText>
          {enableAutoDismiss && showCountdown && (
            <CustomText style={styles.countdown}>
              Dismisses in {countdown} seconds
            </CustomText>
          )}
          <View
            style={[
              styles.buttonContainer,
              !displayConfirm && { justifyContent: 'center' } // Adjust button container alignment if confirm button is not displayed
            ]}
          >
            <CustomButton
              title="Dismiss"
              onPress={onNegativeClick}
              buttonStyle={[styles.button, styles.dismissButton]}
              textStyle={styles.dismissText}
            />
            {displayConfirm && (
              <CustomButton
                title="Ok"
                onPress={onPositiveClick}
                buttonStyle={[styles.button, styles.confirmButton]}
                textStyle={styles.confirmText}
              />
            )}
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.4)'
  },
  dialog: {
    width: 300,
    padding: 20,
    backgroundColor: 'white',
    borderRadius: 10,
    alignItems: 'center'
  },
  button: {
    width: '47%',
    padding: 10,
    borderRadius: 20,
    marginVertical: 10,
    alignItems: 'center'
  },
  dismissButton: {
    borderColor: COLORS.sbsDarkenColor(COLORS.green, 30),
    borderWidth: 0.3
  },
  confirmButton: {
    backgroundColor: COLORS.sbsDarkenColor(COLORS.green, 30)
  },
  dismissText: {
    color: COLORS.sbsDarkenColor(COLORS.green, 30),
    fontSize: SizeUtils.scaleFont(14)
  },
  confirmText: {
    color: COLORS.white,
    fontSize: SizeUtils.scaleFont(14)
  },
  image: {
    width: 50,
    height: 50,
    marginBottom: 20
  },
  title: {
    fontSize: SizeUtils.scaleFont(16),
    // fontWeight: '800',
    marginTop: 12
  },
  body: {
    fontSize: SizeUtils.scaleFont(14),
    textAlign: 'center',
    marginVertical: 20
  },
  countdown: {
    fontSize: SizeUtils.scaleFont(11),
    color: COLORS.sbsDarkRed,
    marginBottom: 10
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%'
  }
});

export default CustomDialog;
