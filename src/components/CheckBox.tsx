// import React, { ReactNode } from "react";
// import BouncyCheckbox, { IBouncyCheckboxProps } from "react-native-bouncy-checkbox";
// import { COLORS } from "../utils/ColorUtils";
// import { ICONS } from "../utils/Icons";
// import { StyleSheet, View } from "react-native";

// interface ICheckBox extends IBouncyCheckboxProps {
//     unCheckedIconComponent?: ReactNode
// }

// const CheckBox = ({
//     size = 27,
//     isChecked = false,
//     iconComponent,
//     unCheckedIconComponent,
//     ...rest
// }: ICheckBox) => {
//     return (
//         <BouncyCheckbox
//             size={size}
//             isChecked={isChecked}
//             useNativeDriver
//             iconComponent={
//                 iconComponent ? (
//                     isChecked ? iconComponent : unCheckedIconComponent
//                 ) : (
//                     isChecked ? ICONS.checkIcon(COLORS.fcmbOrange) :
//                         <View style={[{ width: size, height: size, borderRadius: size }, styles.container]}>
//                             <View style={[{ width: size / 2, height: size / 2, borderRadius: size }, styles.emptyCirce]} />
//                         </View>
//                 )
//             }
//             {...rest}
//         />
//     )
// }

// const styles = StyleSheet.create({
//     container: {
//         borderWidth: 1,
//         borderColor: COLORS.borderGray,
//         justifyContent: 'center',
//         alignItems: 'center'
//     },
//     emptyCirce: {
//         backgroundColor: COLORS.borderGray,
//     }
// })

// export default CheckBox