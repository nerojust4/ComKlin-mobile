import React, { useState } from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  Image,
  ViewStyle,
  View,
  Platform,
  StatusBar
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { COLORS } from '../utils/ColorUtils';
import { IMAGES } from '../utils/Images';
import { SizeUtils } from '../utils/SizeUtils';
import CustomText from './CustomText';
import { Menu, Divider, Provider as PaperProvider } from 'react-native-paper';
import { RootState } from '../store';
import { useSelector } from 'react-redux';

interface CustomBackButtonProps {
  style?: ViewStyle;
  rightIconImageStyle?: ViewStyle;
  image?: any; // Updated type to any to match default value type
  onPress?: () => void;
  onRightIconPress?: (action: string) => void;
  showMoreIcon?: boolean;
  useProfileView?: boolean;
  showCenterIcon?: boolean;
  rightIcon?: any; // Updated type to any to match default value type
  text?: string | undefined;
  menuItems?: { title: string; action: string }[];
}

const CustomBackButton: React.FC<CustomBackButtonProps> = ({
  style,
  image = IMAGES.backArrow || require('../assets/icons/arrowleft.png'),
  showMoreIcon = false,
  onPress,
  rightIcon = IMAGES.moreIcon2,
  onRightIconPress,
  showCenterIcon = false,
  rightIconImageStyle,
  text,
  menuItems = [],
  useProfileView = false
}) => {
  const navigation = useNavigation();
  const [menuVisible, setMenuVisible] = useState(false);
  const user = useSelector((state: RootState) => state.auth.user);

  const fName = user?.firstName.substring(0, 1).toUpperCase(); // Get first two characters and capitalize
  const lName = user?.lastName.substring(0, 1).toUpperCase(); // Get first two characters and capitalize

  return (
    <View style={[styles.header, style]}>
      <TouchableOpacity
        style={styles.button}
        onPress={onPress || (() => navigation.goBack())}
        activeOpacity={0.4}
      >
        <View style={styles.iconContainer}>
          <Image source={image} style={styles.icon} />
          {showCenterIcon && (
            <Image source={IMAGES.sbsImageLogo} style={styles.sbsImage} />
          )}
        </View>
      </TouchableOpacity>
      {text && (
        <CustomText weightType="bold" style={styles.text}>
          {text}
        </CustomText>
      )}
      {useProfileView && (
        <TouchableOpacity
          style={styles.settingsButton}
          onPress={() => {
            onRightIconPress('');
            setMenuVisible(true);
          }}
        >
          <View style={styles.circle}>
            <CustomText style={styles.initials}>
              {lName}
              {fName}
            </CustomText>
          </View>
        </TouchableOpacity>
      )}
      {showMoreIcon && (
        <Menu
          visible={menuVisible}
          onDismiss={() => setMenuVisible(false)}
          anchor={
            <TouchableOpacity
              style={styles.settingsButton}
              onPress={() => setMenuVisible(true)}
            >
              <Image
                source={rightIcon}
                style={[styles.settingsIcon, rightIconImageStyle]}
              />
            </TouchableOpacity>
          }
        >
          {menuItems.map((item, index) => (
            <React.Fragment key={index}>
              <Menu.Item
                onPress={() => {
                  setMenuVisible(false);
                  onRightIconPress && onRightIconPress(item.action);
                }}
                title={item.title}
              />
              {index < menuItems.length - 1 && <Divider />}
            </React.Fragment>
          ))}
        </Menu>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: SizeUtils.responsiveHeight(8), // Adjust header height based on screen size
    backgroundColor: COLORS.lightGray1,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 15,
    elevation: 3,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    marginTop: Platform.OS == 'ios' ? StatusBar.currentHeight + 32 : 0
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1
  },
  iconContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1
  },
  icon: {
    height: 20,
    width: 22,
    tintColor: COLORS.sbsDarkenColor(COLORS.green, 30),
    marginLeft: 5 // Space between the arrow and the logo
  },
  sbsImage: {
    height: 40,
    width: 70,
    resizeMode: 'contain',
    marginLeft: 20 // Space between the arrow and the logo
  },
  text: {
    // marginLeft: 0,
    flex: 0.7,
    fontSize: SizeUtils.scaleFont(13)
  },
  settingsButton: {
    marginLeft: 'auto'
  },
  settingsIcon: {
    height: 30,
    width: 30,
    tintColor: COLORS.sbsDarkenColor(COLORS.green, 30)
  },
  circle: {
    width: 35,
    height: 35,
    borderRadius: 20,
    backgroundColor: COLORS.sbsDarkenColor(COLORS.lightGray1, 30),
    justifyContent: 'center',
    alignItems: 'center'
  },
  initials: {
    fontSize: SizeUtils.scaleFont(15),
    color: COLORS.sbsDarkenColor(COLORS.green, 30) // Assuming you have a white color defined in ColorUtils
  }
});

export default CustomBackButton;
