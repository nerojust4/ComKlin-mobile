import React, { useRef, useEffect } from 'react';
import { View, StyleSheet, Animated } from 'react-native';
import Svg, { Line } from 'react-native-svg';

const AnimatedLine = Animated.createAnimatedComponent(Line);

const XYAxisGraph = () => {
  const animatedXAxis = useRef(new Animated.Value(0)).current;
  const animatedYAxis = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.timing(animatedXAxis, {
      toValue: 280, // Example value
      duration: 1000,
      useNativeDriver: true,
    }).start();

    Animated.timing(animatedYAxis, {
      toValue: 180, // Example value
      duration: 1000,
      useNativeDriver: true,
    }).start();
  }, []);

  return (
    <View style={styles.container}>
      <Svg width="300" height="200">
        <AnimatedLine
          x1="10"
          y1="190"
          x2={animatedXAxis}
          y2="190"
          stroke="black"
          strokeWidth="2"
        />
        <AnimatedLine
          x1="10"
          y1="190"
          x2="10"
          y2={190 - animatedYAxis}
          stroke="black"
          strokeWidth="2"
        />
      </Svg>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
});

export default XYAxisGraph;
