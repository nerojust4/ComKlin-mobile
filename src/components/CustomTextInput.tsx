import React, { useState } from 'react';
import {
  StyleSheet,
  TextInput,
  TextInputProps,
  TouchableOpacity,
  View,
  StyleProp,
  ViewStyle,
  KeyboardAvoidingView
} from 'react-native';
import MaskInput, { MaskInputProps } from 'react-native-mask-input';
import { COLORS } from '../utils/ColorUtils';
import { ICONS } from '../utils/Icons';
import { SizeUtils } from '../utils/SizeUtils';
import CustomText from './CustomText';

type IMaskedInputProps = Pick<MaskInputProps, 'mask' | 'maskAutoComplete'>;

interface ITextInputProps extends TextInputProps, IMaskedInputProps {
  error?: string | null;
  label?: string;
  LeftIcon?: React.ReactNode;
  inputRef?: React.RefObject<TextInput>;
  onChangeMaskedText?: (masked: string, unmasked: string) => void;
  isMasked?: boolean;
  onSubmitEditing?: () => void;
  isLastField?: boolean;
  moreStyles?: StyleProp<ViewStyle>;
  viewStyles?: StyleProp<ViewStyle>;
}

const CustomTextInput: React.FC<ITextInputProps> = ({
  label,
  value,
  secureTextEntry,
  LeftIcon,
  inputRef,
  error,
  isMasked,
  placeholder,
  onChangeMaskedText,
  mask,
  maskAutoComplete,
  keyboardType,
  editable = true,
  onSubmitEditing,
  moreStyles,
  viewStyles,
  ...rest
}) => {
  const [isFocused, setIsFocused] = useState(false);
  const [isPasswordVisible, setIsPasswordVisible] = useState(secureTextEntry);

  const handleFocus = () => setIsFocused(true);
  const handleBlur = () => setIsFocused(false);
  const togglePasswordVisibility = () =>
    setIsPasswordVisible(!isPasswordVisible);

  return (
    <KeyboardAvoidingView style={[styles.container, moreStyles]}>
      {label && (
        <CustomText style={[styles.title, isFocused && styles.titleFocused]}>
          {label}
        </CustomText>
      )}
      <View
        style={[
          styles.inputContainer,
          isFocused && styles.onFocus,
          !editable && styles.nonEditable,
          error && styles.onError,
          viewStyles
        ]}
      >
        {LeftIcon}
        {isMasked ? (
          <MaskInput
            value={value}
            onChangeText={onChangeMaskedText}
            placeholder={placeholder}
            onFocus={handleFocus}
            onBlur={handleBlur}
            ref={inputRef}
            secureTextEntry={isPasswordVisible}
            mask={mask}
            maskAutoComplete={maskAutoComplete}
            style={styles.input}
            keyboardType={keyboardType}
            onSubmitEditing={onSubmitEditing}
            editable={editable}
            {...rest}
          />
        ) : (
          <TextInput
            style={styles.input}
            value={value}
            onFocus={handleFocus}
            onBlur={handleBlur}
            ref={inputRef}
            secureTextEntry={isPasswordVisible}
            placeholder={placeholder}
            placeholderTextColor={COLORS.lightGray}
            keyboardType={keyboardType}
            onSubmitEditing={onSubmitEditing}
            editable={editable}
            {...rest}
          />
        )}
        {secureTextEntry && (
          <TouchableOpacity
            onPress={togglePasswordVisibility}
            style={styles.eyeIcon}
          >
            {isPasswordVisible ? ICONS.eyeOpen : ICONS.eyeClose}
          </TouchableOpacity>
        )}
      </View>
      {error && <CustomText style={styles.error}>{error}</CustomText>}
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    gap: 8
  },
  title: {
    color: COLORS.sbsDarkenColor(COLORS.lightGray, 20),
    fontSize: SizeUtils.scaleFont(13)
  },
  error: {
    color: COLORS.sbsDarkRed,
    fontSize: SizeUtils.scaleFont(12)
  },
  titleFocused: {
    color: COLORS.sbsDarkenColor(COLORS.green, 10),
    fontSize: SizeUtils.scaleFont(13)
  },
  titleFocusedGreen: {
    color: COLORS.green
  },
  inputContainer: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    borderWidth: 0.4,
    height: SizeUtils.pxToDp(60),
    borderColor: COLORS.sbsDarkenColor(COLORS.lightGray, 15),
    borderRadius: 8,
    paddingHorizontal: 16,
    gap: 12,
    backgroundColor: COLORS.fadePinkBackground
  },
  input: {
    flex: 1,
    fontSize: SizeUtils.scaleFont(14),
    color: COLORS.black,
    fontFamily: 'Montserrat-Regular'
  },
  onFocus: {
    // borderColor: COLORS.sbsDarkenColor(COLORS.sbsRed, 40),
    borderWidth: 0.4,
    elevation: 4
    // shadowColor: COLORS.gray,
    // shadowOpacity: 0.8,
    // shadowRadius: 5,
    // shadowOffset: {height: 1, width: 1},
  },
  onError: {
    borderColor: COLORS.sbsMediumDarkRed,
    borderWidth: 0.5,
    elevation: 2,
    // shadowColor: COLORS.sbsDarkRed,
    shadowOpacity: 0.3,
    shadowRadius: 5,
    shadowOffset: { height: 1, width: 1 }
  },
  eyeIcon: {
    padding: 2
  }
});

export default CustomTextInput;
