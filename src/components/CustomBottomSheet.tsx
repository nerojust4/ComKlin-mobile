import React from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  Modal,
  FlatList
} from 'react-native';
import CustomText from './CustomText';
import { COLORS } from '../utils/ColorUtils';
import { SizeUtils } from '../utils/SizeUtils';

interface CustomBottomSheetProps {
  items: any[];
  titleText?: string;
  type?: string;
  visible: boolean;
  onClose: () => void;
  onSelectItem: (item: object) => void;
}

const CustomBottomSheet: React.FC<CustomBottomSheetProps> = ({
  items,
  titleText = 'Select an Item',
  visible,
  onClose,
  onSelectItem,
  type
}) => {
  const handleItemPress = (item: object) => {
    console.log('item', item);
    onSelectItem(item);
    onClose();
  };

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={visible}
      onRequestClose={onClose}
    >
      <TouchableOpacity
        style={styles.modalContainer}
        activeOpacity={1}
        onPress={onClose}
      >
        <TouchableOpacity activeOpacity={1} style={styles.sheetContainer}>
          <CustomText
            style={styles.headerText}
            weightType="bold"
            fontType="averta"
          >
            {titleText}
          </CustomText>
          <View style={styles.listContainer}>
            <FlatList
              data={items}
              renderItem={({ item }) => (
                <TouchableOpacity
                  activeOpacity={0.4}
                  key={item.id}
                  onPress={() => handleItemPress(item)}
                  style={styles.item}
                >
                  {type === 'student' ? (
                    <CustomText weightType="regular">
                      {item?.user?.firstName}{' '}
                      <CustomText weightType="regular">
                        {item?.user?.lastName}
                      </CustomText>
                    </CustomText>
                  ) : (
                    <CustomText weightType="regular">{item?.name}</CustomText>
                  )}
                </TouchableOpacity>
              )}
              ListEmptyComponent={() => {
                return (
                  <CustomText style={styles.emptyText}>
                    No data found
                  </CustomText>
                );
              }}
              keyExtractor={(item) => item.id.toString()}
            />
          </View>
          <TouchableOpacity onPress={onClose} style={styles.closeButton}>
            <CustomText style={styles.closeButtonText} weightType="bold">
              Close
            </CustomText>
          </TouchableOpacity>
        </TouchableOpacity>
      </TouchableOpacity>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.5)'
  },
  sheetContainer: {
    backgroundColor: '#FFF',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingHorizontal: 20,
    // paddingBottom: 15,
    paddingTop: 15,
    maxHeight: '80%' // Allow the sheet to grow but not cover more than 80% of the screen
  },
  listContainer: {
    flexGrow: 1,
    minHeight: 100 // Minimum height to display a few items
  },
  item: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.lightGray2
  },
  closeButton: {
    alignItems: 'center',
    paddingVertical: 13,
    alignContent: 'center'
  },
  closeButtonText: {
    color: COLORS.sbsMediumDarkRed,
    fontSize: SizeUtils.scaleFont(14)
  },
  headerText: {
    color: COLORS.sbsMediumDarkRed,
    fontSize: SizeUtils.scaleFont(14),
    textAlign: 'center'
    // marginVertical: 10,
  },
  emptyText: {
    textAlign: 'center',
    marginTop: 35,
    fontSize: SizeUtils.scaleFont(14)
  }
});

export default CustomBottomSheet;
