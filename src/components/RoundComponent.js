import React from 'react';
import {View, StyleSheet, Text, Image, TouchableOpacity} from 'react-native';
import CustomText from './CustomText';
import {SizeUtils} from '../utils/SizeUtils';
import {IMAGES} from '../utils/Images';
import {COLORS} from '../utils/ColorUtils';

const RoundComponent = ({title, onPress}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={styles.activityCircle} source={IMAGES.eventIcon}>
        <Image style={styles.icon} source={IMAGES.eventIcon} />
      </View>
      {title && (
        <CustomText numberOfLines={1} style={styles.activityTitle}>
          {title}
        </CustomText>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    // marginHorizontal: 5,
  },
  activityCircle: {
    width: 60,
    height: 60,
    borderRadius: 40,
    backgroundColor: COLORS.sbsLightenColor(COLORS.lightGray, 20),
    borderColor: COLORS.sbsDarkenColor(COLORS.green, 10),
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 30,
    height: 30,
    tintColor: COLORS.sbsDarkenColor(COLORS.green, 10),
  },
  activityTitle: {
    marginTop: 5,
    textAlign: 'center',
    fontSize: SizeUtils.scaleFont(10),
    paddingHorizontal: 5,
  },
});

export default RoundComponent;
