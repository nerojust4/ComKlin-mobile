// DrawerContent.tsx
import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { DrawerContentScrollView } from '@react-navigation/drawer';
import { useDispatch, useSelector } from 'react-redux';
import CustomText from './CustomText';
import { COLORS } from '../utils/ColorUtils';
import { SizeUtils } from '../utils/SizeUtils';
import CustomWrapperView from './CustomWrapperView';
import { ICONS } from '../utils/Icons';
import { RootState } from '../../src/store';
import { ROUTE_LABELS } from '../utils/AppConstants';
import useDialog from './hooks/useDialog';
import { logout } from '../store/auth/AuthThunk';

const drawerItems = [
  {
    label: ROUTE_LABELS.USERS,
    imageSource: require('../assets/icons/admin.png')
  },
  ,
  // {
  //   label: ROUTE_LABELS.STAFF,
  //   imageSource: require('../assets/icons/staff.png')
  // }
  {
    label: ROUTE_LABELS.LECTURERS,
    imageSource: require('../assets/icons/teacher.png')
  },
  {
    label: ROUTE_LABELS.ADMIN,
    imageSource: require('../assets/icons/admin.png')
  },
  {
    label: ROUTE_LABELS.COURSES,
    imageSource: require('../assets/icons/courses.png')
  },
  {
    label: ROUTE_LABELS.CLASSES,
    imageSource: require('../assets/icons/class.png')
  },
  {
    label: ROUTE_LABELS.SCHEDULES,
    imageSource: require('../assets/icons/teacher.png')
  },
  {
    label: ROUTE_LABELS.STUDENTS,
    imageSource: require('../assets/icons/students.png')
  },
  {
    label: ROUTE_LABELS.CLASSROOMS,
    imageSource: require('../assets/icons/staff.png')
  },
  {
    label: ROUTE_LABELS.ANNOUNCEMENTS,
    imageSource: require('../assets/icons/home.png')
  },
  {
    label: ROUTE_LABELS.FINANCIALS,
    imageSource: require('../assets/icons/financial.png')
  },
  {
    label: ROUTE_LABELS.REPORTS,
    imageSource: require('../assets/icons/report.png')
  },
  {
    label: ROUTE_LABELS.PROFILE,
    imageSource: require('../assets/icons/profile.png')
  }
];

const DrawerContent = (props) => {
  const dispatch = useDispatch();
  const { user, loading } = useSelector((state: RootState) => state.auth);

  const {
    dialogVisible,
    dialogTitle,
    dialogBody,
    dialogImage,
    displayConfirmButton,
    showDialog,
    hideDialog
  } = useDialog();

  const handleDialogDismiss = () => {
    hideDialog();
  };

  const handleDialogOkay = () => {
    hideDialog();
    dispatch(logout({}));
  };

  const handleLogout = () => {
    showDialog('Logout', 'Do you want to logout?', ICONS.warningIcon, true);
  };

  // Check if user or user.features is null or undefined
  if (!user || !user?.features) {
    // Return null or placeholder content if user data is not available
    return null;
  }

  return (
    <>
      <CustomWrapperView
        dialogVisible={dialogVisible}
        dialogImage={dialogImage}
        dialogTitle={dialogTitle}
        dialogBody={dialogBody}
        onDialogDismiss={handleDialogDismiss}
        onDialogOkay={handleDialogOkay}
        displayConfirm={displayConfirmButton}
        withScroll={false}
      >
        <View style={styles.header}>
          <View
            style={{ flexDirection: 'row', justifyContent: 'space-between' }}
          >
            <Image
              source={require('../assets/images/clean_four.png')}
              resizeMethod="auto"
              style={styles.logo}
            />

            <CustomText
              fontType="averta"
              weightType="bold"
              style={{
                marginTop: 10,
                color: COLORS.sbsLightenColor(COLORS.sbsDarkerRed, 40)
              }}
            >
              {user?.role?.name || 'User'}
            </CustomText>
          </View>
          <CustomText style={styles.welcomeText}>
            Welcome{' '}
            <CustomText fontType="averta" weightType="bold">
              {user.firstName}
            </CustomText>
          </CustomText>
        </View>
        <View style={styles.separator} />
        <View style={{ flex: 3 }}>
          <DrawerContentScrollView>
            {/* Render drawer items based on allowed features */}
            {drawerItems.map(
              (item, index) =>
                user?.features?.includes(item?.label) && (
                  <View key={index}>
                    <TouchableOpacity
                      onPress={() => props.navigation.navigate(item?.label)}
                      style={styles.drawerItemContainer}
                    >
                      <Image
                        source={item?.imageSource}
                        style={styles.itemImage}
                      />
                      <CustomText style={styles.drawerItemText}>
                        {item?.label}
                      </CustomText>
                    </TouchableOpacity>
                    <View style={styles.separator} />
                  </View>
                )
            )}
          </DrawerContentScrollView>
        </View>
      </CustomWrapperView>
      <TouchableOpacity
        style={styles.footer}
        activeOpacity={0.6}
        onPress={handleLogout}
      >
        <CustomText weightType="bold" style={styles.footerText}>
          Logout
        </CustomText>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  header: {
    marginTop: 30,
    justifyContent: 'center',
    paddingHorizontal: 15
  },
  logo: {
    height: 40,
    width: '40%'
  },
  welcomeText: {
    marginVertical: 15,
    fontSize: SizeUtils.scaleFont(14)
  },
  drawerItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
    paddingHorizontal: 20
  },
  drawerItemText: {
    fontSize: SizeUtils.scaleFont(14),
    color: COLORS.sbsDarkerRed
  },
  separator: {
    height: 0.8,
    backgroundColor: COLORS.lightGray,
    width: '100%'
  },
  footer: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    backgroundColor: COLORS.sbsDarkenColor(COLORS.sbsRed, 30)
  },
  footerText: {
    fontSize: SizeUtils.scaleFont(14),
    color: COLORS.white
  },
  itemImage: {
    width: 20,
    height: 20,
    marginRight: 10,
    tintColor: COLORS.sbsMediumDarkRed
  }
});

export default DrawerContent;
