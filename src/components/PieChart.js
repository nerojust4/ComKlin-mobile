import React from 'react';
import {View, StyleSheet} from 'react-native';
import Svg, {Circle, G, Path, Text as SvgText} from 'react-native-svg';

const PieChart = ({data}) => {
  // Example data array
  // You can replace this with your own data array
  // For example, [10, 20, 15, 30, 25]
  const chartData = data || [25, 35, 45, 55];

  const chartWidth = 300;
  const chartHeight = 300;
  const radius = Math.min(chartWidth, chartHeight) / 2;
  const centerX = chartWidth / 2;
  const centerY = chartHeight / 2;
  const total = chartData.reduce((acc, value) => acc + value, 0);

  let startAngle = 0;
  let endAngle = 0;

  const arcs = chartData.map((value, index) => {
    const percentage = value / total;
    endAngle = startAngle + percentage * 2 * Math.PI;
    const arcPath = describeArc(centerX, centerY, radius, startAngle, endAngle);
    const arcColor = `hsl(${(index / chartData.length) * 360}, 70%, 50%)`;
    const labelPosition = getLabelPosition(
      startAngle,
      endAngle,
      radius,
      centerX,
      centerY,
    );

    startAngle = endAngle;

    return {
      path: arcPath,
      color: arcColor,
      labelPosition,
    };
  });

  function describeArc(x, y, radius, startAngle, endAngle) {
    const start = polarToCartesian(x, y, radius, endAngle);
    const end = polarToCartesian(x, y, radius, startAngle);
    const largeArcFlag = endAngle - startAngle <= Math.PI ? '0' : '1';

    return `M ${start.x} ${start.y} A ${radius} ${radius} 0 ${largeArcFlag} 0 ${end.x} ${end.y} L ${x} ${y} Z`;
  }

  function polarToCartesian(centerX, centerY, radius, angleInRadians) {
    const x = centerX + radius * Math.cos(angleInRadians);
    const y = centerY + radius * Math.sin(angleInRadians);
    return {x, y};
  }

  function getLabelPosition(startAngle, endAngle, radius, centerX, centerY) {
    const midAngle = (startAngle + endAngle) / 2;
    const labelRadius = radius / 1.2; // Adjust label position relative to the radius
    const labelX = centerX + labelRadius * Math.cos(midAngle);
    const labelY = centerY + labelRadius * Math.sin(midAngle);
    return {x: labelX, y: labelY};
  }

  return (
    <View style={styles.container}>
      <Svg width={chartWidth} height={chartHeight}>
        <G transform={`translate(${centerX},${centerY})`}>
          {arcs.map((arc, index) => (
            <Path key={index} d={arc.path} fill={arc.color} />
          ))}
          {arcs.map((arc, index) => (
            <SvgText
              key={index}
              x={arc.labelPosition.x}
              y={arc.labelPosition.y}
              fontSize="14"
              fill="white"
              textAnchor="middle">
              {`${((chartData[index] / total) * 100).toFixed(1)}%`}
            </SvgText>
          ))}
        </G>
      </Svg>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    width:'auto',
    height:'auto'
  },
});

export default PieChart;
