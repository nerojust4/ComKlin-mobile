import React, { useState } from 'react';
import { View, TouchableOpacity, Image, StyleSheet } from 'react-native';
import CustomText from './CustomText'; // Replace with your CustomText component
import CustomNameBottomSheet from './CustomNameBottomSheet'; // Replace with your CustomNameBottomSheet component
import { SizeUtils } from '../utils/SizeUtils'; // Replace with your SizeUtils import
import { IMAGES } from '../utils/Images'; // Replace with your IMAGES import

const CustomSelectorWithRetry = ({
  label,
  selectedItem,
  items,
  fetchItems,
  onSelectItem,
}) => {
  const [sheetVisible, setSheetVisible] = useState(false);
  const [retrying, setRetrying] = useState(false);

  const handleRetry = () => {
    setRetrying(true);
    fetchItems()
      .then(() => setRetrying(false))
      .catch(() => setRetrying(false));
  };

  const handleOpenSheet = () => setSheetVisible(true);
  const handleCloseSheet = () => setSheetVisible(false);
  const handleSelect = (item) => {
    onSelectItem(item);
    handleCloseSheet();
  };

  return (
    <>
      <CustomText onPress={handleOpenSheet} style={styles.labelTitle}>
        {label}
      </CustomText>
      <TouchableOpacity onPress={handleOpenSheet} style={styles.selectedStyle}>
        <View style={styles.row}>
          <CustomText style={{ fontSize: SizeUtils.scaleFont(14) }}>
            {selectedItem?.name}{' '}
            {selectedItem?.additionalInfo
              ? `(${selectedItem?.additionalInfo})`
              : ''}
          </CustomText>
          <Image source={IMAGES.arrowDownIcon} style={styles.arrowIcon} />
        </View>
      </TouchableOpacity>
      <CustomNameBottomSheet
        items={items}
        visible={sheetVisible}
        titleText={`Select a ${label.toLowerCase()}`}
        onClose={handleCloseSheet}
        onSelectItem={handleSelect}
      />
      {retrying ? (
        <TouchableOpacity style={styles.retryButton} onPress={handleRetry}>
          <CustomText style={styles.retryButtonText}>Retry</CustomText>
        </TouchableOpacity>
      ) : null}
    </>
  );
};

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  selectedStyle: {
    borderWidth: 0.5,
    borderColor: '#ccc',
    borderRadius: 8,
    paddingHorizontal: 16,
    paddingVertical: 18,
    marginTop: 5,
    marginBottom: 10,
  },
  labelTitle: {
    fontSize: SizeUtils.scaleFont(13),
    color: '#555',
    marginBottom: 5,
  },
  arrowIcon: {
    marginLeft: 10,
    width: 15,
    height: 15,
  },
  retryButton: {
    alignSelf: 'flex-end',
    marginTop: 10,
    marginRight: 20,
    padding: 10,
    backgroundColor: '#f0f0f0',
    borderRadius: 5,
  },
  retryButtonText: {
    color: '#333',
  },
});

export default CustomSelectorWithRetry;
