import {
  Animated,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity
} from 'react-native';
import { IMAGES } from '../utils/Images';
import { SizeUtils } from '../utils/SizeUtils';
import { COLORS } from '../utils/ColorUtils';
import CustomText from './CustomText';

function RenderHeaderView(
  searchBarOpacity: Animated.Value,
  searchQuery: string,
  setSearchFocused: React.Dispatch<React.SetStateAction<boolean>>,
  onSearchChange: (query: any) => void,
  searchFocused: boolean,
  cancelSearch: () => void,
  data: any,
  placeHolder: string
): React.ReactNode {
  return (
    <>
      <Animated.View
        style={[styles.searchBarContainer, { opacity: searchBarOpacity }]}
      >
        <Image source={IMAGES.searchIcon} style={styles.searchIcon} />
        <TextInput
          style={styles.searchBar}
          placeholder={placeHolder}
          value={searchQuery}
          onFocus={() => setSearchFocused(true)}
          onBlur={() => setSearchFocused(false)}
          onChangeText={onSearchChange}
        />
        {searchQuery.length > 0 || searchFocused ? (
          <TouchableOpacity onPress={cancelSearch}>
            <Image source={IMAGES.cancelIcon} style={styles.cancelIcon} />
          </TouchableOpacity>
        ) : null}
      </Animated.View>
      <CustomText
        style={{
          textAlign: 'right',
          marginRight: 20,
          fontSize: SizeUtils.scaleFont(12),
          color: COLORS.sbsLightenColor(COLORS.sbsDarkerRed, 40)
        }}
        weightType="bold"
        fontType="averta"
      >
        Count:{' '}
        <CustomText
          style={{
            fontSize: SizeUtils.scaleFont(14)
          }}
          weightType="bold"
          fontType="averta"
        >
          {data.length}
        </CustomText>
      </CustomText>
    </>
  );
}
const styles = StyleSheet.create({
  searchBarContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: COLORS.lightGray,
    borderWidth: 0.5,
    borderRadius: 5,
    paddingLeft: 16,
    marginHorizontal: SizeUtils.scaleMarginPadding(20),
    backgroundColor: COLORS.white
  },
  searchBar: {
    flex: 1,
    height: 40
  },
  searchIcon: {
    marginRight: 8,
    width: 20,
    height: 20,
    tintColor: COLORS.lightGray
  },
  cancelIcon: {
    marginRight: 15,
    width: 15,
    height: 15,
    tintColor: COLORS.gray
  }
});
export default RenderHeaderView;
