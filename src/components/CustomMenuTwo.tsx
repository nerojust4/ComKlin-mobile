import * as React from 'react';
import {View} from 'react-native';
import {
  Button,
  Menu,
  Divider,
  Provider as PaperProvider,
} from 'react-native-paper';

interface MenuItem {
  title: string;
  onPress: () => void;
}

interface CustomMenuProps {
  buttonText: string;
  items: MenuItem[];
}

const CustomMenuTwo: React.FC<CustomMenuProps> = ({buttonText, items}) => {
  const [visible, setVisible] = React.useState(false);

  const openMenu = () => setVisible(true);

  const closeMenu = () => setVisible(false);

  return (
    <PaperProvider>
      <View
        style={{
          paddingTop: 50,
          flexDirection: 'row',
          justifyContent: 'center',
        }}>
        <Menu
          visible={visible}
          onDismiss={closeMenu}
          anchor={<Button onPress={openMenu}>{buttonText}</Button>}>
          {items.map((item, index) => (
            <React.Fragment key={index}>
              <Menu.Item
                onPress={() => {
                  item.onPress();
                  closeMenu();
                }}
                title={item.title}
              />
              {index < items.length - 1 && <Divider />}
            </React.Fragment>
          ))}
        </Menu>
      </View>
    </PaperProvider>
  );
};

export default CustomMenuTwo;
