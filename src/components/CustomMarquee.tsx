import React, { useRef, useEffect } from 'react';
import { View, Text, Animated, StyleSheet } from 'react-native';

const useMarquee = (text) => {
  const marqueeRef = useRef(null);
  const animValue = useRef(new Animated.Value(0)).current;
  const animDuration = 5000; // Adjust as needed

  useEffect(() => {
    const textWidth = Math.ceil(
      marqueeRef.current.measure((x, y, width, height, pageX, pageY) => width)
    );
    const animConfig = {
      toValue: -textWidth,
      duration: animDuration,
      useNativeDriver: true,
      isInteraction: false
    };

    const animation = Animated.timing(animValue, animConfig);
    const repeatAnimation = Animated.loop(animation);

    repeatAnimation.start();

    return () => {
      repeatAnimation.stop();
    };
  }, []);

  const CustomMarquee = () => (
    <View style={styles.container}>
      <Animated.View
        ref={marqueeRef}
        style={[styles.marquee, { transform: [{ translateX: animValue }] }]}
      >
        <Text style={styles.text}>{text}</Text>
      </Animated.View>
    </View>
  );

  return CustomMarquee;
};

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
    backgroundColor: '#f0f0f0'
  },
  marquee: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: 10
  },
  text: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black'
  }
});

export default useMarquee;
