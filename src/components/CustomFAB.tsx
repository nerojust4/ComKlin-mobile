import {TouchableOpacity, StyleSheet, Image, ViewStyle} from 'react-native';
import {IMAGES} from '../utils/Images';
import {COLORS} from '../utils/ColorUtils';
import React from 'react';

interface FabProp {
  onPress: () => void;
  style?: ViewStyle;
  viewStyle?: ViewStyle;
}

const CustomFAB: React.FC<FabProp> = ({onPress, style,viewStyle}) => {
  return (
    <TouchableOpacity style={[styles.fab,viewStyle]} onPress={onPress}>
      <Image source={IMAGES.addIcon} style={[styles.fabIcon, style]} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    bottom: 30,
    right: 20,
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: COLORS.sbsMediumDarkRed,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 4,
  },

  fabIcon: {
    width: 30,
    height: 30,
    tintColor: COLORS.white,
  },
});
export default CustomFAB;
