import React, { useState, useRef, useEffect } from 'react';
import { View, Image, StyleSheet, Dimensions, FlatList } from 'react-native';
import Dots from 'react-native-dots-pagination';
import { COLORS } from '../utils/ColorUtils';
import CustomText from './CustomText';
import { IMAGES } from '../../src/utils/Images';
import LoadingIndicator from './LoadingIndicator';

const { width } = Dimensions.get('window');

const staticData = [
  {
    id: 1,
    image: IMAGES.slideOne,
    title: 'Cleanup Events',
    description: 'Organize and participate in community cleanup events.'
  },
  {
    id: 2,
    image: IMAGES.slideNine,
    title: 'Waste Collection',
    description: 'Manage and schedule waste collection services.'
  },
  {
    id: 3,
    image: IMAGES.slideTen,
    title: 'Recycling',
    description: 'Find recycling centers and learn about recycling practices.'
  },
  {
    id: 4,
    image: IMAGES.slideEleven,
    title: 'Volunteer',
    description: 'Sign up as a volunteer for various community activities.'
  },
  {
    id: 5,
    image: IMAGES.slideFive,
    title: 'Community Garden',
    description: 'Participate in and maintain community gardens.'
  },
  {
    id: 6,
    image: IMAGES.slideSix,
    title: 'Report Issues',
    description: 'Report environmental issues and hazards in your area.'
  },
  {
    id: 7,
    image: IMAGES.slideSeven,
    title: 'Environmental Education',
    description: 'Learn about environmental conservation and sustainability.'
  }
];

const CarouselItem = ({ item = {}, isStatic = false }) => {
  const imageSource = isStatic ? item?.image : { uri: item?.image };
  return (
    <View style={styles.itemContainer}>
      <Image source={imageSource} style={styles.image} resizeMode="contain" />
      <View style={styles.textContainer}>
        <CustomText
          fontType="productSans"
          weightType="bold"
          style={styles.title}
        >
          {item?.title}
        </CustomText>
        <CustomText
          fontType="productSans"
          weightType="regular"
          style={styles.description}
        >
          {item?.description}
        </CustomText>
      </View>
    </View>
  );
};

const CustomCarousel = ({ data = [], loading = false, error = '' }) => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const flatListRef = useRef(null);

  const isStatic = !data || data?.length === 0;
  const carouselData = isStatic
    ? staticData
    : data?.filter((item) => item?.isCarousel);

  useEffect(() => {
    const interval = setInterval(() => {
      const nextIndex = (currentIndex + 1) % carouselData?.length;
      setCurrentIndex(nextIndex);
      flatListRef.current.scrollToIndex({ index: nextIndex, animated: true });
    }, 3000);

    return () => clearInterval(interval);
  }, [currentIndex, carouselData.length]);

  return (
    <View style={styles.container}>
      <FlatList
        ref={flatListRef}
        data={carouselData}
        horizontal
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <CarouselItem item={item} isStatic={isStatic} />
        )}
        onMomentumScrollEnd={(event) => {
          const index = Math.round(event.nativeEvent.contentOffset.x / width);
          setCurrentIndex(index);
        }}
      />
      <View style={styles.dotStyle}>
        <Dots
          length={carouselData.length}
          active={currentIndex}
          activeColor={COLORS.sbsDarkenColor(COLORS.green, 30)}
          passiveColor={COLORS.gray}
          passiveDotWidth={10}
          activeDotWidth={10}
          marginHorizontal={5}
        />
      </View>
      {loading && <LoadingIndicator title="" body="" />}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  itemContainer: {
    width,
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    width: '90%',
    height: '70%',
    borderRadius: 10
  },
  textContainer: {
    position: 'absolute',
    bottom: 0,
    alignItems: 'center'
  },
  title: {
    fontSize: 18,
    color: COLORS.sbsDarkenColor(COLORS.green, 30),
    textAlign: 'center',
    paddingVertical: 10
  },
  description: {
    fontSize: 14,
    color: COLORS.sbsDarkenColor(COLORS.green, 10),
    textAlign: 'center'
  },
  dotStyle: {
    height: 40,
    marginTop: 10
  },
  errorText: {
    color: 'red',
    textAlign: 'center'
  }
});

export default CustomCarousel;
