import React from 'react';
import {View, StyleSheet} from 'react-native';
import CustomText from './CustomText';
import {COLORS} from '../utils/ColorUtils';

const EmptyState = ({loading, data, what}) => {
  if (loading) {
    return null;
  }
  if (data && data.length === 0) {
    return (
      <View style={styles.emptyStateContainer}>
        <CustomText
          style={styles.emptyStateText}>{`No ${what} found.`}</CustomText>
      </View>
    );
  }
  return null;
};

const styles = StyleSheet.create({
  emptyStateContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
  },
  emptyStateText: {
    fontSize: 18,
    color: COLORS.gray,
  },
});

export default EmptyState;
