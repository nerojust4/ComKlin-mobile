import React from 'react';
import {View, Text, Image, StyleSheet, Dimensions} from 'react-native';
import {SizeUtils} from '../utils/SizeUtils';
import CustomText from './CustomText';

const {width: screenWidth} = Dimensions.get('window');

interface CarouselItemProps {
  // item: {
  uri: string;
  title: string;
  subtitle: string;
  // };
}

const CarouselItem: React.FC<CarouselItemProps> = item => {
  // console.log('item', item.uri);
  return (
    <View style={styles.container}>
      <Image
        source={typeof item.uri === 'string' ? {uri: item.uri} : item.uri}
        style={styles.image}
      />
      <CustomText fontType="productSans" style={styles.title} weightType="bold">
        {item.title}
      </CustomText>
      <CustomText
        fontType="productSans"
        weightType="bold"
        style={styles.subtitle}>
        {item.subtitle}
      </CustomText>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    width: screenWidth,
  },
  image: {
    width: screenWidth,
    height: screenWidth * 0.6, // Adjust the height as per your design
  },
  title: {
    fontSize: SizeUtils.scaleFont(24),
    fontWeight: 'bold',
    marginVertical: 16,
    textAlign: 'center',
    color: '#6B1110',
  },
  subtitle: {
    fontSize: SizeUtils.scaleFont(16),
    textAlign: 'center',
    color: '#6B1110',
  },
});

export default CarouselItem;
