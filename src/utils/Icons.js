import BackIcon from '../assets/svg/BackIcon';
import CircularCloseIcon from '../assets/svg/CircularCloseIcon';
import EyeCloseIcon from '../assets/svg/EyeCloseIcon';
import EyeOpenIcon from '../assets/svg/EyeOpenIcon';
import FCMBLogo from '../assets/svg/FcmbLogo';
import FaceLivelinessIcon from '../assets/svg/FaceLivelinessIcon';
import LocatorIcon from '../assets/svg/LocatorIcon';
import PadlockIcon from '../assets/svg/Padlock';
import ProfileIcon from '../assets/svg/Profileicon';
import QRPayIcon from '../assets/svg/QRPayIcon';
import TransfersIcon from '../assets/svg/TransfersIcon';
import AirtimeIcon from '../assets/svg/AirtimeIcon';
import BiometricsProfile from '../assets/svg/BiometricsProfile';
import ChainIcon from '../assets/svg/ChainIcon';
import CheckIcon from '../assets/svg/CheckIcon';
import InstructionIcon from '../assets/svg/InstructionsIcon';
import ToggleOffIcon from '../assets/svg/ToggleOffIcon';
import ToggleOnIcon from '../assets/svg/ToggleOnIcon';
import KeyIcon from '../assets/svg/KeyIcon';
import OtpIcon from '../assets/svg/OtpIcon';
import SuccessIcon from '../assets/svg/SuccessIcon';
import PhoneIcon from '../assets/svg/PhoneIcon';
import CalenderIcon from '../assets/svg/CalenderIcon';
import SquareCheckIcon from '../assets/svg/SquareCheckIcon';
import HomeTabIcon from '../assets/svg/HomeTabIcon';
import AirtimeTabIcon from '../assets/svg/AirtimeTabIcon';
import LoansTabIcon from '../assets/svg/LoansTabIcon';
import MoreTabIcon from '../assets/svg/MoreTabIcon';
import UpgradeIcon from '../assets/svg/UpgradeIcon';
import PurpleEditIcon from '../assets/svg/PurpleEditIcon';
import ElectricityIcon from '../assets/svg/ElectricityIcon';
import BettingIcon from '../assets/svg/BettingIcon';
import QRIcon from '../assets/svg/QRIcon';
import CableIcon from '../assets/svg/CableIcon';
import LimitsIcon from '../assets/svg/LimitsIcon';
import OthersIcon from '../assets/svg/OthersIcon';
import MenuTransferIcon from '../assets/svg/MenuTransferIcon';
import MenuAirtimeIcon from '../assets/svg/MenuAirtimeIcon';
import YellowAddIcon from '../assets/svg/YellowAddIcon';
import PurpleAddIcon from '../assets/svg/PurpleAddIcon';
import TransparentAddIcon from '../assets/svg/TransparentAddIcon';
import CloseIcon from '../assets/svg/Close';
import UploadIcon from '../assets/svg/UploadIcon';
import WhiteIntructionIcon from '../assets/svg/WhiteInstruction';
import {COLORS} from './ColorUtils';
import WarningIcon from '../assets/svg/WarningIcon';
import AvatarIcon from '../assets/svg/AvatarIcon';
import CameraIcon from '../assets/svg/CameraIcon';
import DoubleArrowUp from '../assets/svg/DoubleArrowUp';
import DoubleArrowDown from '../assets/svg/DoubleArrowDown';
import CbnIcon from '../assets/svg/CbnIcon';
import EmailIcon from '../assets/svg/EmailIcon';
import AddressIcon from '../assets/svg/AddressIcon';
import CityIcon from '../assets/svg/CityIcon';
import SbsIcon from '../assets/svg/SbsIcon';
import MoreIcon from '../assets/svg/MoreIcon';

export const ICONS = {
  eyeOpen: <EyeOpenIcon />,
  eyeClose: <EyeCloseIcon />,
  circularCloseIcon: <CircularCloseIcon />,
  backArrowIcon: <BackIcon />,
  padlockIcon: <PadlockIcon />,
  profileIcon: <ProfileIcon />,
  qrPayIcon: <QRPayIcon />,
  transferIcon: <TransfersIcon />,
  airtimeIcon: <AirtimeIcon />,
  biometricsProfileIcon: <BiometricsProfile />,
  chainIcon: <ChainIcon />,
  emailIcon: <EmailIcon />,
  checkIcon: color => <CheckIcon color={color} />,
  instructionIcon: <InstructionIcon />,
  toggleOffIcon: <ToggleOffIcon />,
  toggleOnIcon: <ToggleOnIcon />,
  keyIcon: <KeyIcon />,
  otpIcon: <OtpIcon />,
  successIcon: <SuccessIcon />,
  phoneIcon: <PhoneIcon />,
  calenderIcon: <CalenderIcon />,
  squareCheckIcon: color => <SquareCheckIcon color={color} />,
  homeTabIcon: color => <HomeTabIcon color={color} />,
  airtimeTabIcon: color => <AirtimeTabIcon color={color} />,
  loansTabIcon: color => <LoansTabIcon color={color} />,
  moreTabIcon: color => <MoreTabIcon color={color} />,
  transparentAddIcon: <TransparentAddIcon />,
  upgradeIcon: <UpgradeIcon />,
  purpleEditIcon: <PurpleEditIcon />,
  electricityIcon: <ElectricityIcon />,
  bettingIcon: <BettingIcon />,
  qRIcon: <QRIcon />,
  cableIcon: <CableIcon />,
  limitsIcon: <LimitsIcon />,
  othersIcon: <OthersIcon />,
  menuTransferIcon: <MenuTransferIcon />,
  menuAirtimeIcon: <MenuAirtimeIcon />,
  yellowAddIcon: <YellowAddIcon />,
  purpleAddIcon: <PurpleAddIcon />,
  closeIcon: <CloseIcon />,
  uploadIcon: <UploadIcon />,
  whiteInstructionIcon: (color = COLORS.white) => (
    <WhiteIntructionIcon color={color} />
  ),
  warningIcon: <WarningIcon />,
  avatarIcon: <AvatarIcon />,
  cameraIcon: <CameraIcon />,
  doubleArrowUp: <DoubleArrowUp />,
  doubleArrowDown: <DoubleArrowDown />,
  CbnIcon: <CbnIcon />,
  addressIcon: <AddressIcon />,
  cityIcon: <CityIcon />,
  sbsIcon: <SbsIcon />,
  moreIcon: <MoreIcon />,
};
