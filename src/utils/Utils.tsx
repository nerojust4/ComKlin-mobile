import { STRINGS } from './Strings';
import { CommonActions } from '@react-navigation/native';
import { Keyboard, Vibration } from 'react-native';
import { Role } from '../models/Role';
import { User } from '../models/User';

type InputRef<T> = React.RefObject<T>; // Define the type for input refs

export const Utils = {
  goToNewScreen: (
    navigation: any, // Adjust the type to match your navigation prop
    location: string,
    options: Record<string, any> = {},
    isWithinTheStack = true,
    shouldResetStack = false,
    resetIndex: number = 0 // Added parameter to specify the active index
  ) => {
    if (shouldResetStack) {
      navigation.dispatch(
        CommonActions.reset({
          index: resetIndex, // Setting the active route index
          routes: [{ name: location, params: options }]
        })
      );
    } else {
      isWithinTheStack
        ? navigation.push(location, options)
        : navigation.navigate(location, options);
    }
  },

  focusInputRef: <T extends { focus?: () => void }>(inputRef: InputRef<T>) => {
    // Focus on the input element when needed
    if (inputRef.current && typeof inputRef.current.focus === 'function') {
      inputRef.current.focus();
    }
  }
};

export const dismissKeyboard = () => {
  if (Keyboard.isVisible()) {
    Keyboard.dismiss();
  }
};

export const hasFeaturePermission = (user: any, feature: string) => {
  const allowedFeatures = user?.features || [];
  // console.log('allowed', allowedFeatures);

  return allowedFeatures.includes(feature);
};

export const isStudent = (user: any) => {
  return user?.role?.name === 'Student';
};

export const getUserRole = (user: User, roles: Role[]): string | undefined => {
  const userRole = roles?.find((role) => role.id === user?.role?.id);
  return userRole?.name;
};

export const handlePlaceHolderUpdate = (role: string, type: string) => {
  let who = '';
  if (role === 'Student') {
    who = STRINGS.student;
  } else if (role === 'Staff') {
    who = STRINGS.staff;
  } else if (role === 'SuperAdmin') {
    who = STRINGS.superAdmin;
  } else {
    who = STRINGS.admin;
  }
  if (type === 'username') {
    return `${STRINGS.enterUsername} ${who}`;
  }
  if (type === 'email') {
    return `${STRINGS.enterEmailFor} ${who}`;
  }
  if (type === 'password') {
    return `${STRINGS.enterPassword} ${who}`;
  }
};

export const triggerVibration = (pattern = 200, repeat = false) => {
  Vibration.vibrate(pattern, repeat);
};

export const truncateText = (text: string, maxLength: number) => {
  if (text.length > maxLength) {
    return text.substring(0, maxLength) + '...';
  }
  return text;
};

export function getTimeOfDayGreeting() {
  const currentHour = new Date().getHours();

  if (currentHour >= 5 && currentHour < 12) {
    return 'Good Morning,';
  } else if (currentHour >= 12 && currentHour < 18) {
    return 'Good Afternoon,';
  } else {
    return 'Good Evening,';
  }
}

export const validateEmail = (email: string) => {
  // Regular expression for basic email validation
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  // Check if the email matches the regular expression
  return emailRegex.test(email);
};

export const validatePhoneNumber = (phoneNumber: string): boolean => {
  // Regular expression for phone number validation with optional country code and prefix
  const phoneRegex = /^\+?[0-9]{6,14}$/;

  // Check if the phoneNumber matches the regular expression
  return phoneRegex.test(phoneNumber);
};

export const MaskDigits = (phoneNumber: string) => {
  const prefix = phoneNumber.substring(0, 4);
  const maskedDigits = phoneNumber.substring(3, 7).replace(/\d/g, '*');
  const suffix = phoneNumber.substring(7);
  return prefix + maskedDigits + suffix;
};

export const FormatDate = (dateOfBirth: string) => {
  const date = new Date(dateOfBirth);
  const year = date.getUTCFullYear();
  const month = String(date.getUTCMonth() + 1).padStart(2, '0');
  const day = String(date.getUTCDate()).padStart(2, '0');

  return `${year}-${month}-${day}`;
};
