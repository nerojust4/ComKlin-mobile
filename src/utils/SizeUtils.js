import {Dimensions, PixelRatio} from 'react-native';

const {width, height} = Dimensions.get('window');
const guidelineBaseWidth = 375;
const guidelineBaseHeight = 667;

export const SizeUtils = {
  /**
   * Convert pixels to dp (device-independent pixels)
   * @param {number} px - The pixel value to be converted
   * @returns {number} The converted dp value
   */
  pxToDp: px =>
    PixelRatio.roundToNearestPixel((px * width) / guidelineBaseWidth),

  /**
   * Convert pixels to sp (scale-independent pixels)
   * @param {number} px - The pixel value to be converted
   * @returns {number} The converted sp value
   */
  pxToSp: px =>
    PixelRatio.roundToNearestPixel((px * height) / guidelineBaseHeight),

  /**
   * Calculate responsive width based on a percentage of the screen width
   * @param {number} percentage - The percentage of the screen width
   * @returns {number} The responsive width value
   */
  responsiveWidth: percentage => (width * percentage) / 100,

  /**
   * Calculate responsive height based on a percentage of the screen height
   * @param {number} percentage - The percentage of the screen height
   * @returns {number} The responsive height value
   */
  responsiveHeight: percentage => (height * percentage) / 100,

  /**
   * Scale font size based on the screen dimensions
   * @param {number} size - The original font size
   * @returns {number} The scaled font size
   */
  scaleFont: size =>
    PixelRatio.roundToNearestPixel((size * width) / guidelineBaseWidth),

  /**
   * Scale margin and padding values based on the screen dimensions
   * @param {number} value - The original margin or padding value
   * @returns {number} The scaled value
   */
  scaleMarginPadding: value =>
    PixelRatio.roundToNearestPixel((value * width) / guidelineBaseWidth),

  /**
   * Get size range for different devices
   * @returns {'small' | 'medium' | 'large'} The size range based on the device width
   */
  getSizeRange: () => {
    if (width < 340) {
      return 'small';
    } else if (width < 600) {
      return 'medium';
    } else {
      return 'large';
    }
  },

  /**
   * Get screen width
   * @returns {number} The width of the screen
   */
  getScreenWidth: () => width,

  /**
   * Get screen height
   * @returns {number} The height of the screen
   */
  getScreenHeight: () => height,
};
