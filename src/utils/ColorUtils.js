// ColorUtils.js
export const ColorUtils = {
  // Shade a color by a given percentage (lighten or darken)
  shadeColor: (color, percent) => {
    const f = parseInt(color.slice(1), 16);
    const t = percent < 0 ? 0 : 255;
    const p = percent < 0 ? percent * -1 : percent;
    const R = f >> 16;
    const G = (f >> 8) & 0x00ff;
    const B = f & 0x0000ff;
    return `#${(
      0x1000000 +
      (Math.round((t - R) * p) + R) * 0x10000 +
      (Math.round((t - G) * p) + G) * 0x100 +
      (Math.round((t - B) * p) + B)
    )
      .toString(16)
      .slice(1)}`;
  },

  // Generate a random hex color
  randomHexColor: () => `#${Math.floor(Math.random() * 16777215).toString(16)}`,

  // Add other color-related utility functions here
};

/**
 * Converts a hex color to an RGBA color string with the specified alpha value.
 * @param {string} hex - The hex color string (e.g., '#FF0000').
 * @param {number} alpha - The alpha value (0 to 1).
 * @returns {string} The RGBA color string (e.g., 'rgba(255, 0, 0, 0.5)').
 */
export const hexToRgba = (hex, alpha) => {
  if (!/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
    throw new Error('Invalid hex color');
  }

  let c = hex.substring(1).split('');
  if (c.length === 3) {
    c = [c[0], c[0], c[1], c[1], c[2], c[2]];
  }
  const r = parseInt(c[0] + c[1], 16);
  const g = parseInt(c[2] + c[3], 16);
  const b = parseInt(c[4] + c[5], 16);

  return `rgba(${r}, ${g}, ${b}, ${alpha})`;
};

export const COLORS = {
  // General Colors
  primary: '#3a48bb',
  secondary: '#5D2DFD',
  transparent: '#00000000',

  // Text Colors
  text: 'rgb(44, 63, 94)',
  labelText: '#868F9F',
  textInput: '#2C3F5E',

  // Background Colors
  background: '#fbfbfb',
  grayBackground: '#FAFBFC',
  lightShadeBlue: '#BBB9E8',
  fadePinkBackground: '#fefdff',
  lightShadePink: '#f4eef6',

  // Button Colors
  buttonGreen: '#37E39F',
  buttonPurple: '#515DF1',

  // Status Colors
  successGreen: '#37E39F',
  errorRed: '#F22121',

  // Grayscale
  white: '#ffffff',
  black: '#000000',
  gray: '#6A6A6A',
  darkGray: '#2C3F5E',
  lightGray: '#bdbdbd',
  textGray: '#4F4F4F',
  textDark: '#222823',
  semiDarkGray: '#334335',
  lightTextGray: '#535353',
  borderGray: '#E7E7E7',
  iconGray: '#8B9096',

  // Shades of light gray
  lightGray1: '#f5f5f5',
  lightGray2: '#e0e0e0',
  lightGray3: '#d3d3d3',
  lightGray4: '#c0c0c0',
  lightGray5: '#a9a9a9',
  lightGray6: '#BDBDBD',

  // Additional Colors
  // Variant Color
  sbsRed: '#cf2e2e',
  // Method to generate a lighter shade
  sbsLightRed: '#e45656',
  // Method to generate a darker shade
  sbsDarkRed: '#a11b1b',
  // Method to generate a more saturated variant
  sbsMoreSaturatedRed: '#d12424',
  // Method to generate a less saturated variant
  sbsLessSaturatedRed: '#cc5757',
  // Method to shift the hue
  sbsHueShiftedRed: '#2ecfcf',
  // Light shades
  sbsLightestRed: '#ff7a7a',
  sbsLightRed: '#ff5757',
  sbsLighterRed: '#ff3434',
  sbsLightRed: '#ff1212',

  // Medium shades
  sbsMediumRed: '#cf2e2e',
  sbsMediumDarkRed: '#a11b1b',

  // Dark shades
  sbsDarkRed: '#8c1717',
  semiDarkRed: '#F34E4E',
  sbsDarkerRed: '#5e0e0e',
  sbsDarkestRed: '#400808',

  //variations
  primary: '#cf2e2e',
  primaryLight: '#e57373',
  primaryVeryLight: '#ffcdd2',
  primaryDark: '#b71c1c',
  primaryVeryDark: '#8e0000',
  black: '#000000',
  darkGrey: '#333333',
  lightGrey: '#666666',
  veryLightGrey: '#999999',
  white: '#ffffff',
  offWhite: '#f5f5f5',
  complementaryTeal: '#009688',
  complementaryIndigo: '#3f51b5',
  // Method to generate a lighter shade
  sbsLightenColor: (color, percent) => {
    const num = parseInt(color.replace('#', ''), 16);
    const amt = Math.round(2.55 * percent);
    const R = (num >> 16) + amt;
    const G = ((num >> 8) & 0x00ff) + amt;
    const B = (num & 0x0000ff) + amt;

    return (
      '#' +
      (
        0x1000000 +
        (R < 255 ? (R < 1 ? 0 : R) : 255) * 0x10000 +
        (G < 255 ? (G < 1 ? 0 : G) : 255) * 0x100 +
        (B < 255 ? (B < 1 ? 0 : B) : 255)
      )
        .toString(16)
        .slice(1)
        .toUpperCase()
    );
  },

  // Method to generate a darker shade
  sbsDarkenColor: (color, percent) => {
    const num = parseInt(color.replace('#', ''), 16);
    const amt = Math.round(2.55 * percent);
    const R = (num >> 16) - amt;
    const G = ((num >> 8) & 0x00ff) - amt;
    const B = (num & 0x0000ff) - amt;

    return (
      '#' +
      (
        0x1000000 +
        (R < 255 ? (R < 1 ? 0 : R) : 255) * 0x10000 +
        (G < 255 ? (G < 1 ? 0 : G) : 255) * 0x100 +
        (B < 255 ? (B < 1 ? 0 : B) : 255)
      )
        .toString(16)
        .slice(1)
        .toUpperCase()
    );
  },

  blue: 'blue',
  darkSlateBlue: 'darkslateblue',
  fcmbPurpleLight2: '#CAB8D6',
  fcmbPurpleLight: '#E4D8EB',
  fcmbPurpleLighter: '#efe6f3',
  fcmbPurple: '#5F138D',
  fcmbPurpleText: '#5C068C',
  fcmbOrange: '#FAB613',
  fcmbYellow: '#FBBF24',
  darkAsh: '#4F4F4F',
  lightRed: '#ff8989',
  lightPurple: '#eddaeb',
  green: '#09B47C',
  grayWhite: '#fafafa',
  fcmbBrown: '#78350F',
  veryLightRed: '#fbe9e9',
};
