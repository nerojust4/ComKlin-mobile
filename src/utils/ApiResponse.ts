// src/utils/apiResponse.ts
interface ApiResponse<T> {
  status: boolean;
  result: T | null;
  errors: {message: string}[];
}

export default ApiResponse;
