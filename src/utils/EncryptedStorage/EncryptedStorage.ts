import EncryptedStorage from 'react-native-encrypted-storage';

const EncryptedStorageUtil = {
  // Function to save data to EncryptedStorage
  saveData: async (key: string, value: any) => {
    try {
      await EncryptedStorage.setItem(key, JSON.stringify(value));
      // console.log(`Data saved successfully for key: ${key} value: ${value}`);
    } catch (error) {
      console.error(`Error saving data for key ${key}:`, error);
    }
  },

  // Function to retrieve data from EncryptedStorage
  getData: async <T>(key: string): Promise<T | null> => {
    try {
      const value = await EncryptedStorage.getItem(key);
      if (value !== null) {
        return JSON.parse(value);
      } else {
        // console.log(`No data found for key: ${key}`);
        return null;
      }
    } catch (error) {
      console.error(`Error retrieving data for key ${key}:`, error);
      return null;
    }
  },

  // Function to remove data from EncryptedStorage
  removeData: async (key: string) => {
    try {
      await EncryptedStorage.removeItem(key);
      console.log(`Data removed successfully for key: ${key}`);
    } catch (error) {
      console.error(`Error removing data for key ${key}:`, error);
    }
  },

  // Function to clear all data from EncryptedStorage
  clearAllData: async () => {
    try {
      await EncryptedStorage.clear();
      console.log('All data cleared successfully');
    } catch (error) {
      console.error('Error clearing all data:', error);
    }
  },

  // Additional utility functions can be added here as needed
};

export default EncryptedStorageUtil;
