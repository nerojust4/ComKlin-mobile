export const STRINGS = {
  // button string constants
  login: 'Log In',
  getStarted: 'Get Started',
  decline: 'Decline',
  accept: 'Accept',
  complete: 'Complete',
  goToLogin: 'Go to Login',
  proceed: 'Proceed',
  continue: 'Continue',
  confirm: 'Confirm',
  username: 'Username',
  enterUsername: 'Enter username for',
  enterEmailFor: 'Enter email for',
  password: 'Password',
  enterPassword: 'Enter password for',

  student: 'Student',
  admin: 'Admin',
  superAdmin: 'SuperAdmin',
  staff: 'Staff',

  // Onboarding Title
  titleTextOne: 'Student Records Management',
  titleTextTwo: 'Staff Records Management',
  titleTextThree: 'Education Anywhere, Anytime',
  titleTextFour: 'Create and Manage School Schedules',

  // Onboarding Subtext
  titleSubtextOne:
    'Access and manage all student records and your school profile',
  titleSubtextTwo:
    'Access and manage staff records and other user details on the app',
  titleSubtextThree:
    'Access educational resources and manage school information on the go',
  titleSubtextFour: 'Effortlessly create and track school schedules with ease',

  goToDashboard: 'Go to Dashboard',
  signUp: 'Sign Up',
  selectUserOption: 'Select a category',
  optionOne: 'Admin',
  optionTwo: 'Staff',
  optionThree: 'Teacher',
  optionFour: 'Student',
  
  email: 'Email',
  enterEmail: 'Enter your email address',

  firstName:'First name',
  lastName:'Last name',
  phoneNumber:'Phone Number',
  address:'Address',
  gender:'Gender',
  country:"Country",
  state:'State',
  city:'City',
  gender:'Gender',

  dobLabel:'Date of Birth'
};
