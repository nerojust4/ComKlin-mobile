import { IMAGES } from './Images';
import { STRINGS } from './Strings';

export const ROUTE_LABELS = {
  MAIN: 'Main',
  BOTTOM_TABS: 'BottomTabs',
  HOME: 'Home',
  PROFILE: 'Profile',
  FEED: 'Feed',
  ENROLLMENTS: 'Enrollments',
  NOTIFICATION: 'Notification',
  RESOURCES: 'Resources',

  USERS: 'Users',
  CREATE_USER: 'CreateUser',
  USER_DETAIL: 'UserDetail',
  EVENT_LIST: 'EventList',
  EVENT_DETAIL: 'EventDetail',

  FINANCIALS: 'Financials',
  ANALYTICS: 'Analytics',
  ANNOUNCEMENTS: 'Announcements',
  REPORTS: 'Reports',

  CREATE_CLEANING: 'Create Cleaning',
  JOIN: 'Join',
  MESSAGE: 'Message',
  SETTINGS: 'Settings',

  ONBOARDING: 'Onboarding',
  LOGIN: 'Login',
  REGISTER: 'Register',
  RESET_PASSWORD: 'ResetPassword'
};
