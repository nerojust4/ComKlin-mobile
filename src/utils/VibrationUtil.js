// utils/VibrationUtil.js
import {Vibration, Platform, PermissionsAndroid} from 'react-native';

/**
 * Triggers a vibration on the device.
 * @param {number | number[]} pattern - The vibration duration or pattern.
 * @param {boolean} [repeat=false] - Whether to repeat the vibration pattern.
 */
export const triggerVibration = async (pattern = 200, repeat = false) => {
  if (Platform.OS === 'android') {
    const hasPermission = await requestVibrationPermission();
    if (!hasPermission) {
      console.log('Vibration permission denied');
      return;
    }
  }
  Vibration.vibrate(pattern, repeat);
};

/**
 * Cancels any ongoing vibration.
 */
export const cancelVibration = () => {
  Vibration.cancel();
};

/**
 * Requests vibration permission on Android.
 * @returns {Promise<boolean>} - Whether the permission was granted.
 */
const requestVibrationPermission = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.VIBRATE,
      {
        title: 'Vibration Permission',
        message: 'This app needs access to vibration to provide feedback.',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('Vibration permission granted');
      return true;
    } else {
      console.log('Vibration permission denied');
      return false;
    }
  } catch (err) {
    console.warn('Error requesting vibration permission:', err);
    return false;
  }
};
