import {TEST_BASE_URL, PROD_BASE_URL} from "@env"

// config.js
const Config = {
  development: {
    apiUrl: TEST_BASE_URL,
  },
  staging: {
    apiUrl: TEST_BASE_URL,
  },
  production: {
    apiUrl: PROD_BASE_URL,
  },
};


export default {Config};
