import {SerializedError} from '@reduxjs/toolkit';
import {AxiosError} from 'axios';
import ApiResponse from '../services/models/ApiResponse';

export const getAxiosErrorMessage = (err: unknown): string | undefined => {
    const error = err as AxiosError<ApiResponse<any>>
    const normalAxiosError = err as AxiosError<any>
    const toolkitError = (err as AxiosError<unknown, any> | SerializedError | undefined)?.message
    let responseError
    if (normalAxiosError?.response?.data?.error_description) {
      responseError = normalAxiosError?.response?.data?.error_description
    }
    if (error?.response?.data?.errors?.length) {
        responseError = error?.response?.data?.errors[0]?.message
    }
    const requestError = error?.message

  return responseError ?? requestError ?? toolkitError;
};
