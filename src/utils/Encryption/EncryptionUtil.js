import CryptoJS from 'react-native-crypto-js';
import { RSA } from 'react-native-rsa-native';
import { ENCRIPTION_PUBLIC_KEY, ENCRYPTION_PRIVATE_KEY } from '@env';
import { AndroidEncryptionDecryption, IOSEncryptionDecryption } from '../NativeModuleUtils';


// Function to generate a random string of specified length
export const generateRandomString = (length) => {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

export const encryptRequestNative = async (requestData) => {
  const encrptedData = {
    body: "",
    secretKey: ""
  }
  const requestDataString = JSON.stringify(requestData);
  if (Platform.OS === "android") {
    return new Promise((resolve) => {
      AndroidEncryptionDecryption.getAesSecretKey((aesSecretKey) => {
        AndroidEncryptionDecryption.encryptBody(requestDataString, aesSecretKey, (encryptedRequestData) => {
          encrptedData.body = encryptedRequestData
          AndroidEncryptionDecryption.encryptSecretKey(aesSecretKey, (encryptedSecretKey) => {
            encrptedData.secretKey = encryptedSecretKey
            resolve(encrptedData)
          })
        })
      })
    })
  } else {
    return new Promise((resolve) => {
      IOSEncryptionDecryption?.encryptData(
        requestDataString, ENCRIPTION_PUBLIC_KEY, (error, response) => {
          const data = JSON.parse(response.data)
          encrptedData.body = data.body
          encrptedData.secretKey = data.secretKey
          resolve(encrptedData)
        }
      )
    })
  }
}

// Function to encrypt request data
export const encryptRequest = async (requestData, publicKey) => {
  console.log('Starting encryption for requestData:', requestData, publicKey);

  // Convert requestData to a string
  const requestDataString = JSON.stringify(requestData);
  console.log('Converted requestData to string:', requestDataString);

  // Generate random symmetric key
  const secretKey = generateRandomString(16); // 128-bit key

  // Encrypt request data using symmetric key and AES algorithm
  const encryptedRequestData = CryptoJS.AES.encrypt(
    requestDataString,
    secretKey,
  ).toString();

  // Encrypt symmetric key using server's public key and RSA algorithm
  const encryptedSecretKey = await RSA.encrypt(
    secretKey,
    publicKey,
  );

  // Log final encryption payload
  console.log('Final encryption payload:', {
    body: encryptedRequestData,
    secretKey: encryptedSecretKey,
  });

  return {
    body: encryptedRequestData,
    secretKey: encryptedSecretKey,
  };
};

export const decryptResponseNative = async (encryptedResponse) => {
  if (Platform.OS === "android") {
    return new Promise((resolve) => {
      AndroidEncryptionDecryption.getDecryptedSecretKey(encryptedResponse.secretKey, (decryptedSecretKey) => {
        AndroidEncryptionDecryption.decryptBodyWithSecretKey(decryptedSecretKey, encryptedResponse.body, (decryptedResponseData) => {
          resolve(decryptedResponseData)
        })
      })
    })
  } else {
    return new Promise((resolve) => {
      IOSEncryptionDecryption?.decryptData(
        encryptedResponse.body,
        encryptedResponse.secretKey,
        ENCRYPTION_PRIVATE_KEY,
        (error, response) => {
          resolve(response.data)
        }
      )
    })
  }
}

// Function to decrypt response data
export const decryptResponse = async (encryptedResponse, privateKey) => {
  // Decrypt symmetric key using client's private key and RSA algorithm
  const decryptedSecretKey = await RSA.decrypt(
    encryptedResponse.secretKey,
    privateKey,
  );

  console.log({ decryptedSecretKey })

  // Decrypt response data using decrypted symmetric key and AES algorithm
  const decryptedResponseData = CryptoJS.AES.decrypt(
    encryptedResponse.body,
    decryptedSecretKey,
  ).toString(CryptoJS.enc.Utf8);

  console.log('Decrypted responseData:', decryptedResponseData);

  return decryptedResponseData;
};
