// src/utils/encryption.ts
import CryptoJS from 'crypto-js';

// Encryption secret key (should be stored securely)
const SECRET_KEY = 'your-secret-key';

export const encryptData = (data: any) => {
  const ciphertext = CryptoJS.AES.encrypt(
    JSON.stringify(data),
    SECRET_KEY,
  ).toString();
  return ciphertext;
};

export const decryptData = (ciphertext: string) => {
  const bytes = CryptoJS.AES.decrypt(ciphertext, SECRET_KEY);
  const decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
  return decryptedData;
};
