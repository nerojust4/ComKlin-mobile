import * as DeviceInfo from 'react-native-device-info';
import {NetworkInfo} from 'react-native-network-info';

export const DeviceInfoWrapper = {
  getDeviceId: () => DeviceInfo.getDeviceId(),
  getDeviceName: () => DeviceInfo.getDeviceName(),
  getAppVersion: () => DeviceInfo.getVersion(),
  getManufacturer: () => DeviceInfo.getManufacturer(),
  getModel: () => DeviceInfo.getModel(),
  getSystemName: () => DeviceInfo.getSystemName(),
  getSystemVersion: () => DeviceInfo.getSystemVersion(),
  getBundleId: () => DeviceInfo.getBundleId(),
  getBuildNumber: () => DeviceInfo.getBuildNumber(),
  getReadableVersion: () => DeviceInfo.getReadableVersion(),
  getUniqueId: () => DeviceInfo.getUniqueId(),
  getCarrier: () => DeviceInfo.getCarrier(),
  getIPAddress: async () => {
    const ipAddress = await NetworkInfo.getIPV4Address();
    return ipAddress;
  },
  appendDeviceInfoToRequest: (request: any) => {
    return {
      ...request,
      deviceId: DeviceInfo.getDeviceId(),
      appVersion: DeviceInfo.getVersion(),
      phoneType: DeviceInfo.getDeviceType(),
      model: DeviceInfoWrapper.getModel(),
      client: DeviceInfoWrapper.getSystemName(),
      systemName: DeviceInfoWrapper.getSystemName(),
      systemVersion: DeviceInfoWrapper.getSystemVersion(),
      bundleId: DeviceInfoWrapper.getBundleId(),
    };
  },
};
