import React from 'react';
import {Svg, Path, LinearGradient, Stop} from 'react-native-svg';

const BackIcon = () => {
  return (
    <Svg width="24" height="24" viewBox="0 0 24 24" fill="none">
      <Path
        d="M20 12H4"
        stroke="url(#paint0_linear_33476_2793)"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M10 18L4 12L10 6"
        stroke="url(#paint1_linear_33476_2793)"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <LinearGradient
        id="paint0_linear_33476_2793"
        x1="4"
        y1="12.5"
        x2="18.758"
        y2="12.1784"
        gradientUnits="userSpaceOnUse">
        <Stop stopColor="#60088C" />
        <Stop offset="1" stopColor="#A11E90" />
      </LinearGradient>
      <LinearGradient
        id="paint1_linear_33476_2793"
        x1="4"
        y1="12"
        x2="9.53686"
        y2="11.9962"
        gradientUnits="userSpaceOnUse">
        <Stop stopColor="#60088C" />
        <Stop offset="1" stopColor="#A11E90" />
      </LinearGradient>
    </Svg>
  );
};

export default BackIcon;
