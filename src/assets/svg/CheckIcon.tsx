import React from "react";
import { ClipPath, Defs, G, Path, Rect, Svg } from "react-native-svg";
import { COLORS } from "../../utils/ColorUtils";

const CheckIcon = ({color = COLORS.fcmbOrange}) => {
    return (
        <Svg width="28" height="28" viewBox="0 0 28 28" fill="none">
            <Path fillRule="evenodd" clipRule="evenodd" d="M14 28C21.4558 28 27.5 21.8937 27.5 14.3612C27.5 6.82872 21.4558 0.722412 14 0.722412C6.54416 0.722412 0.5 6.82872 0.5 14.3612C0.5 21.8937 6.54416 28 14 28Z" fill={color} />
            <Path d="M11.241 19.9178C11.0083 19.9178 10.795 19.8324 10.6205 19.6402L6.76177 15.3914C6.41274 15.0071 6.41274 14.4093 6.76177 14.025C7.1108 13.6407 7.65374 13.6407 8.00277 14.025L11.2604 17.5906L18.9972 9.09292C19.3463 8.70861 19.8892 8.70861 20.2382 9.09292C20.5873 9.47724 20.5873 10.0751 20.2382 10.4594L11.8809 19.6402C11.687 19.8324 11.4737 19.9178 11.241 19.9178Z" fill="white" />
        </Svg>
    )
}

export default CheckIcon