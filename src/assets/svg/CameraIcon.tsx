import React from "react";
import { Circle, Path, Svg } from "react-native-svg";
import { COLORS } from "../../utils/ColorUtils";

const CameraIcon = () => {
    return (
        <Svg width="20" height="20" viewBox="0 0 24 24" fill={COLORS.background} stroke="currentColor" color={COLORS.fcmbPurple} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
            <Path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></Path>
            <Circle cx="12" cy="13" r="4"></Circle>
        </Svg>
    )
}

export default CameraIcon