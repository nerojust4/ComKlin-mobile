import React from 'react';
import {ClipPath, Defs, G, Path, Rect, Svg} from 'react-native-svg';
import {COLORS} from '../../utils/ColorUtils';

const MoreIcon = () => {
  return (
    <Svg
      width="250px"
      height="250px"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <G id="SVGRepo_bgCarrier" stroke-width="0" />

      <G
        id="SVGRepo_tracerCarrier"
        stroke-linecap="round"
        stroke-linejoin="round"
      />

      <G id="SVGRepo_iconCarrier">
        <Path
          opacity="0.1"
          d="M3 12C3 4.5885 4.5885 3 12 3C19.4115 3 21 4.5885 21 12C21 19.4115 19.4115 21 12 21C4.5885 21 3 19.4115 3 12Z"
          fill={COLORS.sbsRed}
        />
        <Path
          d="M3 12C3 4.5885 4.5885 3 12 3C19.4115 3 21 4.5885 21 12C21 19.4115 19.4115 21 12 21C4.5885 21 3 19.4115 3 12Z"
          stroke="#cf2e2e"
          stroke-width="2"
        />
        <Path
          d="M12.0049 16.005L12.0049 15.995"
          stroke="#cf2e2e"
          stroke-width="2"
          stroke-linecap="round"
          stroke-linejoin="round"
        />
        <Path
          d="M12.0049 12.005L12.0049 11.995"
          stroke="#cf2e2e"
          stroke-width="2"
          stroke-linecap="round"
          stroke-linejoin="round"
        />
        <Path
          d="M12.0049 8.005L12.0049 7.995"
          stroke={COLORS.sbsRed}
          stroke-width="2"
          stroke-linecap="round"
          stroke-linejoin="round"
        />
      </G>
    </Svg>
  );
};

export default MoreIcon;
