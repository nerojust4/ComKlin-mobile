import React from "react";
import { Path, Svg } from "react-native-svg";
import { COLORS } from "../../utils/ColorUtils";

const DoubleArrowUp = ({ color = COLORS.fcmbPurple }) => {
    return (
        <Svg width="30px" height="24px" viewBox="0 0 24 24" fill="none">
            <Path d="M5 19L11.2929 12.7071C11.6834 12.3166 12.3166 12.3166 12.7071 12.7071L19 19" stroke={color} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            <Path d="M5 11L11.2929 4.70711C11.6834 4.31658 12.3166 4.31658 12.7071 4.70711L19 11" stroke={color} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
        </Svg>
    )
}

export default DoubleArrowUp