import React from 'react';
import { Path, Svg } from 'react-native-svg';

const UpgradeIcon = () => {
  return (
    <Svg width="16" height="16" viewBox="0 0 16 16" fill="none">
      <Path
        d="M11.333 7.33325L7.99967 3.99992L4.66634 7.33325"
        stroke="#5C068C"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M11.333 12L7.99967 8.66667L4.66634 12"
        stroke="#5C068C"
        strokeWidth="2"
        strokeLinecap="round"
        stroke-linejoin="round"
      />
    </Svg>
  );
};

export default UpgradeIcon;
