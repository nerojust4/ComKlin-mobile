import React from "react";
import { COLORS } from "../../utils/ColorUtils";
import { ClipPath, Defs, G, Path, Rect, Svg } from "react-native-svg";

const HomeTabIcon = ({ color = COLORS.iconGray }) => {
    return (
        <Svg width="17" height="18" viewBox="0 0 17 18" fill="none">
            <G clipPath="url(#clip0_32705_7496)">
                <Path d="M13 18H4.5C2.43225 18 0.75 16.1072 0.75 13.7806V7.86401C0.75 6.55632 1.30172 5.30216 2.22581 4.50909L6.47581 0.861832C7.81484 -0.287277 9.68515 -0.287277 11.0242 0.861832L15.2742 4.50909C16.1983 5.30211 16.75 6.55629 16.75 7.86401V13.7806C16.75 16.1072 15.0677 18 13 18ZM8.75 1.40541C8.21503 1.40541 7.68028 1.5969 7.23388 1.97995L2.98388 5.6272C2.36782 6.15593 2 6.99203 2 7.86385V13.7805C2 15.3315 3.1215 16.5934 4.5 16.5934H13C14.3785 16.5934 15.5 15.3315 15.5 13.7805V7.86385C15.5 6.99206 15.1322 6.15593 14.5161 5.62724L10.2661 1.97998C9.81985 1.597 9.28485 1.40541 8.75 1.40541Z" fill={color} />
            </G>
            <Defs>
                <ClipPath id="clip0_32705_7496">
                    <Rect width="16" height="18" fill="white" transform="translate(0.75)" />
                </ClipPath>
            </Defs>
        </Svg>
    )
}

export default HomeTabIcon