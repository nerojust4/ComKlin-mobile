import React from 'react';
import {Rect, Svg} from 'react-native-svg';

const OthersIcon = () => {
  return (
    <Svg width="25" height="24" viewBox="0 0 25 24" fill="none">
      <Rect
        x="1.5"
        y="1"
        width="9"
        height="9"
        rx="1"
        stroke="#F5A800"
        stroke-width="2"
      />
      <Rect
        x="1.5"
        y="14"
        width="9"
        height="9"
        rx="1"
        stroke="#F5A800"
        stroke-width="2"
      />
      <Rect
        x="14.5"
        y="1"
        width="9"
        height="9"
        rx="1"
        stroke="#F5A800"
        stroke-width="2"
      />
      <Rect
        x="14.5"
        y="14"
        width="9"
        height="9"
        rx="1"
        stroke="#F5A800"
        stroke-width="2"
      />
    </Svg>
  );
};

export default OthersIcon;
