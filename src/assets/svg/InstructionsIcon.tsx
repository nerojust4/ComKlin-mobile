import React from "react";
import { Circle, ClipPath, Defs, G, Path, Rect, Svg } from "react-native-svg";

const InstructionIcon = () => {
    return (
        <Svg width="16" height="17" viewBox="0 0 16 17" fill="none">
            <Path fillRule="evenodd" clipRule="evenodd" d="M7.99967 14.8486C11.6816 14.8486 14.6663 11.8638 14.6663 8.18193C14.6663 4.50003 11.6816 1.51526 7.99967 1.51526C4.31778 1.51526 1.33301 4.50003 1.33301 8.18193C1.33301 11.8638 4.31778 14.8486 7.99967 14.8486Z" stroke="#F34E4E" strokeWidth="2" strokeLinecap="round" stroke-linejoin="round" />
            <Path d="M8 10.8486V8.18188" stroke="#F34E4E" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            <Circle cx="7.99967" cy="5.51518" r="0.666667" fill="#F34E4E" />
        </Svg>
    )
}

export default InstructionIcon