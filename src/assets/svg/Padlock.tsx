import React from 'react';
import {Path, Svg} from 'react-native-svg';
import {COLORS} from '../../utils/ColorUtils';

const PadlockIcon = () => {
  return (
    <Svg width="20" height="21" viewBox="0 0 20 21" fill="none">
      <Path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M2.5 11.2031C2.5 10.0986 3.39543 9.20312 4.5 9.20312H15.5C16.6046 9.20312 17.5 10.0986 17.5 11.2031V16.3698C17.5 17.4744 16.6046 18.3698 15.5 18.3698H4.5C3.39543 18.3698 2.5 17.4744 2.5 16.3698V11.2031Z"
        stroke={COLORS.lightGray6}
        stroke-width="2"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <Path
        d="M5.83301 9.20313V5.86979C5.83301 3.56861 7.69849 1.70313 9.99967 1.70312C12.3009 1.70312 14.1663 3.56861 14.1663 5.86979V9.20313"
        stroke={COLORS.lightGray6}
        stroke-width="2"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
    </Svg>
  );
};

export default PadlockIcon;
