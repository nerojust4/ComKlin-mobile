import React from "react";
import { COLORS } from "../../utils/ColorUtils";
import { ClipPath, Defs, G, Path, Rect, Svg } from "react-native-svg";

const MoreTabIcon = ({ color = COLORS.iconGray }) => {
    return (
        <Svg width="18" height="16" viewBox="0 0 18 16" fill="none">
            <G clipPath="url(#clip0_32705_7518)">
                <Path d="M16.9624 1.99068H6.29011C5.85266 1.99068 5.49805 1.68273 5.49805 1.24535C5.49805 0.807957 5.85266 0.5 6.29011 0.5H16.9624C18.0125 0.541452 18.0133 1.94887 16.9624 1.99068Z" fill={color} />
                <Path d="M15.8996 8.74557H1.54207C1.10461 8.74557 0.75 8.39102 0.75 8.00022C0.75 7.56285 1.10461 7.25488 1.54207 7.25488H15.8996C16.9497 7.29634 16.9505 8.70373 15.8996 8.74557Z" fill={color} />
                <Path d="M16.9619 15.5004H8.82331C8.38587 15.5004 8.03125 15.1925 8.03125 14.7551C8.03125 14.3177 8.38587 14.0098 8.82331 14.0098H16.9619C18.012 14.0512 18.0128 15.4586 16.9619 15.5004Z" fill={color} />
            </G>
            <Defs>
                <ClipPath id="clip0_32705_7518">
                    <Rect width="17" height="15" fill="white" transform="translate(0.75 0.5)" />
                </ClipPath>
            </Defs>
        </Svg>
    )
}

export default MoreTabIcon