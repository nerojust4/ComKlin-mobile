import React from 'react';
import {Path, Svg} from 'react-native-svg';
import {COLORS} from '../../utils/ColorUtils';

const EyeOpenIcon = () => {
  return (
    <Svg width="22" height="21" viewBox="0 0 22 21" fill="none">
      <Path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M1.83301 10.0363C1.83301 10.0363 5.16634 3.36963 10.9997 3.36963C16.833 3.36963 20.1663 10.0363 20.1663 10.0363C20.1663 10.0363 16.833 16.703 10.9997 16.703C5.16634 16.703 1.83301 10.0363 1.83301 10.0363Z"
        stroke={COLORS.lightGray}
        stroke-width="2"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <Path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M11 12.5364C12.3807 12.5364 13.5 11.4171 13.5 10.0364C13.5 8.65567 12.3807 7.53638 11 7.53638C9.61929 7.53638 8.5 8.65567 8.5 10.0364C8.5 11.4171 9.61929 12.5364 11 12.5364Z"
        stroke={COLORS.lightGray}
        stroke-width="2"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
    </Svg>
  );
};

export default EyeOpenIcon;
