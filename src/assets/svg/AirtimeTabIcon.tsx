import React from "react";
import { COLORS } from "../../utils/ColorUtils";
import { ClipPath, Defs, G, Path, Rect, Svg } from "react-native-svg";

const AirtimeTabIcon = ({ color = COLORS.iconGray }) => {
    return (
        <Svg width="14" height="18" viewBox="0 0 14 18" fill="none">
            <G clipPath="url(#clip0_32705_7508)">
                <Path d="M9.58806 18H4.91194C2.68597 18 0.875 16.1075 0.875 13.7812V4.21875C0.875 1.89253 2.68597 0 4.91194 0H9.58806C11.814 0 13.625 1.89253 13.625 4.21875V13.7812C13.625 16.1075 11.814 18 9.58806 18ZM5.73746 15.6861H8.89176C9.77283 15.6485 9.77217 14.2986 8.89176 14.2613H5.73746C5.3707 14.2613 5.0734 14.5803 5.0734 14.9738C5.0734 15.3672 5.3707 15.6862 5.73746 15.6862V15.6861ZM4.91194 1.40625C3.42796 1.40625 2.22064 2.66794 2.22064 4.21875V13.7812C2.22064 15.3321 3.42796 16.5938 4.91194 16.5938H9.58806C11.072 16.5938 12.2794 15.3321 12.2794 13.7812V4.21875C12.2794 2.66794 11.072 1.40625 9.58806 1.40625H4.91194Z" fill={color} />
            </G>
            <Defs>
                <ClipPath id="clip0_32705_7508">
                    <Rect width="12.75" height="18" fill="white" transform="translate(0.875)" />
                </ClipPath>
            </Defs>
        </Svg>
    )
}

export default AirtimeTabIcon