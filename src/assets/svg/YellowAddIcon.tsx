import React from 'react';
import {Circle, Path, Svg} from 'react-native-svg';

const YellowAddIcon = () => {
  return (
    <Svg width="23" height="23" viewBox="0 0 23 23" fill="none">
      <Circle cx="11.5" cy="11.5" r="11.5" fill="#FFB717" />
      <Path
        d="M12.5675 10.4325H15.9325C16.5221 10.4325 17 10.9105 17 11.5C17 12.0895 16.5221 12.5675 15.9325 12.5675H12.5675V15.9325C12.5675 16.5221 12.0895 17 11.5 17C10.9105 17 10.4325 16.5221 10.4325 15.9325V12.5675H7.06746C6.47791 12.5675 6 12.0895 6 11.5C6 10.9105 6.47791 10.4325 7.06746 10.4325H10.4325V7.06746C10.4325 6.47791 10.9105 6 11.5 6C12.0895 6 12.5675 6.47791 12.5675 7.06746V10.4325Z"
        fill="white"
      />
    </Svg>
  );
};

export default YellowAddIcon;
