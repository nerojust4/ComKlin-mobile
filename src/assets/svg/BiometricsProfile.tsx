import React from "react";
import { ClipPath, Defs, G, Path, Rect, Svg } from "react-native-svg";

const BiometricsProfile = () => {
    return (
        <Svg width="21" height="21" viewBox="0 0 21 21" fill="none">
            <G clip-path="url(#clip0_31952_33304)">
                <Path d="M14.9921 15.1627V13.988C14.9921 13.3649 14.7482 12.7673 14.314 12.3267C13.8799 11.8861 13.2911 11.6385 12.6772 11.6385H8.04733C7.43338 11.6385 6.84457 11.8861 6.41044 12.3267C5.97631 12.7673 5.73242 13.3649 5.73242 13.988V15.1627" stroke="#5C068C" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                <Path d="M14.9921 15.1627V13.988C14.9921 13.3649 14.7482 12.7673 14.314 12.3267C13.8799 11.8861 13.2911 11.6385 12.6772 11.6385H8.04733C7.43338 11.6385 6.84457 11.8861 6.41044 12.3267C5.97631 12.7673 5.73242 13.3649 5.73242 13.988V15.1627" stroke="white" strokeOpacity="0.3" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                <Path d="M10.3608 9.28913C11.6393 9.28913 12.6757 8.23724 12.6757 6.93967C12.6757 5.6421 11.6393 4.59021 10.3608 4.59021C9.08232 4.59021 8.0459 5.6421 8.0459 6.93967C8.0459 8.23724 9.08232 9.28913 10.3608 9.28913Z" stroke="#5C068C" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                <Path d="M10.3608 9.28913C11.6393 9.28913 12.6757 8.23724 12.6757 6.93967C12.6757 5.6421 11.6393 4.59021 10.3608 4.59021C9.08232 4.59021 8.0459 5.6421 8.0459 6.93967C8.0459 8.23724 9.08232 9.28913 10.3608 9.28913Z" stroke="white" strokeOpacity="0.3" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            </G>
            <Path d="M12.4746 0.988281H16.7213C18.3781 0.988281 19.7213 2.33143 19.7213 3.98828V6.50441" stroke="#5C068C" strokeWidth="1.5" />
            <Path d="M12.4746 0.988281H16.7213C18.3781 0.988281 19.7213 2.33143 19.7213 3.98828V6.50441" stroke="white" strokeOpacity="0.3" strokeWidth="1.5" />
            <Path d="M8.24707 0.988281H4.00039C2.34354 0.988281 1.00039 2.33143 1.00039 3.98828V6.50441" stroke="#5C068C" strokeWidth="1.5" />
            <Path d="M8.24707 0.988281H4.00039C2.34354 0.988281 1.00039 2.33143 1.00039 3.98828V6.50441" stroke="white" strokeOpacity="0.3" strokeWidth="1.5" />
            <Path d="M8.24707 19.3754H4.00039C2.34354 19.3754 1.00039 18.0322 1.00039 16.3754V13.8592" stroke="#5C068C" strokeWidth="1.5" />
            <Path d="M8.24707 19.3754H4.00039C2.34354 19.3754 1.00039 18.0322 1.00039 16.3754V13.8592" stroke="white" strokeOpacity="0.3" strokeWidth="1.5" />
            <Path d="M12.4746 19.3754H16.7213C18.3781 19.3754 19.7213 18.0322 19.7213 16.3754V13.8592" stroke="#5C068C" strokeWidth="1.5" />
            <Path d="M12.4746 19.3754H16.7213C18.3781 19.3754 19.7213 18.0322 19.7213 16.3754V13.8592" stroke="white" strokeOpacity="0.3" strokeWidth="1.5" />
            <Defs>
                <ClipPath id="clip0_31952_33304">
                    <Rect width="13.8895" height="14.0968" fill="white" transform="translate(3.41699 2.82812)" />
                </ClipPath>
            </Defs>
        </Svg>

    )
}

export default BiometricsProfile