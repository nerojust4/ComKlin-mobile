import React from "react";
import { ClipPath, Defs, G, Path, Rect, Svg } from "react-native-svg";

const ChainIcon = () => {
    return (
        <Svg width="21" height="21" viewBox="0 0 21 21" fill="none">
            <Path d="M8.83301 11.6284C9.55714 12.5965 10.666 13.2023 11.8719 13.2887C13.0777 13.375 14.2616 12.9334 15.1163 12.0784L17.6163 9.57839C19.1954 7.94348 19.1728 5.3447 17.5656 3.73748C15.9584 2.13026 13.3596 2.10767 11.7247 3.68672L10.2913 5.11172" stroke="#BDBDBD" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            <Path d="M12.1668 9.96195C11.4427 8.99387 10.3338 8.38803 9.12796 8.30167C7.92211 8.21531 6.7382 8.65695 5.8835 9.51195L3.3835 12.012C1.80445 13.6469 1.82703 16.2456 3.43426 17.8529C5.04148 19.4601 7.64025 19.4827 9.27517 17.9036L10.7002 16.4786" stroke="#BDBDBD" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
        </Svg>
    )
}

export default ChainIcon