import React from 'react';
import {Path, Svg} from 'react-native-svg';

const EmailIcon = () => {
  return (
    <Svg width="20" height="21" viewBox="0 0 20 21" fill="none">
      <Path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M3.33366 4.29688H16.667C17.5837 4.29688 18.3337 5.04687 18.3337 5.96354V15.9635C18.3337 16.8802 17.5837 17.6302 16.667 17.6302H3.33366C2.41699 17.6302 1.66699 16.8802 1.66699 15.9635V5.96354C1.66699 5.04687 2.41699 4.29688 3.33366 4.29688Z"
        stroke="#BDBDBD"
        stroke-width="2"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <Path
        d="M18.3337 5.96362L10.0003 11.797L1.66699 5.96362"
        stroke="#BDBDBD"
        stroke-width="2"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
    </Svg>
  );
};

export default EmailIcon;
