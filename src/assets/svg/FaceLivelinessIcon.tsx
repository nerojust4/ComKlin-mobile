import React from "react";
import { Circle, Path, Svg } from "react-native-svg";
import { COLORS } from "../../utils/ColorUtils";

const FaceLivelinessIcon = () => {
    return (
        <Svg width="275" height="275" viewBox="0 0 275 275" fill={COLORS.fcmbPurpleLight} >
            <Circle cx="137.5" cy="137.5" r="137.265" fill="#5C068C" />
            <Circle cx="137.5" cy="137.5" r="137.265" fill="white" fill-opacity="0.9" />
            <Circle cx="137.5" cy="137.5" r="137.265" stroke="#5C068C" strokeWidth="0.469283" />
            <Path d="M118.651 104.885H111.299C109.349 104.885 107.479 105.659 106.101 107.038C104.722 108.417 103.947 110.287 103.947 112.237V119.589M155.412 104.885H162.764C164.714 104.885 166.584 105.659 167.963 107.038C169.342 108.417 170.116 110.287 170.116 112.237V119.589M151.736 123.265V130.617M122.328 123.265V130.617M126.004 152.673C126.004 152.673 129.68 156.35 137.032 156.35C144.384 156.35 148.06 152.673 148.06 152.673M137.032 123.265V141.645H133.356M118.651 171.054H111.299C109.349 171.054 107.479 170.279 106.101 168.9C104.722 167.522 103.947 165.652 103.947 163.702V156.35M155.412 171.054H162.764C164.714 171.054 166.584 170.279 167.963 168.9C169.342 167.522 170.116 165.652 170.116 163.702V156.35" stroke="#5C068C" strokeWidth="7.21523" stroke-linecap="round" stroke-linejoin="round" />
        </Svg>

    )
}

export default FaceLivelinessIcon