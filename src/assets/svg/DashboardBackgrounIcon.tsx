import React, {ReactNode} from 'react';
import {View} from 'react-native';
import {SvgXml} from 'react-native-svg';

const DashboardBackgroundIcon = ({children}: {children: ReactNode}) => {
  return (
    <View
      style={{
        position: 'relative',
      }}>
      <SvgXml
        xml={`<svg xmlns="http://www.w3.org/2000/svg" width="340" height="219" viewBox="0 0 340 219" fill="none">
        <g filter="url(#filter0_dd_35330_2947)">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M23 18C23 13.5817 26.5817 10 31 10H308C312.418 10 316 13.5817 316 18V172C316 176.418 312.418 180 308 180H31C26.5817 180 23 176.418 23 172V18Z" fill="#5F138D"/>
        </g>
        <defs>
        <filter id="filter0_dd_35330_2947" x="-7" y="-6" width="353" height="236" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix"/>
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
        <feOffset dy="20"/>
        <feGaussianBlur stdDeviation="15"/>
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.13 0"/>
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_35330_2947"/>
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
        <feOffset dy="14"/>
        <feGaussianBlur stdDeviation="15"/>
        <feColorMatrix type="matrix" values="0 0 0 0 0.372549 0 0 0 0 0.0745098 0 0 0 0 0.552941 0 0 0 0.41 0"/>
        <feBlend mode="normal" in2="effect1_dropShadow_35330_2947" result="effect2_dropShadow_35330_2947"/>
        <feBlend mode="normal" in="SourceGraphic" in2="effect2_dropShadow_35330_2947" result="shape"/>
        </filter>
        </defs>
        </svg>            `}
      />
      {children}
    </View>
  );
};

export default DashboardBackgroundIcon;
