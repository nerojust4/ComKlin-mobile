import React from "react";
import { Path, Svg } from "react-native-svg";

const ProfileIcon = () => {
    return (
        <Svg width="20" height="20" viewBox="0 0 20 20" fill="none">
            <Path d="M16.6663 17.5V15.8333C16.6663 13.9924 15.174 12.5 13.333 12.5H6.66634C4.82539 12.5 3.33301 13.9924 3.33301 15.8333V17.5" stroke="#BDBDBD" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
            <Path fill-rule="evenodd" clip-rule="evenodd" d="M10.0003 9.16667C11.8413 9.16667 13.3337 7.67428 13.3337 5.83333C13.3337 3.99238 11.8413 2.5 10.0003 2.5C8.15938 2.5 6.66699 3.99238 6.66699 5.83333C6.66699 7.67428 8.15938 9.16667 10.0003 9.16667Z" stroke="#BDBDBD" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        </Svg>
    )
}

export default ProfileIcon