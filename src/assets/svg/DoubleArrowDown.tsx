import React from "react";
import { Path, Svg } from "react-native-svg";
import { COLORS } from "../../utils/ColorUtils";

const DoubleArrowDown = ({color = COLORS.fcmbPurple}) => {
    return (
        <Svg width="30px" height="24px" viewBox="0 0 24 24" fill="none">
            <Path d="M19 5L12.7071 11.2929C12.3166 11.6834 11.6834 11.6834 11.2929 11.2929L5 5" stroke={color} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            <Path d="M19 13L12.7071 19.2929C12.3166 19.6834 11.6834 19.6834 11.2929 19.2929L5 13" stroke={color} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
        </Svg>
    )
}

export default DoubleArrowDown