import React from "react";
import { Path, Svg } from "react-native-svg";

const CloseIcon = () => {
    return (
        <Svg width="17" height="17" viewBox="0 0 17 17" fill="none">
            <Path fill-rule="evenodd" clip-rule="evenodd" d="M8.10514 15.1663C11.787 15.1663 14.7718 12.1816 14.7718 8.49967C14.7718 4.81778 11.787 1.83301 8.10514 1.83301C4.42324 1.83301 1.43848 4.81778 1.43848 8.49967C1.43848 12.1816 4.42324 15.1663 8.10514 15.1663Z" stroke="#F34E4E" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
            <Path d="M10.1055 6.5L6.10547 10.5" stroke="#F34E4E" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            <Path d="M6.10547 6.5L10.1055 10.5" stroke="#F34E4E" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
        </Svg>
    )
}

export default CloseIcon