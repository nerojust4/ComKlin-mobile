// models/Class.ts
export interface User {
  id: number;
  firstName: string;
  lastName: string;
}

export interface Lecturer {
  id: number;
  course: string;
  department: string;
  user: User;
}

export interface ClassRoom {
  id: number;
  name: string;
  roomNumber: number;
}

export interface Course {
  id: number;
  courseCode: string;
  courseName: string;
  credits: number;
  classRoom: ClassRoom;
}

export interface Class {
  id: number;
  name: string;
  semester: string;
  year: number;
  course: Course;
  lecturer: Lecturer;
  tempClassRoom?: ClassRoom | null; // Optional tempClassRoom which can be null or ClassRoom type
}

export interface GetClassesResponse {
  classes: Class[];
  totalPages: number;
}

export interface CreateClassRequest {
  name: string;
  semester: string;
  year: number;
  courseId: number;
  lecturerId: number;
  classRoomId?: number;
  tempClassRoomId?: number | null; // Optional temporary class room ID
}

export interface UpdateClassRequest {
  name?: string;
  semester?: string;
  year?: number;
  courseId?: number;
  lecturerId?: number;
  classRoomId?: number;
  tempClassRoomId?: number | null; // Optional temporary class room ID
}

export interface CreateClassResponse {
  class: Class;
}
