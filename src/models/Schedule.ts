// src/models/ClassSchedule.ts

// Schedule interface with updated parameters
export interface Schedule {
  id: number;
  classId: number;
  startDate: string | null;
  endDate: string | null;
  recurrenceType: 'daily' | 'weekly' | 'monthly' | 'custom' | null; // Recurrence type
  recurrenceInterval: number | null; // Interval for recurrence (e.g., every 2 days)
  recurrenceEndDate: string | null; // End date for recurrence
  recurrenceDays: string[]; // Days for custom recurrence
  students: number[]; // List of student IDs
  createdAt: string;
  updatedAt: string;
}

// CreateScheduleRequest interface with updated parameters
export interface CreateScheduleRequest {
  classId: number;
  startDate: string | null;
  endDate: string | null;
  recurrenceType: 'daily' | 'weekly' | 'monthly' | 'custom' | null; // Recurrence type
  recurrenceInterval: number | null; // Interval for recurrence (e.g., every 2 days)
  recurrenceEndDate: string | null; // End date for recurrence
  recurrenceDays: string[]; // Days for custom recurrence
  students: number[]; // List of student IDs
}

// UpdateScheduleRequest interface with updated parameters
export interface UpdateScheduleRequest {
  classId?: number;
  startDate?: string | null;
  endDate?: string | null;
  recurrenceType?: 'daily' | 'weekly' | 'monthly' | 'custom' | null; // Recurrence type
  recurrenceInterval?: number | null; // Interval for recurrence (e.g., every 2 days)
  recurrenceEndDate?: string | null; // End date for recurrence
  recurrenceDays?: string[]; // Days for custom recurrence
  students?: number[]; // List of student IDs
}

// GetSchedulesResponse interface with updated schedule details
export interface GetSchedulesResponse {
  schedules: Schedule[];
  totalPages: number;
}
