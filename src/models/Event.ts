// src/models/Event.ts

export interface Event {
  id: number;
  title: string;
  description: string;
  date: string; // Assuming date is stored as a string in ISO format
  location: string;
  createdAt: string; // Assuming createdAt is stored as a string in ISO format
  updatedAt: string; // Assuming updatedAt is stored as a string in ISO format
}

export interface GetEventsResponse {
  events: Event[];
  totalPages: number;
}
