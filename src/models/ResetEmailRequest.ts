// models/UserRegistration.ts
export interface ResetEmailRequest {
  email: string;
  client?: string;
}
