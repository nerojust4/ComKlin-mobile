export interface User {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
}

export interface Lecturer {
  id: number;
  qualification: string;
  course: string;
  yearsOfExperience: number;
  department: string;
  userId: number;
  createdAt: string;
  updatedAt: string;
  user?: User;
}

export interface GetLecturerResponse {
  teachers: Lecturer[];
  totalPages: number;
}

export interface CreateLecturerRequest {
  qualification: string;
  subject: string;
  yearsOfExperience: number;
  department: number;
  userId: number;
}

export interface UpdateLecturerRequest {
  qualification?: string;
  subject?: string;
  yearsOfExperience?: number;
  department?: number;
  userId?: number;
}
export interface DeleteLecturerRequest {
  userId?: number;
}

export interface CreateLecturerResponse {
  teacher: Lecturer;
}

export interface ValidateLecturerResponse {
  isTeacherExists: boolean;
}
