interface User {
  id: number;
  firstName: string;
  lastName: string;
}

interface Lecturer {
  id: number;
  course: string;
  user: User;
}

interface Course {
  id: number;
  courseCode: string;
}

interface ClassRoom {
  id: number;
  name: string;
  roomNumber: number;
  roomDescription: string;
  capacity: number;
}

interface Class {
  id: number;
  name: string;
  courseId: number;
  classRoomId: number;
  course: Course;
  classRoom: ClassRoom;
  lecturer: Lecturer;
}

interface Schedule {
  id: number;
  startDate: string; // Assuming date string format
  endDate: string; // Assuming date string format
  class: Class;
}

export interface Enrollment {
  id: number;
  studentId: number;
  scheduleId: number;
  createdAt: string; // Assuming date string format
  updatedAt: string; // Assuming date string format
  schedule: Schedule;
}

export interface GetEnrollmentsResponse {
  enrollments: Enrollment[];
  totalPages: number;
}

export interface CreateEnrollmentRequest {
  scheduleId: number;
  studentId: number;
}

export interface UpdateEnrollmentRequest {
  scheduleId?: number;
  studentId?: number;
}
