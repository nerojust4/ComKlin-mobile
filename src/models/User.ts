export interface User {
  id: number;
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  phoneNumber: string;
  dob: string;
  gender: string;
  address: string;
  city: string;
  state: string;
  country: string;
  active: boolean;
  image: string | null;
  roleId: number;
}

export interface CreateUserRequest {
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  phoneNumber: string;
  address: string;
  gender:string,
  dob:Date,
  city: string;
  state: string;
  country: string;
  password: string;
  roleId: number;
}

export interface UpdateUserRequest {
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  phoneNumber: string;
  address: string;
  gender:string,
  dob:Date,
  city: string;
  state: string;
  country: string;
  password: string;
  roleId: number;
}

export interface CreateUserResponse {
  active: boolean;
  id: number;
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  phoneNumber: string;
  dob: string;
  gender: string;
  city: string;
  address: string;
  state: string;
  country: string;
  roleId: number;
  updatedAt: string;
  createdAt: string;
}

