import {User} from './User';

export interface Student {
    id: number;
    major: string;
    yearOfStudy: string;
    userId: number;
    createdAt: string; // You might want to use Date instead of string
    updatedAt: string; // You might want to use Date instead of string
    user: User;
  }
export interface CreateStudentRequest {
  userId: number;
  major: string;
  yearOfStudy: string;
  // Add other fields required to create a student
}

export interface UpdateStudentRequest {
  userId?: number;
  major?: string;
  yearOfStudy?: string;
  // Add other fields required to update a student
}

export interface GetStudentsResponse {
  students: Student[];
  totalPages: number;
}
