// models/Announcement.ts

export interface Announcement {
  id: number;
  title: string;
  description: string;
  date: string;
}

export interface GetAnnouncementsResponse {
  announcements: Announcement[];
  totalPages: number;
}

export interface CreateAnnouncementRequest {
  title: string;
  description: string;
  date: string;
}

export interface UpdateAnnouncementRequest {
  title?: string;
  description?: string;
  date?: string;
}

export interface CreateAnnouncementResponse {
  announcement: Announcement;
}
