export interface Role {
    id: number;
    name: string;
    createdAt: string;
    updatedAt: string;
  }

  export interface GetRolesResponse {
    roles: Role[];
    totalPages: number;
  }