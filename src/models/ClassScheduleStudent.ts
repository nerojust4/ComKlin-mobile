// src/models/ClassScheduleStudent.ts
export interface ClassScheduleStudent {
  classScheduleId: number;
  studentId: number;
  createdAt: string;
  updatedAt: string;
}

export interface CreateClassScheduleStudentRequest {
  classScheduleId: number;
  studentId: number;
}

export interface UpdateClassScheduleStudentRequest {
  classScheduleId?: number;
  studentId?: number;
}

export interface GetClassScheduleStudentsResponse {
  classScheduleStudents: ClassScheduleStudent[];
  totalPages: number;
}
