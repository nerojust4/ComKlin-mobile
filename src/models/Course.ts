export interface Course {
  id: number;
  courseCode: string;
  courseName: string;
  description: string;
  credits: number;
  department: string;
  createdAt: string;
  updatedAt: string;
}

export interface GetCoursesResponse {
  courses: Course[];
  totalPages: number;
}

export interface CreateCourseRequest {
  courseCode: string;
  courseName: string;
  description: string;
  credits: number;
  department: string;
}

export interface UpdateCourseRequest {
  courseCode?: string;
  courseName?: string;
  description?: string;
  credits?: number;
  department?: string;
}

export interface CreateCourseResponse {
  course: Course;
}
