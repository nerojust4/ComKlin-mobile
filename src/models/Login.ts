import {User} from './User';

export interface LoginRequest {
  email: string;
  password: string;
  client?: string;
  phoneType?: string;
  deviceId?: string;
  deviceName?: string;
  appVersion?: string;
  ipAddress?: string;
}
export interface LoginResponse {
  token: string;
  user: User;
}
export interface LogoutResponse {
  message: string;
}
