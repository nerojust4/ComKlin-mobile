export interface AppConfig {
  id: number;
  url: string;
  name: string;
  label: string;
}

export interface GetAppConfigResponse {
  config: AppConfig[];
  totalPages: number;
}

export interface Statistics {
  totalUsers: number;
  totalStaff: number;
  totalAdmin: number;
  totalStudents: number;
  totalLecturers: number;
}
export interface GetStatisticsResponse {
  statistics: Statistics[];
}
