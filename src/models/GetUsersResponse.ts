import { User } from "./User";

export interface GetUsersResponse {
  users: User[];
  totalPages: number;
}
