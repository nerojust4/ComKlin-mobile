export interface ClassRoom {
  id: number;
  name: string;
  roomNumber: string;
  roomDescription: string;
  createdAt: string;
  updatedAt: string;
}

export interface CreateClassRoomRequest {
  name: string;
  roomNumber: string;
  roomDescription: string;
}

export interface UpdateClassRoomRequest {
  name?: string;
  roomNumber?: string;
  roomDescription?: string;
}

export interface CreateClassRoomResponse {
  classRoom: ClassRoom;
}

export interface GetClassRoomResponse {
  classRooms: ClassRoom[];
  totalPages: number;
}
