// models/UserRegistration.ts
export interface ResetEmailResponse {
  statusMessage: string;
}
