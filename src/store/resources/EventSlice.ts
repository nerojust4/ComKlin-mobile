import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {Event} from '../../models/Event';
import {createEvent, deleteEvent, fetchEvents, updateEvent} from './EventThunk';

interface EventState {
  events: Event[];
  event: Event | null;
  totalPages: number;
  loading: boolean;
  error: string | null;
}

const initialState: EventState = {
  events: [],
  event: null,
  totalPages: 0,
  loading: false,
  error: null,
};

const eventSlice = createSlice({
  name: 'event',
  initialState,
  reducers: {
    setEvents: (
      state,
      action: PayloadAction<{events: Event[]; totalPages: number}>,
    ) => {
      state.events = action?.payload?.events || [];
      state.totalPages = action?.payload?.totalPages || 0;
      state.loading = false;
      state.error = null;
    },
    setEvent: (state, action: PayloadAction<Event | null>) => {
      state.event = action.payload;
      state.loading = false;
      state.error = null;
    },
    setError: (state, action: PayloadAction<string | null>) => {
      state.error = action.payload;
      state.loading = false;
    },
  },
  extraReducers: builder => {
    builder
      // Fetch events
      .addCase(fetchEvents.pending, state => {
        state.loading = true;
        state.error = null;
      })
      .addCase(fetchEvents.fulfilled, (state, action) => {
        state.events = action.payload.events;
        state.totalPages = action.payload.totalPages;
        state.loading = false;
        state.error = null;
      })
      .addCase(fetchEvents.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload as string;
      })
      // Create event
      .addCase(createEvent.pending, state => {
        state.loading = true;
        state.error = null;
      })
      .addCase(createEvent.fulfilled, (state, action) => {
        state.events.push(action.payload);
        state.loading = false;
        state.error = null;
      })
      .addCase(createEvent.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload as string;
      })
      // Update event
      .addCase(updateEvent.pending, state => {
        state.loading = true;
        state.error = null;
      })
      .addCase(updateEvent.fulfilled, (state, action) => {
        const index = state.events.findIndex(
          event => event.id === action.payload.id,
        );
        if (index !== -1) {
          state.events[index] = action.payload;
        }
        state.loading = false;
        state.error = null;
      })
      .addCase(updateEvent.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload as string;
      })
      // Delete event
      .addCase(deleteEvent.pending, state => {
        state.loading = true;
        state.error = null;
      })
      .addCase(deleteEvent.fulfilled, (state, action) => {
        state.events = state.events.filter(
          event => event.id !== action.payload,
        );
        state.loading = false;
        state.error = null;
      })
      .addCase(deleteEvent.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload as string;
      });
  },
});

export const {setEvents, setEvent, setError} = eventSlice.actions;

export default eventSlice.reducer;
