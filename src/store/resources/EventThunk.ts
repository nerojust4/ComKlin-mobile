import {createAsyncThunk} from '@reduxjs/toolkit';
import api from '../../services/api'; // Assuming you have an API service configured
import {setEvents, setEvent, setError} from './EventSlice'; // Adjust the import paths based on your actual file structure
import {Event, GetEventsResponse} from '../../models/Event';
import ApiResponse from '../../utils/ApiResponse';

export const fetchEvents = createAsyncThunk(
  'event/fetchEvents',
  async (
    {page, pageSize}: {page: number; pageSize: number},
    {rejectWithValue, dispatch},
  ) => {
    try {
      const response = await api.get<ApiResponse<GetEventsResponse>>(
        '/events',
        {
          params: {
            page,
            pageSize,
          },
        },
      );
      const {status, result, errors} = response.data;

      if (status || (result && !errors.length)) {
        dispatch(
          setEvents({events: result.events, totalPages: result.totalPages}),
        );
        return {events: result.events, totalPages: result.totalPages};
      } else {
        return rejectWithValue(errors[0]?.message || 'Failed to fetch events');
      }
    } catch (error: any) {
      const errorMessage =
        error.response?.data?.errors?.[0]?.message || 'Failed to fetch events';
      return rejectWithValue(errorMessage);
    }
  },
);

export const createEvent = createAsyncThunk(
  'event/createEvent',
  async (eventData: Event, {rejectWithValue, dispatch}) => {
    try {
      const response = await api.post<ApiResponse<Event>>('/events', eventData);
      const {status, result, errors} = response.data;

      if (status || (result && !errors.length)) {
        dispatch(setEvent(result));
        return result;
      } else {
        return rejectWithValue(errors[0]?.message || 'Failed to create event');
      }
    } catch (error: any) {
      const errorMessage =
        error.response?.data?.errors?.[0]?.message || 'Failed to create event';
      return rejectWithValue(errorMessage);
    }
  },
);

export const updateEvent = createAsyncThunk(
  'event/updateEvent',
  async (
    {id, eventData}: {id: number; eventData: Event},
    {rejectWithValue, dispatch},
  ) => {
    try {
      const response = await api.put<ApiResponse<Event>>(
        `/events/${id}`,
        eventData,
      );
      const {status, result, errors} = response.data;

      if (status || (result && !errors.length)) {
        dispatch(setEvent(result));
        return result;
      } else {
        return rejectWithValue(errors[0]?.message || 'Failed to update event');
      }
    } catch (error: any) {
      const errorMessage =
        error.response?.data?.errors?.[0]?.message || 'Failed to update event';
      return rejectWithValue(errorMessage);
    }
  },
);

export const deleteEvent = createAsyncThunk(
  'event/deleteEvent',
  async (eventId: number, {rejectWithValue, dispatch}) => {
    try {
      const response = await api.delete<ApiResponse<void>>(
        `/events/${eventId}`,
      );
      const {status, errors} = response.data;

      if (status || !errors.length) {
        // Optionally handle success actions here
        return eventId;
      } else {
        return rejectWithValue(errors[0]?.message || 'Failed to delete event');
      }
    } catch (error: any) {
      const errorMessage =
        error.response?.data?.errors?.[0]?.message || 'Failed to delete event';
      return rejectWithValue(errorMessage);
    }
  },
);
