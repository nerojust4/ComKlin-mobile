// src/store/userThunks.ts
import {createAsyncThunk} from '@reduxjs/toolkit';
import api from '../../services/api';
import {clearUser, setError, setLoading, setProfile} from './AuthSlice';
import {LoginRequest, LoginResponse, LogoutResponse} from '../../models/Login';
import ApiResponse from '../../utils/ApiResponse';
import {ResetEmailRequest} from '../../models/ResetEmailRequest';
import {ResetEmailResponse} from '../../models/ResetEmailResponse';
import {GetRolesResponse} from '../../models/Role';
import {setUser, setUsers} from '../user/UserSlice';

export const login = createAsyncThunk(
  'auth/login',
  async (loginData, {rejectWithValue, dispatch}) => {
    try {
      dispatch(setError(null)); // Clear the error state

      const response = await api.post('/login', loginData);

      // Log the raw response
      console.log('Raw response:', response);

      if (!response || !response.data) {
        throw new Error('No response data');
      }

      const wrappedResponse = {
        status: response.data.status,
        result: response.data.result,
        errors: response.data.errors,
      };

      // Log the wrapped response
      console.log('Wrapped response:', wrappedResponse);

      if (wrappedResponse.status) {
        const token = wrappedResponse.result?.token; // Use optional chaining to access token
        const user = wrappedResponse.result?.user; // Use optional chaining to access user

        if (token && user) {
          // Log the user and token
          console.log('User:', user);
          console.log('Token:', token);

          dispatch(setProfile({token, user}));
          dispatch(setUser(user));
          return wrappedResponse.result; // Return the response data on successful login
        } else {
          return rejectWithValue(
            wrappedResponse.errors?.[0]?.message || 'Invalid response format',
          );
        }
      } else {
        return rejectWithValue(
          wrappedResponse.errors?.[0]?.message || 'Login failed',
        );
      }
    } catch (error) {
      // Handle network errors or other exceptions
      const errorMessage =
        error.response?.data?.errors?.[0]?.message ||
        'Login failed, please check your network connection and try again';

      // Log the error
      console.log('Error:', errorMessage, error);

      return rejectWithValue(errorMessage);
    }
  },
);

export const logout = createAsyncThunk(
  'auth/logout',
  async (_, {rejectWithValue, dispatch}) => {
    try {
      console.log('About to logout');
      const response = await api.get<ApiResponse<LogoutResponse>>('/logout');
      const {status, result, errors} = response.data;

      if (status || (result && !errors.length)) {
        console.log('Logout response', result);
        dispatch(clearUser()); // Clear user data on logout
        return result;
      } else {
        return rejectWithValue(errors[0]?.message || 'Logout failed');
      }
    } catch (error: any) {
      // Handle network errors or other exceptions
      const errorMessage =
        error.response?.data?.errors?.[0]?.message || 'Logout failed';
      console.log('Error:', errorMessage, error);

      return rejectWithValue(errorMessage);
    }
  },
);

export const resetPasswordWithEmail = createAsyncThunk(
  'auth/reset-password',
  async (userData: ResetEmailRequest, {rejectWithValue, dispatch}) => {
    try {
      dispatch(setError(null)); // Clear the error state
      const response = await api.post<ApiResponse<ResetEmailResponse>>(
        '/reset-password',
        userData,
      );
      const {status, result, errors} = response.data;

      if (status || (result && !errors.length)) {
        return result;
      } else {
        return rejectWithValue(errors[0]?.message || 'Registration failed');
      }
    } catch (error: any) {
      // Handle network errors or other exceptions
      const errorMessage =
        error.response?.data?.errors?.[0]?.message || 'Registration failed';
      return rejectWithValue(errorMessage);
    }
  },
);
export const resetPasswordWithToken = createAsyncThunk(
  'auth/reset-password',
  async (userData: ResetEmailRequest, {rejectWithValue, dispatch}) => {
    try {
      dispatch(setError(null)); // Clear the error state
      const response = await api.post<ApiResponse<ResetEmailResponse>>(
        '/reset-password',
        userData,
      );
      const {status, result, errors} = response.data;

      if (status || (result && !errors.length)) {
        return result;
      } else {
        return rejectWithValue(errors[0]?.message || 'Registration failed');
      }
    } catch (error: any) {
      // Handle network errors or other exceptions
      const errorMessage =
        error.response?.data?.errors?.[0]?.message || 'Registration failed';
      return rejectWithValue(errorMessage);
    }
  },
);
