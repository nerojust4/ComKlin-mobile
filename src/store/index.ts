import {configureStore} from '@reduxjs/toolkit';
import authReducer from './auth/AuthSlice';
import userReducer from './user/UserSlice';
import eventReducer from './events/EventSlice';

const store = configureStore({
  reducer: {
    auth: authReducer,
    users: userReducer,
    events: eventReducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false, // Disable serializable check in development mode
    }),
});
export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;

export default store;
