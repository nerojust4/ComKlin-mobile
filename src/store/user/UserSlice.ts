// src/store/UserSlice.ts
import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {
  fetchUsers,
  register,
  checkIfUsernameExists,
  createUser,
  updateUser,
  deleteUser,
  searchUsers,
} from './UserThunk';
import {User} from '../../models/User';
import {GetUsersResponse} from '../../models/GetUsersResponse';

interface UserState {
  users: User[];
  user: User | null;
  totalPages: number;
  loading: boolean;
  error: string | null;
  isUserExists: boolean;
  showUsernameDialog: boolean;
  usernameDialogMessage: string;
}

const initialState: UserState = {
  users: [],
  user: null,
  totalPages: 0,
  loading: false,
  error: null,
  isUserExists: false,
  showUsernameDialog: false,
  usernameDialogMessage: '',
};

const userSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    setError: (state, action: PayloadAction<string | null>) => {
      state.error = action.payload;
    },
    setUsers: (
      state,
      action: PayloadAction<{users: User[]; totalPages: number}>,
    ) => {
      state.users = action.payload.users;
      state.totalPages = action.payload.totalPages;
    },
    setUser: (state, action: PayloadAction<User | null>) => {
      state.user = action.payload;
    },
    clearUser(state) {
      Object.assign(state, initialState);
    },
  },
  extraReducers: builder => {
    builder
      .addCase(fetchUsers.pending, state => {
        state.loading = true;
        state.error = null;
      })
      .addCase(
        fetchUsers.fulfilled,
        (state, action: PayloadAction<GetUsersResponse>) => {
          state.loading = false;
          state.users = action.payload.users;
          state.totalPages = action.payload.totalPages;
        },
      )
      .addCase(fetchUsers.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload as string;
      })
      .addCase(searchUsers.pending, state => {
        state.loading = true;
        state.error = null;
      })
      .addCase(
        searchUsers.fulfilled,
        (state, action: PayloadAction<GetUsersResponse>) => {
          state.loading = false;
          state.users = action.payload.users;
          state.totalPages = action.payload.totalPages;
        },
      )
      .addCase(searchUsers.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload as string;
      })
      // Register, CheckUsername, CreateUser, UpdateUser, and DeleteUser cases
      .addCase(register.pending, state => {
        state.loading = true;
        state.error = null;
      })
      .addCase(register.fulfilled, (state, action) => {
        state.loading = false;
        state.user = action.payload as User;
        state.error = null;
      })
      .addCase(register.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload as string;
      })
    
      .addCase(createUser.pending, state => {
        state.loading = true;
        state.error = null;
      })
      .addCase(createUser.fulfilled, (state, action: PayloadAction<User>) => {
        state.loading = false;
        state.users.push(action.payload);
      })
      .addCase(createUser.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload as string;
      })
      .addCase(updateUser.pending, state => {
        state.loading = true;
        state.error = null;
      })
      .addCase(updateUser.fulfilled, (state, action: PayloadAction<User>) => {
        state.loading = false;
        state.user = action.payload;
        state.users = state.users.map(user =>
          user.id === action.payload.id ? action.payload : user,
        );
      })
      .addCase(updateUser.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload as string;
      })
      .addCase(deleteUser.pending, state => {
        state.loading = true;
        state.error = null;
      })
      .addCase(deleteUser.fulfilled, (state, action: PayloadAction<number>) => {
        state.loading = false;
        state.users = state.users.filter(user => user.id !== action.payload);
      })
      .addCase(deleteUser.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload as string;
      });
  },
});

export const {setError, setUsers, setUser, clearUser} = userSlice.actions;

export default userSlice.reducer;
