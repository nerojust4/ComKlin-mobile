// src/store/userThunks.ts
import {createAsyncThunk} from '@reduxjs/toolkit';
import api from '../../services/api';
import {setUsers, setUser, setError} from './UserSlice';
import {GetUsersResponse} from '../../models/GetUsersResponse';
import {User} from '../../models/User';
import {
  CreateUserResponse,
  CreateUserRequest,
  UpdateUserRequest,
} from '../../models/User';
import ApiResponse from '../../utils/ApiResponse';
import {ValidateUsernameResponse} from '../../models/ValidateUsernameResponse';

export const register = createAsyncThunk(
  'user/register',
  async (userData: CreateUserRequest, {rejectWithValue, dispatch}) => {
    try {
      dispatch(setError(null)); // Clear the error state
      const response = await api.post<ApiResponse<CreateUserResponse>>(
        '/register',
        userData,
      );
      const {status, result, errors} = response.data;

      if (status || (result && !errors.length)) {
        dispatch(setUser(result.user));
        dispatch(fetchUsers({page: 1, pageSize: 10}));
        console.log('registerd user', result);

        return result.user;
      } else {
        return rejectWithValue(errors[0]?.message || 'Registration failed');
      }
    } catch (error: any) {
      // Handle network errors or other exceptions
      const errorMessage =
        error.response?.data?.errors?.[0]?.message || 'Registration failed';
      return rejectWithValue(errorMessage);
    }
  },
);

export const checkIfUsernameExists = createAsyncThunk(
  'user/checkIfUsernameExists',
  async (username: string, {rejectWithValue, dispatch}) => {
    try {
      dispatch(setError(null)); // Clear the error state

      const response = await api.get<ApiResponse<ValidateUsernameResponse>>(
        `/user/validate-username/${username}`,
      );
      const {status, result, errors} = response.data;

      if (status || (result && !errors.length)) {
        return result?.isUserExists;
      } else {
        return rejectWithValue(
          errors[0]?.message || 'Username Validation failed',
        );
      }
    } catch (error: any) {
      // Handle network errors or other exceptions
      const errorMessage =
        error.response?.data?.errors?.[0]?.message ||
        'Username Validation failed';
      return rejectWithValue(errorMessage);
    }
  },
);

export const createUser = createAsyncThunk(
  'user/createUser',
  async (userData: CreateUserRequest, {rejectWithValue, dispatch}) => {
    try {
      console.log('About to create user', userData);
      const response = await api.post<ApiResponse<User>>('/user', userData);
      const {status, result, errors} = response.data;

      if (status || (result && !errors.length)) {
        dispatch(setUser(result));
        // console.log('registered user', result);

        return result;
      } else {
        return rejectWithValue(
          errors[0]?.message || 'User registration failed',
        );
      }
    } catch (error: any) {
      const errorMessage =
        error.response?.data?.errors?.[0]?.message ||
        'User registration failed';
      return rejectWithValue(errorMessage);
    }
  },
);
export const fetchUsers = createAsyncThunk(
  'user/fetchUsers',
  async (
    {
      page,
      pageSize,
      fetchAll,
    }: {page: number; pageSize: number; fetchAll?: boolean},
    {rejectWithValue, dispatch},
  ) => {
    console.log('about to get users');
    try {
      const response = await api.get<ApiResponse<GetUsersResponse>>('/user', {
        params: {
          page: fetchAll ? undefined : page,
          pageSize: fetchAll ? undefined : pageSize,
          all: fetchAll ? true : undefined,
        },
      });
      const {status, result, errors} = response.data;
      const {users, totalPages} = result;
      if (status || (result && !errors.length)) {
        dispatch(setUsers({users, totalPages}));
        // console.log('users', users);
        return {users, totalPages};
      } else {
        return rejectWithValue(errors[0]?.message || 'Fetching users failed');
      }
    } catch (error: any) {
      const errorMessage =
        error.response?.data?.errors?.[0]?.message || 'Failed to fetch users';
      return rejectWithValue(errorMessage);
    }
  },
);

export const searchUsers = createAsyncThunk(
  'user/searchUsers',
  async (
    {
      page,
      pageSize,
      query,
      fetchAll,
    }: {page: number; pageSize: number; query: string; fetchAll: boolean},
    {rejectWithValue, dispatch},
  ) => {
    console.log('about to search users', query);
    try {
      const response = await api.get<ApiResponse<GetUsersResponse>>('/user', {
        params: {
          page,
          pageSize,
          keywords: query,
          all: fetchAll,
        },
      });

      const {status, result, errors} = response.data;
      const {users, totalPages} = result;
      if (status || (result && !errors.length)) {
        dispatch(setUsers({users, totalPages}));
        return {users, totalPages};
      } else {
        return rejectWithValue(errors[0]?.message || 'Searching users failed');
      }
    } catch (error: any) {
      const errorMessage =
        error.response?.data?.errors?.[0]?.message || 'Failed to search users';
      return rejectWithValue(errorMessage);
    }
  },
);

export const updateUser = createAsyncThunk(
  'user/updateUser',
  async (
    {id, data}: {id: number; data: UpdateUserRequest},
    {rejectWithValue, dispatch},
  ) => {
    try {
      console.log('About to update profile data', data);
      const response = await api.put<ApiResponse<User>>(`/user/${id}`, data);
      dispatch(setUser(response?.data?.result));
      dispatch(fetchUsers({page: 1, pageSize: 10}));

      return response.data.result;
    } catch (error: any) {
      return rejectWithValue('Failed to update user');
    }
  },
);
export const fetchUserById = createAsyncThunk(
  'user/fetchUser',
  async (id: number, {rejectWithValue, dispatch}) => {
    try {
      const response = await api.get<ApiResponse<void>>(`/user/${id}`);

      dispatch(setUser(response.data.result));
      console.log('Deleted user', response.data);

      return response.data.result;
    } catch (error: any) {
      return rejectWithValue('Failed to delete user');
    }
  },
);
export const deleteUser = createAsyncThunk(
  'user/deleteUser',
  async (id: number, {rejectWithValue, dispatch}) => {
    try {
      const response = await api.delete<ApiResponse<void>>(`/user/${id}`);

      dispatch(setUser(response.data.result));
      console.log('Deleted user', response.data);

      return id;
    } catch (error: any) {
      return rejectWithValue('Failed to delete user');
    }
  },
);
