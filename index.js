/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import {LogBox} from 'react-native';

// Ignore specific log notifications by message
LogBox.ignoreLogs([
  '(ADVICE) View #163 of type RCTView has a shadow set but cannot calculate shadow efficiently. Consider setting a solid background color to fix this, or apply the shadow to a more specific component.',
  'Sending `onAnimatedValueUpdate` with no listeners registered.',
  'ReactImageView: Image source "null" doesn\'t exist',
  'Require cycles are allowed, but can result in uninitialized values. Consider refactoring to remove the need for a cycle.',
  ' Warning: This synthetic event is reused for performance reasons. If you\'re seeing this, you\'re accessing the property `nativeEvent` on a released/nullified synthetic event. This is set to null. If you must keep the original synthetic event around, use event.persist(). See https://reactjs.org/link/event-pooling for more information.'
]);

AppRegistry.registerComponent(appName, () => App);
